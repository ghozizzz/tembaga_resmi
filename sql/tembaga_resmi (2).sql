-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2019 at 05:24 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tembaga_resmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `beli_sparepart`
--

CREATE TABLE `beli_sparepart` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) NOT NULL,
  `no_pengajuan` varchar(50) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `jenis_kebutuhan` tinyint(1) NOT NULL,
  `nama_pengaju` varchar(50) DEFAULT NULL,
  `tgl_sparepart_dibutuhkan` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `approved` datetime NOT NULL,
  `approved_by` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `rejected` datetime NOT NULL,
  `rejected_by` int(11) NOT NULL,
  `reject_remarks` text NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beli_sparepart`
--

INSERT INTO `beli_sparepart` (`id`, `reff_id`, `no_pengajuan`, `tgl_pengajuan`, `jenis_kebutuhan`, `nama_pengaju`, `tgl_sparepart_dibutuhkan`, `status`, `created`, `created_by`, `approved`, `approved_by`, `remarks`, `rejected`, `rejected_by`, `reject_remarks`, `modified`, `modified_by`) VALUES
(47, 25, 'PPS-KMP.250419.0001', '2019-04-30', 1, '', '0000-00-00', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0),
(48, 0, 'PPS-KMP.201905.0001', '2019-05-02', 1, 'SUP', NULL, 1, '2019-05-02 03:05:00', 1, '2019-05-02 03:05:57', 1, '', '0000-00-00 00:00:00', 0, '', '2019-05-02 03:05:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `beli_sparepart_detail`
--

CREATE TABLE `beli_sparepart_detail` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) NOT NULL,
  `beli_sparepart_id` int(11) NOT NULL,
  `sparepart_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `flag_po` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beli_sparepart_detail`
--

INSERT INTO `beli_sparepart_detail` (`id`, `reff_id`, `beli_sparepart_id`, `sparepart_id`, `qty`, `keterangan`, `flag_po`) VALUES
(83, 75, 47, 51, 8, '', 1),
(84, 76, 47, 52, 4, '', 1),
(85, 77, 47, 129, 19, '', 1),
(86, 78, 47, 1705, 10, '', 1),
(87, 0, 48, 8, 25, '', 1),
(88, 0, 48, 158, 36, '', 0),
(89, 0, 48, 54, 50, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lpb`
--

CREATE TABLE `lpb` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) NOT NULL,
  `no_bpb` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `po_id` int(11) NOT NULL,
  `vk_id` int(11) DEFAULT '0',
  `remarks` text,
  `pengirim` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpb`
--

INSERT INTO `lpb` (`id`, `reff_id`, `no_bpb`, `tanggal`, `po_id`, `vk_id`, `remarks`, `pengirim`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(26, 50, 'BPB-KMP.201905.0001', '2019-05-02', 41, 0, '', '', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(27, 51, 'BPB-KMP.201905.0002', '2019-05-02', 42, 0, '', '', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(28, 52, 'BPB-KMP.201905.0003', '2019-05-02', 42, 0, '', '', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(29, 0, 'BPB-KMP.201905.0005', '2019-05-02', 43, 0, '', '', '2019-05-02 03:05:58', 1, '2019-05-02 03:05:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lpb_detail`
--

CREATE TABLE `lpb_detail` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) NOT NULL,
  `lpb_id` int(11) NOT NULL,
  `po_detail_id` int(11) NOT NULL,
  `sparepart_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `line_remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpb_detail`
--

INSERT INTO `lpb_detail` (`id`, `reff_id`, `lpb_id`, `po_detail_id`, `sparepart_id`, `qty`, `line_remarks`) VALUES
(6, 57, 26, 3, 129, 22, ''),
(7, 58, 26, 4, 1705, 10, ''),
(8, 59, 27, 5, 51, 8, ''),
(9, 60, 28, 6, 52, 4, ''),
(10, 0, 29, 7, 8, 30, '');

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE `po` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) NOT NULL,
  `no_po` varchar(25) NOT NULL,
  `tanggal` date NOT NULL,
  `beli_sparepart_id` int(11) NOT NULL,
  `ppn` tinyint(1) NOT NULL,
  `diskon` int(11) NOT NULL,
  `materai` int(11) NOT NULL,
  `currency` varchar(4) NOT NULL,
  `kurs` float DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `term_of_payment` varchar(50) NOT NULL,
  `jenis_po` varchar(25) NOT NULL,
  `flag_dp` tinyint(1) NOT NULL,
  `flag_pelunasan` tinyint(1) NOT NULL,
  `status` smallint(6) NOT NULL,
  `remarks` text,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po`
--

INSERT INTO `po` (`id`, `reff_id`, `no_po`, `tanggal`, `beli_sparepart_id`, `ppn`, `diskon`, `materai`, `currency`, `kurs`, `supplier_id`, `customer_id`, `term_of_payment`, `jenis_po`, `flag_dp`, `flag_pelunasan`, `status`, `remarks`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(41, 90, 'POSP-KMP.201904.0005', '2019-04-30', 47, 1, 0, 0, 'IDR', 0, 74, 0, 'TUNAI', 'Sparepart', 0, 0, 3, NULL, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(42, 91, 'POSP-KMP.201905.0001', '2019-05-02', 47, 0, 0, 0, 'IDR', 0, 389, 0, 'TUNAI', 'Sparepart', 0, 0, 3, NULL, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(43, 0, 'PO-KMP.201905.0002', '2019-05-02', 48, 0, 0, 0, 'IDR', 0, 305, 0, 'TUNAI', 'Sparepart', 0, 0, 3, NULL, '2019-05-02 03:05:04', 1, '2019-05-02 03:05:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `po_detail`
--

CREATE TABLE `po_detail` (
  `id` int(11) NOT NULL,
  `reff_id` int(11) NOT NULL,
  `po_id` int(11) NOT NULL,
  `beli_sparepart_detail_id` int(11) NOT NULL,
  `sparepart_id` int(11) NOT NULL,
  `rongsok_id` int(11) NOT NULL,
  `ampas_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `qty` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `bruto` int(11) NOT NULL,
  `netto` int(11) NOT NULL,
  `flag_lpb` tinyint(1) NOT NULL,
  `flag_dtr` tinyint(1) NOT NULL,
  `flag_dtbj` tinyint(1) NOT NULL,
  `flag_dtwip` tinyint(1) NOT NULL,
  `flag_dtt` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po_detail`
--

INSERT INTO `po_detail` (`id`, `reff_id`, `po_id`, `beli_sparepart_detail_id`, `sparepart_id`, `rongsok_id`, `ampas_id`, `jenis_barang_id`, `amount`, `qty`, `total_amount`, `bruto`, `netto`, `flag_lpb`, `flag_dtr`, `flag_dtbj`, `flag_dtwip`, `flag_dtt`) VALUES
(3, 188, 41, 85, 129, 0, 0, 0, 450000, 19, 8550000, 0, 0, 1, 0, 0, 0, 0),
(4, 189, 41, 86, 1705, 0, 0, 0, 650000, 10, 6500000, 0, 0, 1, 0, 0, 0, 0),
(5, 190, 42, 83, 51, 0, 0, 0, 1800000, 8, 14400000, 0, 0, 1, 0, 0, 0, 0),
(6, 191, 42, 84, 52, 0, 0, 0, 4000000, 4, 16000000, 0, 0, 1, 0, 0, 0, 0),
(7, 0, 43, 87, 8, 0, 0, 0, 50000, 25, 1250000, 0, 0, 1, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beli_sparepart`
--
ALTER TABLE `beli_sparepart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beli_sparepart_detail`
--
ALTER TABLE `beli_sparepart_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lpb`
--
ALTER TABLE `lpb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lpb_detail`
--
ALTER TABLE `lpb_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_detail`
--
ALTER TABLE `po_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beli_sparepart`
--
ALTER TABLE `beli_sparepart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `beli_sparepart_detail`
--
ALTER TABLE `beli_sparepart_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `lpb`
--
ALTER TABLE `lpb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `lpb_detail`
--
ALTER TABLE `lpb_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `po`
--
ALTER TABLE `po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `po_detail`
--
ALTER TABLE `po_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
