<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GudangRongsok extends CI_Controller{   
    function __construct(){
        parent::__construct();

        if($this->session->userdata('status') != "login"){
            redirect(base_url("index.php/Login"));
        }
    }
    
    function index(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang Rongsok";
        $data['content']   = "gudang_rongsok/index";

        $this->load->model('Model_beli_rongsok'); 
        $data['list_rongsok'] = $this->Model_beli_rongsok->show_data_rongsok()->result();

        $this->load->view('layout', $data);  
    }

    function spb_list(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        $data['content']= "ingot/spb_list";
        $this->load->model('Model_ingot');
        $data['list_data'] = $this->Model_ingot->spb_list()->result();

        $this->load->view('layout', $data);
    }

    function filter_spb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "ingot/spb_list";
            $this->load->model('Model_ingot');
            if($id == 0){
                $data['list_data'] = $this->Model_ingot->spb_list_filter_0()->result();
            }else if($id == 1){
                $data['list_data'] = $this->Model_ingot->spb_list_filter_1()->result();
            }

            $this->load->view('layout', $data);
        }else{
            redirect('index.php/Ingot/spb_list');
        }
    }

    function view_spb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "ingot/view_spb_v1";

            $this->load->model('Model_ingot');
            $data['myData'] = $this->Model_ingot->show_header_spb($id)->row_array();           
            $data['myDetail'] = $this->Model_ingot->show_detail_spb($id)->result(); 
            $data['detailSPBFulfilment'] = $this->Model_ingot->show_detail_spb_fulfilment_approved($id)->result();
            $data['detailSPB'] = $this->Model_ingot->show_detail_spb_fulfilment($id)->result();
            $data['apolo'] = $this->Model_ingot->show_apolo()->result();
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/Ingot/spb_list');
        }
    }

    function laporan_list(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

            $data['content']= "beli_rongsok/laporan_list";
            $this->load->model('Model_beli_rongsok'); 
            //$data['detailTanggal'] = $this->Model_beli_sparepart->show_laporan()->result();
            $data['list'] = $this->Model_beli_rongsok->show_laporan()->result();
            $this->load->view('layout', $data);   
    }

    function view_laporan(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['end'] = date("Y-m-t", strtotime($id));

            $data['content']= "beli_rongsok/view_laporan";
            $this->load->model('Model_beli_rongsok');
            $data['detailLaporan'] = $this->Model_beli_rongsok->show_view_laporan('RONGSOK',$id)->result();
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/BeliRongsok/laporan_list');
        }
    }

    function view_detail_laporan(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        $id_barang = $this->uri->segment(4);
        if($id){
            $group_id    = $this->session->userdata('group_id');
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $items = strval($id);
            $tgl=str_split($id,4);
            $tahun=$tgl[0];
            $bulan=$tgl[1];

            $data['tgl'] = array(
                'tahun' => $tahun,
                'bulan' => $bulan
            );

            $data['content']= "beli_rongsok/view_detail_laporan";
            $this->load->model('Model_beli_rongsok');
            $data['detailLaporan'] = $this->Model_beli_rongsok->show_laporan_detail($bulan,$tahun,$id_barang)->result();
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/BeliRongsok/laporan_list');
        }
    }

    function gudang_rongsok(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang Rongsok";
        $data['content']   = "beli_rongsok/gudang_rongsok";
        
        $this->load->model('Model_beli_rongsok');
        $data['list_data'] = $this->Model_beli_rongsok->gudang_rongsok_list()->result();
        
        $this->load->view('layout', $data);
    }

    function view_gudang_rongsok(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        $group_id    = $this->session->userdata('group_id');

        if($id){
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $data['judul']     = "Gudang Rongsok";
            $data['content']   = "beli_rongsok/view_gudang_rongsok";
            
            $this->load->model('Model_beli_rongsok');
            $data['list_data'] = $this->Model_beli_rongsok->view_gudang_rongsok($id)->result();
            
            $this->load->view('layout', $data);
        }else{
            redirect('index.php/BeliRongsok/gudang_rongsok');
        }
    }

    function kartu_stok(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');

        $rongsok_id = $_GET['r'];
        $start = date('Y-m-d', strtotime($_GET['ts']));
        $end = date('Y-m-d', strtotime($_GET['te']));

            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $data['judul']     = "Gudang Rongsok";

        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_gudang_fg');
        $data['rongsok'] = $this->Model_beli_rongsok->show_data_rongsok_detail($rongsok_id)->row_array();
        $data['start'] = $start;
        $data['end'] = $end;
        
        // $data['stok_before'] = $this->Model_beli_rongsok->get_stok_before($start,$end,$rongsok_id)->row_array();
        if($_GET['bl']==0){
            if($rongsok_id==0){
                $loop = $this->Model_gudang_fg->show_view_laporan('RONGSOK',$start)->result_array();
                foreach ($loop as $key => $value) {
                    $data['loop'][$key] = array('header'=>$loop[$key],'detailLaporan'=> $this->Model_beli_rongsok->show_kartu_stok($start,$end,$value['jenis_barang_id'])->result());
                }
                $this->load->view('gudang_rongsok/kartu_stok_all', $data);
            }else{
                $data['stok_before'] = $this->Model_gudang_fg->inventory_stok_before('RONGSOK',$start,$rongsok_id)->row_array();
                $data['detailLaporan'] = $this->Model_beli_rongsok->show_kartu_stok($start,$end,$rongsok_id)->result();
                $this->load->view('gudang_rongsok/kartu_stok', $data);
            }
        }else{
            $data['stok_before'] = $this->Model_gudang_fg->inventory_stok_before('RONGSOK',$start,$rongsok_id)->row_array();
            $data['detailLaporan'] = $this->Model_beli_rongsok->show_kartu_stok_palette($start,$end,$rongsok_id)->result();
            $this->load->view('gudang_rongsok/kartu_stok_palette', $data);
        }
    }

    // function print_laporan_bulanan(){
    //     $module_name = $this->uri->segment(1);
    //     $id = $this->uri->segment(3);
    //     if($id){
    //         $group_id    = $this->session->userdata('group_id');        
    //         if($group_id != 1){
    //             $this->load->model('Model_modules');
    //             $roles = $this->Model_modules->get_akses($module_name, $group_id);
    //             $data['hak_akses'] = $roles;
    //         }
    //         $data['group_id']  = $group_id;
    //         $this->load->helper('tanggal_indo');            
    //         $items = strval($id);
    //         $tgl=str_split($id,4);
    //         $tahun=$tgl[0];
    //         $bulan=$tgl[1];

    //         $tgl = $tahun.'/'.$bulan.'/01';
    //         // print_r($tgl); die();
    //         $data['tgl'] = array(
    //             'tahun' => $tahun,
    //             'bulan' => $bulan
    //         );

    //         $this->load->model('Model_beli_rongsok');
    //         $data['detailLaporan'] = $this->Model_beli_rongsok->show_laporan_barang($tgl,$bulan,$tahun)->result();
    //         $this->load->view("gudang_rongsok/print_laporan_bulanan", $data);
    //     }else{
    //         redirect('index.php/BeliRongsok/laporan_list');
    //     }
    // }

    function print_laporan_bulanan(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $this->load->helper('tanggal_indo');        

            $tgl=explode("-",$id);
            $tahun=$tgl[0];
            $bulan=$tgl[1];

            $tgl = $tahun.'-'.$bulan.'-01';

            $data['tgl'] = array(
                'tahun' => $tahun,
                'bulan' => $bulan
            );

            $this->load->model('Model_beli_rongsok');
            $data['detailLaporan'] = $this->Model_beli_rongsok->show_laporan_barang('RONGSOK',$id)->result();
            // print_r($data['detailLaporan']);die();
            $this->load->view("gudang_rongsok/print_laporan_bulanan", $data);
        }else{
            redirect('index.php/GudangRongsok/laporan_list');
        }
    }

    function print_permintaan_gudang(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');

        $start = date('Y/m/d', strtotime($_GET['ts']));
        $end = date('Y/m/d', strtotime($_GET['te']));

            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $data['judul']     = "Gudang Rongsok";

        $this->load->model('Model_beli_rongsok');
        $data['start'] = $start;
        $data['end'] = $end;

            $data['detailLaporan'] = $this->Model_beli_rongsok->permintaan_rongsok_dari_produksi($start,$end)->result();

            $this->load->view('gudang_rongsok/print_permintaan_gudang', $data);
    }

    function search_permintaan_gudang(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang Rongsok";
        $data['content']   = "gudang_rongsok/search_permintaan_gudang";

        $this->load->model('Model_beli_rongsok');

        $this->load->view('layout', $data);  
    }

    function print_permintaan_external(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');

        $start = date('Y/m/d', strtotime($_GET['ts']));
        $end = date('Y/m/d', strtotime($_GET['te']));

        $data['start'] = $start;
        $data['end'] = $end;

        $this->load->model('Model_beli_rongsok');
            $data['detailLaporan'] = $this->Model_beli_rongsok->permintaan_rongsok_external($start,$end)->result();

            $this->load->view('gudang_rongsok/print_permintaan_external', $data);
    }

    function search_permintaan_external(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang Rongsok";
        $data['content']   = "gudang_rongsok/search_permintaan_external";

        // $this->load->model('Model_beli_rongsok');

        $this->load->view('layout', $data);  

            $this->load->helper('tanggal_indo');            
            // $items = strval($id);
            // $tgl=str_split($id,4);
            // $tahun=$tgl[0];
            // $bulan=$tgl[1];

            // $tgl = $tahun.'/'.$bulan.'/01';

            // $data['tgl'] = array(
            //     'tahun' => $tahun,
            //     'bulan' => $bulan
            // );

            // $this->load->model('Model_beli_rongsok');
            // $data['detailLaporan'] = $this->Model_beli_rongsok->show_laporan_barang_detail($tgl,$bulan,$tahun)->result();
            // $this->load->view("gudang_rongsok/print_laporan_bulanan_detail", $data);
    }

    function proses_inventory(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $jb = $this->uri->segment(3);
        $tgl_proses = $this->uri->segment(4);

        $this->db->trans_start();
        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_gudang_fg');

            $tgl=explode("-",$tgl_proses);
            $tahun=$tgl[1];
            $bulan=$tgl[0];

            $start = $tahun.'-'.$bulan.'-01';

                if($bulan==01){
                  $bulan_b = 12;
                  $tahun_b = $tahun-1;
                } else {
                  $bulan_b= intval($bulan)-1;
                  if($bulan_b < 10){
                    $bulan_b= '0'.$bulan_b;
                    $tahun_b= $tahun;
                  }
                  $tahun_b = $tahun;
                }

            $before = $tahun_b.'-'.$bulan_b.'-01';

            // echo $before;die();
                if($bulan==12){
                  $bulan = 01;
                  $tahun2 = $tahun+1;
                } else {
                  $bulan2= intval($bulan)+1;
                  if($bulan2 < 10){
                    $bulan2= '0'.$bulan2;
                    $tahun2= $tahun;
                  }
                  $tahun2 = $tahun;
                }

            $end = $tahun2.'-'.$bulan2.'-01';

            $jenis_barang = $this->Model_beli_rongsok->show_data_rongsok()->result();
            $this->db->where(array(
                'tanggal' => $start,
                'jenis_barang' => $jb
            ));
            $this->db->delete('inventory');
            $no = 0;
            foreach ($jenis_barang as $key => $value) {
                $stok_before = $this->Model_gudang_fg->inventory_stok_before($jb,$before,$value->id)->row_array();
                $t = 1;
                if(empty($stok_before)){
                    $stok_before = $this->Model_beli_rongsok->get_stok_before($start,$end,$value->id)->row_array();
                    $t = 2;
                }
                // echo $t;
                $trx = $this->Model_beli_rongsok->show_kartu_stok_inventory($start,$end,$value->id)->row_array();
                if(!empty($stok_before) || !empty($trx)){
                    // echo $value->jenis_barang.' | '.$stok_before['netto_masuk'].$stok_before['netto_keluar'].' | ';
                    // echo $trx['netto_masuk'].' - '.$trx['netto_keluar'].'<br>';
                    if($t==1){
                        $stok_awal = $stok_before['stok_akhir'];
                    }else{
                        $stok_awal = $stok_before['netto_masuk']-$stok_before['netto_keluar'];
                    }
                    // if(!empty($stok_before)){
                    //     //stok awal
                    //     $this->db->insert('inventory', array(
                    //         'jenis_barang'=>$jb,
                    //         'bulan'=>$bulan,
                    //         'tahun'=>$tahun,
                    //         'tanggal'=>$start,
                    //         'jenis_barang_id'=>$value->id,
                    //         'qty'=>$stok_before['qty'],
                    //         'netto'=>$stok_awal,
                    //         'created_at'=>$tanggal,
                    //         'created_by'=>$user_id
                    //     ));
                    // }

                    $stok_awal_next_month = $stok_awal + $trx['netto_masuk'] - $trx['netto_keluar'];
                    // echo $value->jenis_barang.' | '.$stok_awal.' | '.$trx['netto_masuk'].' & '.$trx['netto_keluar'].' | '.$stok_awal_next_month.'<br>';
                    if($stok_awal > 0 || $trx['netto_masuk'] > 0 || $trx['netto_keluar'] > 0){
                        //stok akhir
                        $this->db->insert('inventory', array(
                            'jenis_barang'=>$jb,
                            'bulan'=>$bulan,
                            'tahun'=>$tahun,
                            'tanggal'=>$start,
                            'jenis_barang_id'=>$value->id,
                            'qty'=> 0,
                            'stok_awal'=>$stok_awal,
                            'netto_masuk'=>((empty($trx['netto_masuk']))? 0: $trx['netto_masuk']),
                            'netto_keluar'=>((empty($trx['netto_keluar']))? 0: $trx['netto_keluar']),
                            'stok_akhir'=>$stok_awal_next_month,
                            'created_at'=>$tanggal,
                            'created_by'=>$user_id
                        ));
                    }
                }
            }
// die();
        if($this->db->trans_complete()){
            $this->session->set_flashdata('flash_msg', 'Laporan Rongsok berhasil diproses !');
            redirect(base_url('index.php/GudangRongsok/laporan_list'));
        } else {
            $this->session->set_flashdata('flash_msg', 'Laporan Rongsok gagal diproses, silahkan dicoba kembali!');
            redirect(base_url('index.php/GudangRongsok/laporan_list'));
        }
    }
}