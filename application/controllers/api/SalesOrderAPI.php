<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class SalesOrderAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
	}

    public function so_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

            if($data['category'] == 'FG'){
                $this->db->insert('t_spb_fg', $data['spb']);
                $insert_id = $this->db->insert_id();
            }else if($data['category'] == 'WIP'){
                $this->db->insert('t_spb_wip', $data['spb']);
                $insert_id = $this->db->insert_id();
            }else if($data['category'] == 'RONGSOK'){
                $this->db->insert('spb', $data['spb']);
                $insert_id = $this->db->insert_id();
            }else if($data['category'] == 'AMPAS'){
                $this->db->insert('t_spb_ampas', $data['spb']);
                $insert_id = $this->db->insert_id();
            }else if($data['category'] == 'LAIN'){
                $insert_id = '0';
                $tgl_po = '0000-00-00';
            }

        //setting data SO
        unset($data['so']['flag_ppn']);
        unset($data['so']['created']);
        unset($data['so']['created_by']);
        
        $this->db->insert('sales_order', $data['so']);
        $so_id = $this->db->insert_id();

        //setting data TSO
        unset($data['tso']['created_at']);
        unset($data['tso']['created_by']);
        $data['tso']['id'] = $so_id;
        $data['tso']['so_id'] = $so_id;
        $data['tso']['no_spb'] = $insert_id;
        $this->db->insert('t_sales_order', $data['tso']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function so_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select so.id, tso.no_spb, so.tanggal from sales_order so
            left join t_sales_order tso on tso.id = so.id where so.reff1 =".$data['so_id'])->row_array();
        // print_r($so);
        // die();
        $this->db->where('id', $so['id']);
        $this->db->update('sales_order', $data['so']);
        
        $this->db->where('id', $so['id']);
        $this->db->update('t_sales_order', $data['tso']);

        $this->db->where('t_so_id', $so['id']);
        $this->db->delete('t_sales_order_detail');

            if($data['category'] == 'FG'){

                $this->db->where('t_spb_fg_id', $so['no_spb']);
                $this->db->delete('t_spb_fg_detail');
                foreach ($data['details'] as $i => $item){
                    $this->db->insert('t_spb_fg_detail', array(
                        'reff1'=>$item['no_spb_detail'],
                        't_spb_fg_id'=>$so['no_spb'],
                        'tanggal'=>$so['tanggal'],
                        'jenis_barang_id'=>$item['jenis_barang_id'],
                        'uom'=>$item['uom'],
                        'netto'=>$item['netto'],
                        'keterangan'=>'SALES ORDER'
                    ));
                    $insert_id = $this->db->insert_id();

                    $this->db->insert('t_sales_order_detail', array(
                        'reff1'=>$item['id'],
                        't_so_id'=>$so['id'],
                        'no_spb_detail'=>$insert_id,
                        'jenis_barang_id'=>$item['jenis_barang_id'],
                        'nama_barang_alias'=>$item['nama_barang_alias'],
                        'amount'=>$item['amount'],
                        'qty'=>$item['qty'],
                        'total_amount'=>$item['total_amount'],
                        'bruto'=>$item['bruto'],
                        'netto'=>$item['netto']
                    ));
                }

            }else if($data['category'] == 'AMPAS'){
                $dataC = array(
                    't_spb_ampas_id' => $spb,
                    'tanggal' => $tanggal,
                    'jenis_barang_id' => $this->input->post('barang_id'),
                    'uom' => $this->input->post('uom'),
                    'netto' => $this->input->post('netto'),
                    'keterangan' => 'SALES ORDER'
                );
                $this->db->insert('t_spb_ampas_detail', $dataC);
                $insert_id = $this->db->insert_id();
            }else if($data['category'] == 'WIP'){
                $this->db->where('t_spb_wip_id', $so['no_spb']);
                $this->db->delete('t_spb_wip_detail');
                foreach ($data['details'] as $i => $item){
                    $this->db->insert('t_spb_wip_detail', array(
                        'reff1'=>$item['no_spb_detail'],
                        't_spb_wip_id'=>$so['no_spb'],
                        'jenis_barang_id'=>$item['jenis_barang_id'],
                        'qty'=>$item['qty'],
                        'uom'=>$item['uom'],
                        'berat'=>$item['netto'],
                        'keterangan'=>'SALES ORDER'
                    ));
                    $insert_id = $this->db->insert_id();

                    $this->db->insert('t_sales_order_detail', array(
                        'reff1'=>$item['id'],
                        't_so_id'=>$so['id'],
                        'no_spb_detail'=>$insert_id,
                        'jenis_barang_id'=>$item['jenis_barang_id'],
                        'nama_barang_alias'=>$item['nama_barang_alias'],
                        'amount'=>$item['amount'],
                        'qty'=>$item['qty'],
                        'total_amount'=>$item['total_amount'],
                        'bruto'=>$item['bruto'],
                        'netto'=>$item['netto']
                    ));
                }

            }else if($data['category'] == 'RONGSOK'){
                $this->db->where('spb_id', $so['no_spb']);
                $this->db->delete('spb_detail');
                foreach ($data['details'] as $i => $item){
                    $this->db->insert('spb_detail', array(
                        'reff1'=>$item['no_spb_detail'],
                        'spb_id'=>$so['no_spb'],
                        'rongsok_id'=>$item['jenis_barang_id'],
                        'qty'=>$item['qty'],
                        'line_remarks'=>'SALES ORDER'
                    ));
                    $insert_id = $this->db->insert_id();

                    $this->db->insert('t_sales_order_detail', array(
                        'reff1'=>$item['id'],
                        't_so_id'=>$so['id'],
                        'no_spb_detail'=>$insert_id,
                        'jenis_barang_id'=>$item['jenis_barang_id'],
                        'nama_barang_alias'=>$item['nama_barang_alias'],
                        'amount'=>$item['amount'],
                        'qty'=>$item['qty'],
                        'total_amount'=>$item['total_amount'],
                        'bruto'=>$item['bruto'],
                        'netto'=>$item['netto']
                    ));
                }
            }else if($data['category'] == 'LAIN'){
                $insert_id = 0;
                foreach ($data['details'] as $i => $item) {
                    $this->db->insert('t_sales_order_detail', array(
                        'reff1'=>$item['id'],
                        't_so_id'=>$so['id'],
                        'no_spb_detail'=>$insert_id,
                        'jenis_barang_id'=>$item['jenis_barang_id'],
                        'nama_barang_alias'=>$item['nama_barang_alias'],
                        'amount'=>$item['amount'],
                        'qty'=>$item['qty'],
                        'total_amount'=>$item['total_amount'],
                        'bruto'=>$item['bruto'],
                        'netto'=>$item['netto']
                    ));
                }
            }

        // foreach ($data['details'] as $i => $item) {
        //     $data['details'][$i]['t_so_id'] = $so['id'];
        //     $data['details'][$i]['reff1'] = $data['details'][$i]['id'];
        //     unset($data['details'][$i]['id']);
        // }

        // $this->db->insert_batch('t_sales_order_detail', $data['details']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    

    public function so_del_get(){
        $id = $this->get('id');

        if($id == 0){
            $this->response([
                'status' => false,
                'message' => 'ID belum dikirim'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $data = $this->db->query('select id, no_spb, jenis_barang from t_sales_order where reff1='.$id)->row_array();
            $this->db->delete('sales_order', ['id'=> $data['id']]);
            $this->db->delete('t_sales_order', ['id' => $data['id']]);

            if($data['jenis_barang'] == 'FG'){
                $this->db->where('id', $data['no_spb']);
                $this->db->delete('t_spb_fg');
            }else if($data['jenis_barang'] == 'WIP'){
                $this->db->where('id', $data['no_spb']);
                $this->db->delete('t_spb_wip');
            }else if($data['jenis_barang'] == 'RONGSOK'){
                $this->db->where('id', $data['no_spb']);
                $this->db->delete('spb');
            }else if($data['jenis_barang'] == 'AMPAS'){
                $this->db->where('id', $data['no_spb']);
                $this->db->delete('t_spb_ampas');
            }

            if($this->db->affected_rows() > 0){
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'Berhasil di delete'
                ],REST_Controller::HTTP_OK);
            }else{  
                $this->response([
                    'status' => false,
                    'message' => 'ID tidak ditemukan'
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function sj_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select id, no_spb, jenis_barang from t_sales_order where reff1 =".$data['tsj']['sales_order_id'])->row_array();
        // print_r($so);die();
        //update flag sj

            $this->db->where('id', $so['id']);
            $this->db->update('sales_order', array(
                'flag_sj' => $data['flag_sj'],
                'flag_invoice' => 0
            ));

        //setting data SJ
        $data_tsj = array(
            'reff1' => $data['tsj']['id'],
            'no_surat_jalan' => $data['tsj']['no_surat_jalan'],
            'sales_order_id' => $so['id'],
            'po_id' => $data['tsj']['po_id'],
            'spb_id' => $data['tsj']['spb_id'],
            'retur_id' => $data['tsj']['retur_id'],
            'inv_id' => $data['tsj']['inv_id'],
            'status' => $data['tsj']['status'],
            'tanggal' => $data['tsj']['tanggal'],
            'jenis_barang' => $data['tsj']['jenis_barang'],
            'm_customer_id' => $data['tsj']['m_customer_id'],
            'no_kendaraan' => $data['tsj']['no_kendaraan'],
            'supir' => $data['tsj']['supir'],
            'remarks' => $data['tsj']['remarks']
        );
        $this->db->insert('t_surat_jalan', $data_tsj);
        $sj_id = $this->db->insert_id();

        //setting insert Gudang FG
        foreach ($data['gudang'] as $k => $v) {

        if($so['jenis_barang']=='FG'){

            $this->db->where('id', $so['no_spb']);
            $this->db->update('t_spb_fg', array(
                'status'=>$data['tsj']['status_spb']
            ));

            //cek mau insert produksi_fg
            // if($data['gudang'][$k]['id_prd']){
            //     $cek_pf = $this->db->query("select id from produksi_fg where reff1 =".$data['gudang'][$k]['id_prd'])->row_array();
            //     if(empty($cek_pf)){
            //         $this->db->insert('produksi_fg', array(
            //             'reff1' => $data['gudang'][$k]['id_prd'],
            //             'no_laporan_produksi' => $data['gudang'][$k]['no_laporan_produksi'],
            //             'tanggal' => $data['gudang'][$k]['tgl_prd'],
            //             'flag_result' => $data['gudang'][$k]['flag_result'],
            //             'remarks' => $data['gudang'][$k]['remarks_prd'],
            //             'jenis_barang_id' => $data['gudang'][$k]['jb_prd'],
            //             'jenis_packing_id' => $data['gudang'][$k]['jenis_packing_id']
            //         ));
            //         $pf_id = $this->db->insert_id();
            //     }else{
            //         $pf_id = $cek_pf['id'];
            //     }
            // }else{
            //     $pf_id = 0;
            // }

            //insert produksi_fg_detail
            // if($pf_id > 0){
            //     $this->db->insert('produksi_fg_detail', array(
            //         'produksi_fg_id' => $pf_id,
            //         'tanggal' => $data['gudang'][$k]['tgl_prd'],
            //         'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
            //         'no_produksi' => $data['gudang'][$k]['no_produksi'],
            //         'bruto' => $data['gudang'][$k]['bruto'],
            //         'netto' => $data['gudang'][$k]['netto'],
            //         'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
            //         'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
            //         'keterangan' => $data['gudang'][$k]['remarks_prd']
            //     ));
            // }

            $a_date = $data['tsj']['tanggal'];
            // $date = date("Y-m-t", strtotime($a_date));//TANGGAL TERAKHIR
            $hari = date('d', strtotime($a_date));//HARI

            if($hari <= 10){
                $last_date = date("Y-m-j", strtotime("last day of previous month"));
                $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
            }else{
                $last_date = $a_date;
                $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
            }
            // echo $last_date;die();
            //cek mau insert t_bpb_fg
            $cek_bpb = $this->db->query("select id, no_bpb_fg from t_bpb_fg where tanggal ='".$last_date."' AND jenis_barang_id = ".$v['sj_jb'])->row_array();
            // print_r($cek_bpb);die();
            if(empty($cek_bpb)){
                $this->db->insert('t_bpb_fg', array(
                    'no_bpb_fg' => 'BPB-KMP.'.$tgl_bpb.'.',
                    'tanggal' => $last_date,
                    'produksi_fg_id' => 0,
                    'jenis_barang_id' => $v['sj_jb'],
                    'status' => 1,
                    'keterangan' => $data['gudang'][$k]['ket_bpb']
                ));
                $bpb_id = $this->db->insert_id();
            }else{
                $bpb_id = $cek_bpb['id'];
            }

            //insert t_bpb_fg_detail
            if($bpb_id > 0){
                $cek_bpb_detail = $this->db->query("select id from t_bpb_fg_detail where no_packing_barcode ='".$v['no_packing']."'")->row_array(); 
                if(empty($cek_bpb_detail)){
                    $this->db->insert('t_bpb_fg_detail', array(
                        'reff1' => $data['gudang'][$k]['id_bpb_d'],
                        't_bpb_fg_id' => $bpb_id,
                        'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                        'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
                        'no_produksi' => $data['gudang'][$k]['no_produksi'],
                        'bruto' => $data['gudang'][$k]['bruto'],
                        'netto' => $data['gudang'][$k]['netto'],
                        'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                        'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                        'flag_taken' => 0
                    ));
                    $bpb_detail_id = $this->db->insert_id();
                }else{
                        $this->db->where('id', $cek_bpb_detail['id']);
                        $this->db->update('t_bpb_fg_detail', array(
                            'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                            't_bpb_fg_id'=> $bpb_id
                        ));
                    $bpb_detail_id = $cek_bpb_detail['id'];
                }
            }else{
                $bpb_detail_id = 0;
            }

            //insert t_gudang_fg
                $cek_gudang_fg = $this->db->query("select id from t_gudang_fg where no_packing ='".$v['no_packing']."'")->row_array();
                $spb = $this->db->query("select id, no_spb from t_spb_fg where reff1 =".$data['gudang'][$k]['t_spb_fg_id'])->row_array();
                if(empty($cek_gudang_fg)){
                    $this->db->insert('t_gudang_fg', array(
                            'reff1' => $data['gudang'][$k]['id'],
                            'tanggal'=> $last_date,
                            'jenis_trx' =>1, // 0 masuk
                            'flag_taken'=>1, // 0 belum diambil
                            't_bpb_fg_id' => $bpb_id,
                            't_bpb_fg_detail_id' => $bpb_detail_id,
                            't_spb_fg_id' => $so['no_spb'],
                            't_sj_id' => $sj_id,
                            'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                            'nomor_SPB' => $spb['no_spb'],
                            'nomor_BPB' => 'BPB-KMP.'.$tgl_bpb.'.',
                            'no_produksi' => $data['gudang'][$k]['no_produksi'],
                            'no_packing' => $data['gudang'][$k]['no_packing'],
                            'netto' => $data['gudang'][$k]['netto'],
                            'bruto' => $data['gudang'][$k]['bruto'],
                            'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                            'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                            'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                            'keterangan' => $data['gudang'][$k]['keterangan'],
                            'tanggal_masuk' => $last_date,
                            'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar'],
                            'created_by'=> 0,
                            'created_at' => 0
                        ));
                    $gudang_id = $this->db->insert_id();
                }else{
                    $this->db->where('id', $cek_gudang_fg['id']);
                    $this->db->update('t_gudang_fg', array(
                        'jenis_trx'=>1,
                        'flag_taken'=>1,
                        'tanggal'=> $last_date,
                        't_bpb_fg_id' => $bpb_id,
                        't_bpb_fg_detail_id' => $bpb_detail_id,
                        't_spb_fg_id' => $so['no_spb'],
                        'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                        'nomor_SPB' => $spb['no_spb'],
                        'nomor_BPB' => 'BPB-KMP.'.$tgl_bpb.'.',
                        't_spb_fg_id'=> $so['no_spb'],
                        't_sj_id' => $sj_id,
                        'tanggal_masuk' => $last_date,
                        'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar']
                    ));
                    $gudang_id = $cek_gudang_fg['id'];
                }

            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $data['gudang'][$k]['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $gudang_id,
                'jenis_barang_id'=> $data['gudang'][$k]['sj_jb'],
                'jenis_barang_alias'=> 0,
                'no_packing'=> $data['gudang'][$k]['no_packing'],
                'qty'=> $data['gudang'][$k]['jb_qty'],
                'bruto'=> $data['gudang'][$k]['bruto'],
                'netto'=> $data['gudang'][$k]['netto'],
                'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                'line_remarks'=> $data['gudang'][$k]['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }elseif($so['jenis_barang']=='WIP'){
            $this->db->where('id', $so['no_spb']);
            $this->db->update('t_spb_wip', array(
                'status'=>$data['tsj']['status_spb']
            ));

            $spb_detail = $this->db->query("select id from t_spb_wip_detail where t_spb_wip_id =".$so['no_spb']." and jenis_barang_id=".$v['jenis_barang_id'])->row_array();

            $this->db->insert('t_spb_wip_fulfilment', array(
                't_spb_wip_id'=>$so['no_spb'],
                't_spb_wip_detail_id'=>$spb_detail['id'],
                'jenis_barang_id'=>$v['jenis_barang_id'],
                'qty'=>$v['qty'],
                'berat'=>$v['berat'],
                'keterangan'=>$v['keterangan']
            ));

            //setting t_gudang_wip
            $this->db->insert('t_gudang_wip', array(
                'reff1'=>$v['id'],
                'tanggal'=>$v['tanggal'],
                'flag_taken'=>$v['flag_taken'],
                'jenis_trx'=>$v['jenis_trx'],
                't_hasil_wip_id'=>$v['t_hasil_wip_id'],
                'jenis_barang_id'=>$v['jenis_barang_id'],
                't_spb_wip_id'=>$so['no_spb'],
                't_spb_wip_detail_id'=>$spb_detail['id'],
                't_bpb_wip_detail_id'=>$v['t_bpb_wip_detail_id'],
                'qty'=>$v['qty'],
                'uom'=>$v['uom'],
                'berat'=>$v['berat'],
                'keterangan'=>$v['keterangan']
            ));
            $gudang_id = $this->db->insert_id();

            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $data['gudang'][$k]['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $gudang_id,
                'jenis_barang_id'=> $data['gudang'][$k]['jenis_barang_id'],
                'jenis_barang_alias'=> $data['gudang'][$k]['jenis_barang_alias'],
                'no_packing'=> 0,
                'qty'=> $data['gudang'][$k]['jb_qty'],
                'bruto'=> 0,
                'netto'=> $data['gudang'][$k]['berat'],
                'nomor_bobbin'=> 0,
                'line_remarks'=> $data['gudang'][$k]['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }elseif($so['jenis_barang']=='RONGSOK'){
            $this->db->where('id', $so['no_spb']);
            $this->db->update('spb', array(
                'status'=>$data['tsj']['status_spb']
            ));

            //NOTE ADA DI NOTE ROOT FOLDER!!!!
            // cek insert dtr pertama udah apa belum
                $cek_dtr = $this->db->query("select dtr.id, ttr.id as id_ttr from dtr left join ttr on ttr.dtr_id = dtr.id where dtr.type =".$sj_id)->row_array();
                if(empty($cek_dtr)){
                    $nomor = substr($data['tsj']['no_surat_jalan'],7,11);
                    // echo $nomor;
                    // die();
                    $this->db->insert('dtr', array(
                        'type' => $sj_id,
                        'no_dtr' => 'DTR-RSK.'.$nomor,
                        'tanggal' => $data['tsj']['tanggal'],
                        'po_id' => $data['tsj']['po_id'],
                        'so_id' => 0,
                        'retur_id' => $data['tsj']['retur_id'],
                        'prd_id' => 0,
                        'flag_taken' => 0,
                        'supplier_id' => 822, //SUPPLIER REPACKING
                        'customer_id' => 0,
                        'jenis_barang' => 'RONGSOK',
                        'remarks' => '',
                        'status' => 1
                    ));
                    $dtr_id = $this->db->insert_id();

                    $this->db->insert('ttr', array(
                        'reff2' => $sj_id,
                        'no_ttr' => 'BPB-RSK.'.$nomor,
                        'tanggal' => $data['tsj']['tanggal'],
                        'no_sj' => '',
                        'dtr_id' => $dtr_id,
                        'jmlh_afkiran' => 0,
                        'jmlh_pengepakan' => 0,
                        'jmlh_lain' => 0,
                        'remarks' => 0,
                        'flag_bayar' => 0,
                        'flag_produksi' => 0,
                        'ttr_status' => 1
                    ));
                    $ttr_id = $this->db->insert_id();
                }else{
                    $dtr_id = $cek_dtr['id'];
                    $ttr_id = $cek_dtr['id_ttr'];
                }

                //insert produksi_fg_detail
                    //CEK DTR DETAILNYA UDAH ADA APA BELUM
                $cek_dd = $this->db->query("select dd.id, td.id as id_ttr from dtr_detail dd left join ttr_detail td on td.dtr_detail_id = dd.id where dd.reff1 =".$v['id'])->row_array();

                if(empty($cek_dd['id'])){
                    $this->db->insert('dtr_detail', array(
                        'reff1'=> $v['id'],
                        'dtr_id' => $dtr_id,
                        'po_detail_id' => 0,
                        'so_id' => $so['id'],
                        'retur_id' => 0,
                        'rongsok_id' => $v['rongsok_id'],
                        'qty' => $v['qty'],
                        'bruto' => $v['bruto'],
                        'berat_palette' => $v['berat_palette'],
                        'netto' => $v['netto'],
                        'netto_resmi' => $v['netto_resmi'],
                        'no_pallete' => $v['no_pallete'],
                        'tanggal_masuk' => $v['tanggal_masuk'],
                        'tanggal_keluar'=> $v['tanggal_keluar'],
                        'flag_taken'=> 1,
                        'flag_sj'=> $sj_id,
                        'flag_resmi'=> 1,
                        'line_remarks' => ''
                    ));
                    $dtr_detail_id = $this->db->insert_id();

                    $this->db->insert('ttr_detail', array(
                        'reff1'=> $v['id'],
                        'ttr_id'=> $ttr_id,
                        'dtr_detail_id'=> $dtr_detail_id,
                        'rongsok_id'=> $v['rongsok_id'],
                        'ampas_id'=> 0,
                        'qty' => $v['qty'],
                        'bruto' => $v['bruto'],
                        'netto' => $v['netto'],
                        'line_remarks' => ''
                    ));
                }else{
                    $dtr_detail_id = $cek_dd['id'];
                    $ttr_detail_id = $cek_dd['id_ttr'];
                }

                    $this->db->insert('spb_detail_fulfilment',array(
                        'spb_id'=>$so['no_spb'],
                        'dtr_detail_id'=>$dtr_detail_id,
                        'netto'=> $v['netto'],
                        'ttr_id'=>$ttr_id
                    ));

            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $v['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $dtr_detail_id,
                'jenis_barang_id'=> $v['rongsok_id'],
                'jenis_barang_alias'=> $v['jenis_barang_alias'],
                'no_packing'=> $v['no_pallete'],
                'qty'=> $v['jb_qty'],
                'bruto'=> $v['bruto'],
                'berat'=> $v['berat_palette'],
                'netto'=> $v['netto'],
                'nomor_bobbin'=> $v['nomor_bobbin'],
                'line_remarks'=> $v['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }elseif($so['jenis_barang'] == 'LAIN'){
            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $v['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> 0,
                'jenis_barang_id'=> $v['sj_jb'],
                'jenis_barang_alias'=> $v['jenis_barang_alias'],
                'no_packing'=> $v['no_packing'],
                'qty'=> $v['jb_qty'],
                'bruto'=> $v['bruto'],
                'berat'=> $v['berat'],
                'netto'=> $v['netto'],
                'nomor_bobbin'=> $v['nomor_bobbin'],
                'line_remarks'=> $v['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }//IF JENIS BARANG
        }
        // echo $this->db->trans_status(); die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $get = $this->db->query("select id, sales_order_id from t_surat_jalan where reff1 =".$data['id_sj'])->row_array();
     
        $this->db->where('id', $get['id']);
        $this->db->update('t_surat_jalan', $data['header']);

        foreach ($data['details'] as $v) {
            if($v['id_tsj_detail']!=''){
                $get_d = $this->db->query("select id from t_surat_jalan_detail where reff1 =".$v['id_tsj_detail'])->row_array();
                $this->db->where('id',$get_d['id']);
                $this->db->update('t_surat_jalan_detail', array(
                    'jenis_barang_alias'=>$v['barang_alias_id']
                ));
            }
        }
        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_revisi_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        foreach ($data as $v) {
            if($v['id']!=''){
                $get_d = $this->db->query("select id from t_surat_jalan_detail where reff1 =".$v['id'])->row_array();
                // print_r($get_d);
                $this->db->where('id',$get_d['id']);
                $this->db->update('t_surat_jalan_detail', array(
                    'bruto'=> $v['bruto'],
                    'berat'=> $v['berat'],
                    'netto'=> $v['netto_r']
                ));

                $get_dtr = $this->db->query("select id from dtr_detail where reff1 =".$v['gudang_id'])->row_array();
                // print_r($get_dtr);
                $this->db->where('id', $get_dtr['id']);
                $this->db->update('dtr_detail', array(
                    'bruto'=> $v['bruto'],
                    'berat_palette'=> $v['berat'],
                    'netto'=> $v['netto_r']
                ));

                $this->db->where('dtr_detail_id', $get_dtr['id']);
                $this->db->update('ttr_detail', array(
                    'bruto'=> $v['bruto'],
                    'netto'=> $v['netto_r']
                ));

                $this->db->where('dtr_detail_id', $get_dtr['id']);
                $this->db->update('spb_detail_fulfilment', array(
                    'netto'=> $v['netto_r']
                ));
            }
        }
        // echo $this->db->trans_status();die();    

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}