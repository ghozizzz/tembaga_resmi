<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Testapi extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_jenis_barang');
	}

	public function index_get(){
        $jenis_barang = $this->Model_jenis_barang->list_data()->result();

        if($jenis_barang){
	        $this->response([
	            'status' => true,
	            'data' => $jenis_barang
	        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
	  	}
    }

    public function index_post(){
    	$data = [
    		'jenis_barang' => $this->post('jenis_barang'),
    		'kode' => $this->post('kode'),
    		'category' => $this->post('category'),
    		'uom' => $this->post('uom')
    	];

    	if($this->Model_jenis_barang->insert_jenis_barang($data) > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah'
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function index_put(){
    	$id = $this->put('id');

    	$data = [
    		'jenis_barang' => $this->put('jenis_barang'),
    		'kode' => $this->put('kode'),
    		'category' => $this->put('category'),
    		'uom' => $this->put('uom')
    	];

    	if($this->Model_jenis_barang->update_jenis_barang($data, $id) > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil mengubah data'
    		],REST_Controller::HTTP_NO_CONTENT);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal mengubah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }
}