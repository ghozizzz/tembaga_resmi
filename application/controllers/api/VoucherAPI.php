<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class VoucherAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_jenis_barang');
	}

    public function vc_add_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $this->db->insert('f_kas', $data['header']);
        $id_fk = $this->db->insert_id();
        // print_r($data['details']);
        // echo $id_fk;
        foreach ($data['details'] as $key => $value) {
            // $data[$value] = $ttr_id;
            $data['details'][$key]['id_fk'] = $id_fk;
            $this->db->insert('voucher', $data['details'][$key]);
        }

        // print("<pre>".print_r($data['details'],true)."</pre>");
        // $this->db->insert_batch('voucher', $data['details']);
        // die();
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function vc_del_get(){
        $id = $this->get('id');

        $this->db->trans_start();
        $get = $this->db->query("select fk.id, v.id as id_vc, v.po_id FROM f_kas fk
                left join voucher v on v.id_fk = fk.id
                WHERE fk.reff1 =".$id." limit 1")->row_array();

            $this->db->delete('voucher', ['id_fk' => $get['id']]);
            $this->db->delete('f_kas', ['id' => $get['id']]);

            if($get['po_id']>0){
                $this->db->where('id', $get['po_id']);
                $this->db->update('po', array(
                    'status'=>3,
                    'flag_pelunasan'=>0
                ));
            }
        // print_r($get);
        // die();

            if($this->db->trans_complete()){
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'Berhasil di delete'
                ],REST_Controller::HTTP_OK);
            }else{  
                $this->response([
                    'status' => false,
                    'message' => 'ID tidak ditemukan'
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
    }
}