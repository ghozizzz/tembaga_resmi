<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class BeliFinishGoodAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_jenis_barang');
	}

	public function po_post(){
    	$data = [
            'reff1'=> $this->post('reff1'),
            'no_po'=> $this->post('no_po'),
            'tanggal'=> $this->post('tanggal'),
            'ppn'=>$this->post('ppn'),
            'diskon'=>$this->post('diskon'),
            'materai'=>$this->post('materai'),
            'currency'=>$this->post('currency'),
            'kurs'=>$this->post('kurs'),
            'supplier_id'=>$this->post('supplier_id'),
            'term_of_payment'=>$this->post('term_of_payment'),
            'jenis_po'=>$this->post('jenis_po')
        ];

    		$this->db->insert('po', $data);
        	$id = $this->db->insert_id();

    	if($id > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah',
    			'id'=> $id
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function po_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $get = $this->db->query("select id from po where reff1 =".$data['po_id'])->row_array();

        $this->db->where('id', $get['id']);
        $this->db->update('po', $data['master']);
        
        $this->db->where('po_id', $get['id']);
        $this->db->delete('po_detail');

        foreach ($data['details'] as $i => $item) {
            $data['details'][$i]['po_id'] = $get['id'];
            $data['details'][$i]['reff1'] = $data['details'][$i]['id'];
            unset($data['details'][$i]['id']);
        }

        if($this->db->insert_batch('po_detail', $data['details'])){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtbj_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $po = $this->db->query("select id, status from po where reff1 =".$data['po_id'])->row_array();
        if($po['status'] != $data['po']['status']){
            $this->db->where('id', $po['id']);
            $this->db->update('po', $data['po']);
        }

        $data['dtbj']['reff1'] = $data['dtbj']['id'];
        $data['dtbj']['po_id'] = $po['id'];
        unset($data['dtbj']['id']);
        unset($data['dtbj']['flag_ppn']);

        $this->db->insert('dtbj', $data['dtbj']);
        $dtbj_id = $this->db->insert_id();

        $dtbj_detail = [];
        foreach ($data['details'] as $i => $v){
            $data['details'][$i]['dtbj_id'] = $dtbj_id;
            $get_detail = $this->db->query("select id from po_detail where reff1=".$v['po_detail_id'])->row_array();
            $data['details'][$i]['po_detail_id'] = $get_detail['id'];
            $data['details'][$i]['reff1'] = $v['id'];
            unset($data['details'][$i]['id']);
        }

        $this->db->insert_batch('dtbj_detail', $data['details']);

        $this->db->insert('t_bpb_fg', $data['data_bpb']);
        $bpb_fg_id = $this->db->insert_id();

        $bpb_detail = [];
        foreach ($data['details_bpb'] as $key => $value) {
            $data['details_bpb'][$key]['t_bpb_fg_id'] = $bpb_fg_id;
            $data['details_bpb'][$key]['reff1'] = $value['id'];
            unset($data['details_bpb'][$key]['id']);
        }

        $this->db->insert_batch('t_bpb_fg_detail', $data['details_bpb']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function voucher_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $po = $this->db->query("select id, status from po where reff1 =".$data['voucher']['po_id'])->row_array();

        $this->db->insert('f_kas', $data['f_kas']);
        $id_fk = $this->db->insert_id();

        $data['voucher']['po_id'] = $po['id'];
        $data['voucher']['id_fk'] = $id_fk;

            $this->db->insert('voucher', $data['voucher']);
            $id = $this->db->insert_id();

        $this->db->where('id', $po['id']);
        $this->db->update('po', $data['update_po']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function close_po_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $po = $this->db->query("select id, status from po where reff1 =".$this->post('po_id'))->row_array();

        $data = array(
                'status'=> $this->post('status'),
                'remarks'=>$this->post('remarks')
            );
        
        $this->db->where('id', $po['id']);
        $this->db->update('po', $data);
        
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function delete_po_get(){

        // $json = file_get_contents('php://input');

        // Converts it into a PHP object
        // $data = json_decode($json, true);

        $id = $this->get('id');

        $this->db->trans_start();
        $get = $this->db->query("select id from po where reff1 =".$id)->row_array();

        // echo $id.'|'.$get['id'];
        // die();

        $this->db->where('id', $get['id']);
        $this->db->delete('po');

        $this->db->where('po_id', $get['id']);
        $this->db->delete('po_detail');

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di hapus'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal di hapus'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function bpb_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $bpb = $this->db->query("select id from t_bpb_fg where reff1=".$data['bpb_id'])->row_array();
        
        $this->db->where('id', $bpb['id']);
        $this->db->update('t_bpb_fg', $data['bpb']);
        
            foreach ($data['details'] as $v) {  
                $bpb_detail = $this->db->query("select id from t_bpb_fg_detail where reff1=".$v['id_bpb_fg_detail'])->row_array();  
                $data_else = array(
                            'reff1'=> $v['reff1'],
                            'tanggal'=> $data['tgl_input'],
                            'jenis_trx' => 0, //0 masuk
                            'flag_taken'=> 0, // 0 belum diambil
                            't_bpb_fg_id' => $bpb['id'],
                            't_bpb_fg_detail_id' => $bpb_detail['id'],
                            'jenis_barang_id' => $v['id_jenis_barang'] ,
                            'nomor_BPB' => $this->input->post('no_bpb'),
                            'no_produksi' => $v['no_produksi'],
                            'no_packing' => $v['no_packing'],
                            'netto' => $v['netto'],
                            'bruto' => $v['bruto'],
                            'berat_bobbin' => $v['berat_bobbin'],
                            'bobbin_id' => $v['id_bobbin'],
                            'nomor_bobbin'=> $v['no_bobbin'],
                            'keterangan' => $this->input->post('remarks'),
                            'tanggal_masuk' => $data['tgl_input'],
                            'created_by'=> 0,
                            'created_at' => 0
                        );
                $this->db->insert('t_gudang_fg', $data_else);
            }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}