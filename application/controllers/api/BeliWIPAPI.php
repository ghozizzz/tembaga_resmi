<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class BeliWIPAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_jenis_barang');
	}

	public function po_post(){
    	$data = [
            'reff1'=> $this->post('reff1'),
            'no_po'=> $this->post('no_po'),
            'tanggal'=> $this->post('tanggal'),
            'ppn'=>$this->post('ppn'),
            'diskon'=>$this->post('diskon'),
            'materai'=>$this->post('materai'),
            'currency'=>$this->post('currency'),
            'kurs'=>$this->post('kurs'),
            'supplier_id'=>$this->post('supplier_id'),
            'term_of_payment'=>$this->post('term_of_payment'),
            'jenis_po'=>$this->post('jenis_po')
        ];

    		$this->db->insert('po', $data);
        	$id = $this->db->insert_id();

    	if($id > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah',
    			'id'=> $id
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function po_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $get = $this->db->query("select id from po where reff1 =".$data['po_id'])->row_array();

        $this->db->where('id', $get['id']);
        $this->db->update('po', $data['master']);
        
        $this->db->where('po_id', $get['id']);
        $this->db->delete('po_detail');

        foreach ($data['details'] as $i => $item) {
            $data['details'][$i]['po_id'] = $get['id'];
            $data['details'][$i]['reff1'] = $data['details'][$i]['id'];
            unset($data['details'][$i]['id']);
        }

        if($this->db->insert_batch('po_detail', $data['details'])){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtwip_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $po = $this->db->query("select id, status from po where reff1 =".$data['po_id'])->row_array();
        if($po['status'] != $data['po']['status']){
            $this->db->where('id', $po['id']);
            $this->db->update('po', $data['po']);
        }

        $data['dtwip']['reff1'] = $data['dtwip']['id'];
        $data['dtwip']['po_id'] = $po['id'];
        unset($data['dtwip']['id']);
        unset($data['dtwip']['flag_ppn']);

        $this->db->insert('dtwip', $data['dtwip']);
        $dtwip_id = $this->db->insert_id();

        $dtwip_detail = [];
        foreach ($data['details'] as $i => $v){
            $data['details'][$i]['dtwip_id'] = $dtwip_id;
            $get_detail = $this->db->query("select id from po_detail where reff1=".$v['po_detail_id'])->row_array();
            $data['details'][$i]['po_detail_id'] = $get_detail['id'];
            $data['details'][$i]['reff1'] = $v['id'];
            unset($data['details'][$i]['id']);
        }
        
        $this->db->insert_batch('dtwip_detail', $data['details']);

        $this->db->insert('t_bpb_wip', $data['data_bpb']);
        $bpb_wip_id = $this->db->insert_id();

        $bpb_detail = [];
        foreach ($data['details_bpb'] as $key => $value) {
            $data['details_bpb'][$key]['bpb_wip_id'] = $bpb_wip_id;
            $data['details_bpb'][$key]['reff1'] = $value['id'];
            unset($data['details_bpb'][$key]['id']);
        }

        $this->db->insert_batch('t_bpb_wip_detail', $data['details_bpb']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function voucher_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $po = $this->db->query("select id, status from po where reff1 =".$data['voucher']['po_id'])->row_array();

        $this->db->insert('f_kas', $data['f_kas']);
        $id_fk = $this->db->insert_id();

        $data['voucher']['po_id'] = $po['id'];
        $data['voucher']['id_fk'] = $id_fk;

            $this->db->insert('voucher', $data['voucher']);
            $id = $this->db->insert_id();

        $this->db->where('id', $po['id']);
        $this->db->update('po', $data['update_po']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function close_po_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $po = $this->db->query("select id, status from po where reff1 =".$this->post('po_id'))->row_array();

        $data = array(
                'status'=> $this->post('status'),
                'remarks'=>$this->post('remarks')
            );
        
        $this->db->where('id', $po['id']);
        $this->db->update('po', $data);
        
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function bpb_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $bpb = $this->db->query("select id from t_bpb_wip where reff1=".$data['bpb_id'])->row_array();
        
        $this->db->where('id', $bpb['id']);
        $this->db->update('t_bpb_wip', $data['bpb']);
        
            foreach ($data['details'] as $v) {    
                $data_gudang = array(
                        'reff1'=> $v['reff1'],
                        'tanggal'=> $data['tgl_input'],
                        'flag_taken'=>0,
                        't_spb_wip_detail_id'=>$v['id_spb_detail'],
                        't_hasil_wip_id'=> 0,
                        'jenis_barang_id' => $v['id_jenis_barang'] ,
                        't_bpb_wip_detail_id'=>$v['id'],
                        'qty' =>$v['qty'],
                        'uom' =>$v['uom'],
                        'berat' =>str_replace('.', '', $v['berat']),
                        'keterangan' =>null,
                        'created_by'=> 0
                );
                $this->db->insert('t_gudang_wip', $data_gudang);
            }
        // die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}