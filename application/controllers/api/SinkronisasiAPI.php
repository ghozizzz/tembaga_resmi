<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class SinkronisasiAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
	}

    public function so_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        // $no = 0;

        foreach ($data as $key => $v) {
        // echo $key['id'];die();
            if($v['jenis_barang'] == 'FG'){
                $spb = $this->db->query("select id from t_spb_fg where reff1 =".$v['no_spb'])->row_array();
                if(empty($spb)){
                    $this->db->insert('t_spb_fg', array(
                        'reff1'=> $v['no_spb'],
                        'no_spb'=> $v['nomor_spb'],
                        'flag_tolling'=> $v['flag_tolling_spb'],
                        'jenis_spb'=> 6,//JENIS SPB SO
                        'tanggal'=> $v['tanggal'],
                        'keterangan'=> $v['no_sales_order']
                    ));
                    $insert_id = $this->db->insert_id();
                }else{
                    $insert_id = $spb['id'];
                }
            }else if($v['jenis_barang'] == 'WIP'){
                $spb = $this->db->query("select id from t_spb_wip where reff1 =".$v['no_spb'])->row_array();
                if(empty($spb)){
                    $this->db->insert('t_spb_wip', array(
                        'reff1'=> $v['no_spb'],
                        'no_spb_wip'=> $v['nomor_spb'],
                        'flag_tolling'=> $v['flag_tolling_spb'],
                        'tanggal'=> $v['tanggal'],
                        'flag_produksi'=> 6,//JENIS SPB SO
                        'keterangan'=> $v['no_sales_order']
                    ));
                    $insert_id = $this->db->insert_id();
                }else{
                    $insert_id = $spb['id'];
                }
            }else if($v['jenis_barang'] == 'RONGSOK'){
                $spb = $this->db->query("select id from spb where reff1 =".$v['no_spb'])->row_array();
                if(empty($spb)){
                    $this->db->insert('spb', array(
                        'reff1'=> $v['no_spb'],
                        'no_spb'=> $v['nomor_spb'],
                        'flag_tolling'=> $v['flag_tolling_spb'],
                        'jenis_spb'=> 6,//JENIS SPB SO
                        'jenis_barang'=> 1,
                        'tanggal'=> $v['tanggal'],
                        'remarks'=> $v['no_sales_order']
                    ));
                    $insert_id = $this->db->insert_id();
                }else{
                    $insert_id = $spb['id'];
                }
            }else if($v['jenis_barang'] == 'LAIN'){
                $insert_id = '0';
                $tgl_po = '0000-00-00';
            }
            
            $so = $this->db->query("select id from sales_order where reff1 =".$v['id'])->row_array();
            if(empty($so)){
                $this->db->insert('sales_order', array(
                    'reff1'=> $v['id'],
                    'no_sales_order'=> $v['no_sales_order'],
                    'tanggal'=> $v['tanggal'],
                    'flag_tolling'=> $v['flag_tolling'],
                    'm_customer_id'=> $v['m_customer_id'],
                    'marketing_id'=> $v['marketing_id'],
                    'keterangan' => $v['keterangan']
                ));
                $so_id = $this->db->insert_id();

                $this->db->insert('t_sales_order', array(
                    'reff1'=> $v['id'],
                    'id'=>$so_id,
                    'alias'=>$v['alias'],
                    'so_id'=>$so_id,
                    'no_po'=>$v['no_po'],
                    'term_of_payment'=>$v['term_of_payment'],
                    'no_spb'=>$insert_id,
                    'tgl_po'=>$v['tgl_po'],
                    'jenis_so'=>$v['jenis_so'],
                    'jenis_barang'=>$v['jenis_barang'],
                    'currency'=>$v['currency'],
                    'kurs'=>$v['kurs']
                ));
            }else{
                $so_id = $so['id'];
                $this->db->where('id',$so_id);
                $this->db->update('t_sales_order', array(
                    'alias'=>$v['alias'],
                    'no_po'=>$v['no_po'],
                    'term_of_payment'=>$v['term_of_payment'],
                    'no_spb'=>$insert_id,
                    'tgl_po'=>$v['tgl_po'],
                    'jenis_so'=>$v['jenis_so'],
                    'jenis_barang'=>$v['jenis_barang'],
                    'currency'=>$v['currency'],
                    'kurs'=>$v['kurs']
                ));
            }
            
            if($v['jenis_barang'] == 'FG'){
                foreach ($v['details'] as $i => $item){
                    $check_spbd = $this->db->query("select id from t_spb_fg_detail where reff1 =".$item['no_spb_detail'])->row_array();
                    if(empty($check_spbd)){
                        $this->db->insert('t_spb_fg_detail', array(
                            'reff1'=>$item['no_spb_detail'],
                            't_spb_fg_id'=>$insert_id,
                            'tanggal'=>$v['tanggal'],
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'uom'=>$item['uom'],
                            'netto'=>$item['netto'],
                            'keterangan'=>'SALES ORDER'
                        ));
                        $sbp_d = $this->db->insert_id();
                    }else{
                        $this->db->where('id',$check_spbd['id']);
                        $this->db->update('t_spb_fg_detail', array(
                            'tanggal'=>$v['tanggal'],
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'uom'=>$item['uom'],
                            'netto'=>$item['netto'],
                        ));
                        $spb_d = $check_spbd['id'];
                    }

                    $check_sod = $this->db->query("select id from t_sales_order_detail where reff1 =".$item['id'])->row_array();
                    if(empty($check_sod)){
                        $this->db->insert('t_sales_order_detail', array(
                            'reff1'=>$item['id'],
                            't_so_id'=>$so_id,
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }else{
                        $this->db->where('id',$check_sod['id']);
                        $this->db->update('t_sales_order_detail', array(
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                        $spb_d = $check_spbd['id'];
                    }
                }
            }else if($v['jenis_barang'] == 'WIP'){
                foreach ($v['details'] as $i => $item){
                    $check_spbd = $this->db->query("select id from t_spb_wip_detail where reff1 =".$item['no_spb_detail'])->row_array();
                    if(empty($check_spbd)){
                        $this->db->insert('t_spb_wip_detail', array(
                            'reff1'=>$item['no_spb_detail'],
                            't_spb_wip_id'=>$insert_id,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'qty'=>$item['qty'],
                            'uom'=>$item['uom'],
                            'berat'=>$item['netto'],
                            'keterangan'=>'SALES ORDER'
                        ));
                        $sbp_d = $this->db->insert_id();
                    }else{
                        $this->db->where('id', $check_spbd['id']);
                        $this->db->update('t_spb_wip_detail', array(
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'qty'=>$item['qty'],
                            'uom'=>$item['uom'],
                            'berat'=>$item['netto'],
                        ));
                        $spb_d = $check_spbd['id'];
                    }

                    $check_sod = $this->db->query("select id from t_sales_order_detail where reff1 =".$item['id'])->row_array();
                    if(empty($check_sod)){
                        $this->db->insert('t_sales_order_detail', array(
                            'reff1'=>$item['id'],
                            't_so_id'=>$so_id,
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }else{
                        $this->db->where('id',$check_sod['id']);
                        $this->db->update('t_sales_order_detail', array(
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }
                }

            }else if($v['jenis_barang'] == 'RONGSOK'){
                foreach ($v['details'] as $i => $item){
                    $check_spbd = $this->db->query("select id from spb_detail where reff1 =".$item['no_spb_detail'])->row_array();
                    if(empty($check_spbd)){
                        $this->db->insert('spb_detail', array(
                            'reff1'=>$item['no_spb_detail'],
                            'spb_id'=>$insert_id,
                            'rongsok_id'=>$item['jenis_barang_id'],
                            'qty'=>$item['qty'],
                            'line_remarks'=>'SALES ORDER'
                        ));
                        $spb_d = $this->db->insert_id();
                    }else{
                        $this->db->where('id', $check_spbd['id']);
                        $this->db->update('spb_detail', array(
                            'rongsok_id'=>$item['jenis_barang_id'],
                            'qty'=>$item['qty'],
                        ));
                        $spb_d = $check_spbd['id'];
                    }

                    $check_sod = $this->db->query("select id from t_sales_order_detail where reff1 =".$item['id'])->row_array();
                    if(empty($check_sod)){
                        $this->db->insert('t_sales_order_detail', array(
                            'reff1'=>$item['id'],
                            't_so_id'=>$so_id,
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }else{
                        $this->db->where('id',$check_sod['id']);
                        $this->db->update('t_sales_order_detail', array(
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }
                }
            }else if($v['jenis_barang'] == 'LAIN'){
                foreach ($v['details'] as $i => $item){
                    $check_sod = $this->db->query("select id from t_sales_order_detail where reff1 =".$item['id'])->row_array();
                    // print_r($check_sod);die();
                    if(empty($check_sod)){
                        $this->db->insert('t_sales_order_detail', array(
                            'reff1'=>$item['id'],
                            't_so_id'=>$so_id,
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }else{
                        $this->db->where('id',$check_sod['id']);
                        $this->db->update('t_sales_order_detail', array(
                            'no_spb_detail'=>$spb_d,
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'nama_barang_alias'=>$item['nama_barang_alias'],
                            'amount'=>$item['amount'],
                            'qty'=>$item['qty'],
                            'total_amount'=>$item['total_amount'],
                            'bruto'=>$item['bruto'],
                            'netto'=>$item['netto']
                        ));
                    }
                }
            }
            // $no++;
            // echo $no;
        }
        // echo $this->db->trans_status();die();
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        foreach ($data as $key => $value) {
            // print_r($data[$key]['sales_order_id']);die();
            $check = $this->db->query("select id from t_surat_jalan where reff1 =".$data[$key]['id'])->row_array();
            $so = $this->db->query("select id, no_spb, jenis_barang from t_sales_order where reff1 =".$data[$key]['sales_order_id'])->row_array();
            // print_r($so);die();
            //update flag sj

                $this->db->where('id', $so['id']);
                $this->db->update('sales_order', array(
                    'flag_sj' => $data[$key]['flag_sj'],
                    'flag_invoice' => 0
                ));

            if(empty($check)){
            //setting data SJ
            $data_tsj = array(
                'reff1' => $data[$key]['id'],
                'no_surat_jalan' => $data[$key]['no_surat_jalan'],
                'sales_order_id' => $so['id'],
                'po_id' => $data[$key]['po_id'],
                'spb_id' => $data[$key]['spb_id'],
                'retur_id' => $data[$key]['retur_id'],
                'inv_id' => $data[$key]['inv_id'],
                'status' => $data[$key]['status'],
                'tanggal' => $data[$key]['tanggal'],
                'jenis_barang' => $data[$key]['jenis_barang'],
                'm_customer_id' => $data[$key]['m_customer_id'],
                'no_kendaraan' => $data[$key]['no_kendaraan'],
                'supir' => $data[$key]['supir'],
                'remarks' => $data[$key]['remarks']
            );
            $this->db->insert('t_surat_jalan', $data_tsj);
            $sj_id = $this->db->insert_id();
            }else{
                $sj_id = $check['id'];
            }
            //setting insert Gudang FG
            foreach ($value['gudang'] as $k => $v) {

            if($so['jenis_barang']=='FG'){

                $this->db->where('id', $so['no_spb']);
                $this->db->update('t_spb_fg', array(
                    'status'=>$data[$key]['status_spb']
                ));

                // $primary_check_barcode = $this->db->query("select id from t_bpb_fg_detail where no_packing_barcode ='".$v['no_packing']."'")->row_array();
                // if(empty($primary_check_barcode)){
                $a_date = $data[$key]['tanggal'];
                    // $a_date = '2019-07-30';
                // $date = date("Y-m-t", strtotime($a_date));//TANGGAL TERAKHIR
                $hari = date('d', strtotime($a_date));//HARI

                if($hari <= 10){
                    $last_date = date("Y-m-j", strtotime("last day of previous month"));
                    $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
                }else{
                    $last_date = $a_date;
                    $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
                }

                // echo $last_date;die();
                //cek mau insert t_bpb_fg
                $cek_bpb = $this->db->query("select id, no_bpb_fg from t_bpb_fg where tanggal ='".$last_date."' AND jenis_barang_id = ".$v['sj_jb'])->row_array();
                // print_r($cek_bpb);die();
                if(empty($cek_bpb)){
                    $this->db->insert('t_bpb_fg', array(
                        'no_bpb_fg' => 'BPB-KMP.'.$tgl_bpb.'.',
                        'tanggal' => $last_date,
                        'produksi_fg_id' => 0,
                        'jenis_barang_id' => $v['sj_jb'],
                        'status' => 1,
                        'keterangan' => $v['ket_bpb']
                    ));
                    $bpb_id = $this->db->insert_id();
                }else{
                    $bpb_id = $cek_bpb['id'];
                }

                //insert t_bpb_fg_detail
                if($bpb_id > 0){
                    $cek_bpb_detail = $this->db->query("select id from t_bpb_fg_detail where no_packing_barcode ='".$v['no_packing']."'")->row_array();  
                    if(empty($cek_bpb_detail)){
                        $this->db->insert('t_bpb_fg_detail', array(
                            'reff1' => $v['id_bpb_d'],
                            't_bpb_fg_id' => $bpb_id,
                            'jenis_barang_id' => $v['sj_jb'],
                            'no_packing_barcode' => $v['no_packing'],
                            'no_produksi' => $v['no_produksi'],
                            'bruto' => $v['bruto'],
                            'netto' => $v['netto'],
                            'berat_bobbin' => $v['berat_bobbin'],
                            'bobbin_id' => $v['bobbin_id'],
                            'flag_taken' => 0
                        ));
                        $bpb_detail_id = $this->db->insert_id();
                    }else{
                        $this->db->where('id', $cek_bpb_detail['id']);
                        $this->db->update('t_bpb_fg_detail', array(
                            'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                            't_bpb_fg_id'=> $bpb_id
                        ));
                        $bpb_detail_id = $cek_bpb_detail['id'];
                    }
                }else{
                    $bpb_detail_id = 0;
                }

                //insert t_gudang_fg
                    $cek_gudang_fg = $this->db->query("select id from t_gudang_fg where no_packing ='".$v['no_packing']."'")->row_array();
                    $spb = $this->db->query("select id, no_spb from t_spb_fg where reff1 =".$v['t_spb_fg_id'])->row_array();
                    if(empty($cek_gudang_fg)){
                        $this->db->insert('t_gudang_fg', array(
                                'reff1' => $v['id'],
                                'tanggal'=> $v['tanggal'],
                                'jenis_trx' =>1, //0 masuk
                                'flag_taken'=>1, // 0 belum diambil
                                't_bpb_fg_id' => $bpb_id,
                                't_bpb_fg_detail_id' => $bpb_detail_id,
                                't_spb_fg_id' => $so['no_spb'],
                                't_sj_id' => $sj_id,
                                'jenis_barang_id' => $v['sj_jb'],
                                'nomor_SPB' => $spb['no_spb'],
                                'nomor_BPB' => $v['nomor_BPB'],
                                'no_produksi' => $v['no_produksi'],
                                'no_packing' => $v['no_packing'],
                                'netto' => $v['netto'],
                                'bruto' => $v['bruto'],
                                'berat_bobbin' => $v['berat_bobbin'],
                                'bobbin_id' => $v['bobbin_id'],
                                'nomor_bobbin'=> $v['nomor_bobbin'],
                                'keterangan' => $v['keterangan'],
                                'tanggal_masuk' => $last_date,
                                'tanggal_keluar' => $v['tanggal_keluar'],
                                'created_by'=> 0,
                                'created_at' => 0
                            ));
                        $gudang_id = $this->db->insert_id();
                    }else{
                    $this->db->where('id', $cek_gudang_fg['id']);
                        $this->db->update('t_gudang_fg', array(
                            'jenis_trx'=>1,
                            'flag_taken'=>1,
                            'tanggal'=> $last_date,
                            't_bpb_fg_id' => $bpb_id,
                            't_bpb_fg_detail_id' => $bpb_detail_id,
                            't_spb_fg_id' => $so['no_spb'],
                            'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                            'nomor_SPB' => $spb['no_spb'],
                            'nomor_BPB' => 'BPB-KMP.'.$tgl_bpb.'.',
                            't_spb_fg_id'=> $so['no_spb'],
                            't_sj_id' => $sj_id,
                            'tanggal_masuk' => $last_date,
                            'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar']
                        ));
                        $gudang_id = $cek_gudang_fg['id'];
                    }

                //setting data SJ
                $this->db->insert('t_surat_jalan_detail', array(
                    'reff1'=> $v['id_sj_d'],
                    't_sj_id'=> $sj_id,
                    'gudang_id'=> $gudang_id,
                    'jenis_barang_id'=> $v['sj_jb'],
                    'jenis_barang_alias'=> $v['jenis_barang_alias'],
                    'no_packing'=> $v['no_packing'],
                    'qty'=> $v['jb_qty'],
                    'bruto'=> $v['bruto'],
                    'netto'=> $v['netto'],
                    'nomor_bobbin'=> $v['nomor_bobbin'],
                    'line_remarks'=> $v['line_remarks'],
                    'created_by'=> 0,
                    'created_at'=> 0
                ));
            //}//check barcode udah ada apa belum
            }elseif($so['jenis_barang']=='WIP'){
                $this->db->where('id', $so['no_spb']);
                $this->db->update('t_spb_wip', array(
                    'status'=>$data[$key]['status_spb']
                ));

                $spb_detail = $this->db->query("select id from t_spb_wip_detail where t_spb_wip_id =".$so['no_spb']." and jenis_barang_id=".$v['jenis_barang_id'])->row_array();

                if(empty($spb_detail)){
                    $spb_detail['id'] = 0;
                }
                $this->db->insert('t_spb_wip_fulfilment', array(
                    't_spb_wip_id'=>$so['no_spb'],
                    't_spb_wip_detail_id'=>$spb_detail['id'],
                    'jenis_barang_id'=>$v['jenis_barang_id'],
                    'qty'=>$v['qty'],
                    'berat'=>$v['berat'],
                    'keterangan'=>$v['keterangan']
                ));

                //setting t_gudang_wip
                $this->db->insert('t_gudang_wip', array(
                    'reff1'=>$v['id'],
                    'tanggal'=>$v['tanggal'],
                    'flag_taken'=>$v['flag_taken'],
                    'jenis_trx'=>$v['jenis_trx'],
                    't_hasil_wip_id'=>$v['t_hasil_wip_id'],
                    'jenis_barang_id'=>$v['jenis_barang_id'],
                    't_spb_wip_id'=>$so['no_spb'],
                    't_spb_wip_detail_id'=>$spb_detail['id'],
                    't_bpb_wip_detail_id'=>$v['t_bpb_wip_detail_id'],
                    'qty'=>$v['qty'],
                    'uom'=>$v['uom'],
                    'berat'=>$v['berat'],
                    'keterangan'=>$v['keterangan']
                ));
                $gudang_id = $this->db->insert_id();

                //setting data SJ
                $check_sj = $this->db->query("select id from t_surat_jalan_detail where reff1 =".$v['id_sj_d'])->row_array();
                if(empty($check_sj)){
                    $this->db->insert('t_surat_jalan_detail', array(
                        'reff1'=> $v['id_sj_d'],
                        't_sj_id'=> $sj_id,
                        'gudang_id'=> $gudang_id,
                        'jenis_barang_id'=> $v['jenis_barang_id'],
                        'jenis_barang_alias'=> $v['jenis_barang_alias'],
                        'no_packing'=> 0,
                        'qty'=> $v['jb_qty'],
                        'bruto'=> 0,
                        'netto'=> $v['berat'],
                        'nomor_bobbin'=> 0,
                        'line_remarks'=> $v['line_remarks'],
                        'created_by'=> 0,
                        'created_at'=> 0
                    ));
                }
            }elseif($so['jenis_barang']=='RONGSOK'){
                $this->db->where('id', $so['no_spb']);
                $this->db->update('spb', array(
                    'status'=>$data[$key]['status_spb']
                ));

                //NOTE ADA DI NOTE ROOT FOLDER!!!!
                // cek insert dtr pertama udah apa belum
                    $cek_dtr = $this->db->query("select dtr.id, ttr.id as id_ttr from dtr left join ttr on ttr.dtr_id = dtr.id where dtr.type =".$sj_id)->row_array();
                    if(empty($cek_dtr)){
                        $nomor = substr($data[$key]['no_surat_jalan'],7,11);
                        // echo $nomor;
                        // die();
                        $this->db->insert('dtr', array(
                            'type' => $sj_id,
                            'no_dtr' => 'DTR-RSK.'.$nomor,
                            'tanggal' => $data[$key]['tanggal'],
                            'po_id' => $data[$key]['po_id'],
                            'so_id' => 0,
                            'retur_id' => $data[$key]['retur_id'],
                            'prd_id' => 0,
                            'flag_taken' => 0,
                            'supplier_id' => 822,//SUPPLIER REPACKING
                            'customer_id' => 0,
                            'jenis_barang' => 'RONGSOK',
                            'remarks' => '',
                            'status' => 1
                        ));
                        $dtr_id = $this->db->insert_id();

                        $this->db->insert('ttr', array(
                            'no_ttr' => 'BPB-RSK.'.$nomor,
                            'tanggal' => $data[$key]['tanggal'],
                            'no_sj' => '',
                            'dtr_id' => $dtr_id,
                            'jmlh_afkiran' => 0,
                            'jmlh_pengepakan' => 0,
                            'jmlh_lain' => 0,
                            'remarks' => 0,
                            'flag_bayar' => 0,
                            'flag_produksi' => 0,
                            'ttr_status' => 1
                        ));
                        $ttr_id = $this->db->insert_id();
                    }else{
                        $dtr_id = $cek_dtr['id'];
                        $ttr_id = $cek_dtr['id_ttr'];
                    }

                    //insert produksi_fg_detail
                        //CEK DTR DETAILNYA UDAH ADA APA BELUM
                    $cek_dd = $this->db->query("select dd.id, td.id as id_ttr from dtr_detail dd left join ttr_detail td on td.dtr_detail_id = dd.id where dd.reff1 =".$v['id'])->row_array();
                    if(empty($cek_dd['id'])){
                        $this->db->insert('dtr_detail', array(
                            'reff1'=> $v['id'],
                            'dtr_id' => $dtr_id,
                            'po_detail_id' => 0,
                            'so_id' => $so['id'],
                            'retur_id' => 0,
                            'rongsok_id' => $v['rongsok_id'],
                            'qty' => $v['qty'],
                            'bruto' => $v['bruto'],
                            'berat_palette' => $v['berat_palette'],
                            'netto' => $v['netto'],
                            'netto_resmi' => $v['netto'],
                            'no_pallete' => $v['no_pallete'],
                            'tanggal_masuk' => $v['tanggal_masuk'],
                            'tanggal_keluar'=> $v['tanggal_keluar'],
                            'flag_taken'=> 1,
                            'flag_sj'=> $sj_id,
                            'flag_resmi'=> 1,
                            'line_remarks' => ''
                        ));
                        $dtr_detail_id = $this->db->insert_id();

                        $this->db->insert('ttr_detail', array(
                            'reff1'=> $v['id'],
                            'ttr_id'=> $ttr_id,
                            'dtr_detail_id'=> $dtr_detail_id,
                            'rongsok_id'=> $v['rongsok_id'],
                            'ampas_id'=> 0,
                            'qty' => $v['qty'],
                            'bruto' => $v['bruto'],
                            'netto' => $v['netto'],
                            'line_remarks' => ''
                        ));
                        $ttr_detail_id = $this->db->insert_id();

                        $this->db->insert('spb_detail_fulfilment',array(
                            'spb_id'=>$so['no_spb'],
                            'dtr_detail_id'=>$dtr_detail_id,
                            'netto'=> $v['netto'],
                            'ttr_id'=>$ttr_id
                        ));
                    }else{
                        $this->db->where('id', $cek_dd['id']);
                        $this->db->update('dtr_detail', array(
                            'netto'=> $v['netto'],
                            'netto_resmi'=> $v['netto'],
                            'flag_sj'=>0,
                            'flag_resmi'=> 1
                        ));
                        $dtr_detail_id = $cek_dd['id'];
                        $ttr_detail_id = $cek_dd['id_ttr'];
                    // echo $sj_id;

                        $this->db->where('dtr_detail_id', $cek_dd['id']);
                        $this->db->update('spb_detail_fulfilment',array(
                            'netto'=> $v['netto']
                        ));
                    }
                    // echo $sj_id;

                //setting data SJ
                $check_sj = $this->db->query("select id from t_surat_jalan_detail where reff1 =".$v['id_sj_d'])->row_array();
                if(empty($check_sj)){
                    $this->db->insert('t_surat_jalan_detail', array(
                        'reff1'=> $v['id_sj_d'],
                        't_sj_id'=> $sj_id,
                        'gudang_id'=> $dtr_detail_id,
                        'jenis_barang_id'=> $v['rongsok_id'],
                        'jenis_barang_alias'=> $v['jenis_barang_alias'],
                        'no_packing'=> $v['no_pallete'],
                        'qty'=> $v['jb_qty'],
                        'bruto'=> $v['bruto'],
                        'berat'=> $v['berat_palette'],
                        'netto'=> $v['netto'],
                        'nomor_bobbin'=> $v['nomor_bobbin'],
                        'line_remarks'=> $v['line_remarks'],
                        'created_by'=> 0,
                        'created_at'=> 0
                    ));
                }
            }elseif($so['jenis_barang']=='LAIN'){
                //setting data SJ
                // echo 'lain';die();
                $check_sj = $this->db->query("select id from t_surat_jalan_detail where reff1 =".$v['id_sj_d'])->row_array();
                if(empty($check_sj)){
                    $this->db->insert('t_surat_jalan_detail', array(
                        'reff1'=> $v['id_sj_d'],
                        't_sj_id'=> $sj_id,
                        'gudang_id'=> 0,
                        'jenis_barang_id'=> $v['sj_jb'],
                        'jenis_barang_alias'=> $v['jenis_barang_alias'],
                        'no_packing'=> $v['no_packing'],
                        'qty'=> $v['jb_qty'],
                        'bruto'=> $v['bruto'],
                        'berat'=> $v['berat'],
                        'netto'=> $v['netto'],
                        'nomor_bobbin'=> $v['nomor_bobbin'],
                        'line_remarks'=> $v['line_remarks'],
                        'created_by'=> 0,
                        'created_at'=> 0
                    ));
                }
            }//IF JENIS BARANG
          }//LOOP DETAIL
            if($so['jenis_barang']=='RONGSOK'){
                $cek_dtr_kosong = $this->db->query("select count(id) from dtr_detail where dtr_id =".$dtr_id)->row_array();
                if(empty($cek_dtr_kosong)){
                    $this->db->where('id', $dtr_id);
                    $this->db->delete('dtr');

                    $this->db->where('id', $ttr_id);
                    $this->db->delete('ttr');
                }
            }
        }//LOOP SYNC

        // echo $this->db->trans_status();die();
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function inv_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        foreach ($data as $key => $value) {

            $sj = $this->db->query("select id, sales_order_id from t_surat_jalan where reff1 =".$value['id_surat_jalan'])->row_array();

            //update t_surat_jalan
            $check_inv = $this->db->query("select id from f_invoice where reff1 =".$value['id'])->row_array();
            $sj = $this->db->query("select id, sales_order_id from t_surat_jalan where reff1=".$value['id_surat_jalan'])->row_array();
            // echo $value['id_surat_jalan'];
            // print_r($sj);die();
            if(empty($check_inv)){
                //insert f_invoice
                $this->db->insert('f_invoice', array(
                    'reff1'=>$value['id'],
                    'jenis_trx'=>$value['jenis_trx'],
                    'term_of_payment'=>$value['term_of_payment'],
                    'bank_id'=>$value['bank_id'],
                    'diskon'=>$value['diskon'],
                    'add_cost'=>$value['add_cost'],
                    'materai'=>$value['materai'],
                    'currency'=>$value['currency'],
                    'kurs'=>$value['kurs'],
                    'nama_direktur'=>$value['nama_direktur'],
                    'no_invoice'=>$value['no_invoice'],
                    'nilai_invoice'=>$value['nilai_invoice'],
                    'nilai_bayar'=>$value['nilai_bayar'],
                    'nilai_pembulatan'=>$value['nilai_pembulatan'],
                    'flag_matching'=>$value['flag_matching'],
                    'flag_resmi'=>0,
                    'tanggal'=>$value['tanggal'],
                    'tgl_jatuh_tempo'=>$value['tgl_jatuh_tempo'],
                    'id_customer'=>$value['id_customer'],
                    'id_sales_order'=>$sj['sales_order_id'],
                    'id_surat_jalan'=>$sj['id'],
                    'id_retur'=>0,
                    'keterangan'=>$value['keterangan']
                ));
                $f_id = $this->db->insert_id();
            }else{
                $f_id = $check_inv['id'];
                $this->db->where('id', $f_id);
                $this->db->update('f_invoice', array(
                    'id_sales_order'=>$sj['sales_order_id'],
                    'id_surat_jalan'=>$sj['id']
                ));
            }

                $this->db->where('id', $sj['id']);
                $this->db->update('t_surat_jalan', array(
                    'inv_id'=> $f_id
                ));

                //update sales_order
                $this->db->where('id', $sj['sales_order_id']);
                $this->db->update('sales_order', array(
                    'flag_invoice' => $value['flag_invoice']
                ));
                //insert f_invoice_detail
                foreach ($value['details'] as $i => $item) {
                    $check_inv_d = $this->db->query("select id from f_invoice_detail where reff1=".$item['id'])->row_array();
                    if(empty($check_inv_d)){
                        $this->db->insert('f_invoice_detail', array(
                            'reff1'=>$item['id'],
                            'id_invoice'=>$f_id,
                            'sj_detail_id'=>$sj['id'],
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'qty'=>$item['qty'],
                            'netto'=>$item['netto'],
                            'harga'=>$item['harga'],
                            'total_harga'=>$item['total_harga'],
                            'keterangan'=>$item['keterangan']
                        ));
                    }else{
                        $this->db->where('id', $check_inv_d['id']);
                        $this->db->update('f_invoice_detail', array(
                            'id_invoice'=>$f_id,
                            'sj_detail_id'=>$sj['id'],
                            'jenis_barang_id'=>$item['jenis_barang_id'],
                            'qty'=>$item['qty'],
                            'netto'=>$item['netto'],
                            'harga'=>$item['harga'],
                            'total_harga'=>$item['total_harga'],
                            'keterangan'=>$item['keterangan']
                        ));
                    }
                }
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}