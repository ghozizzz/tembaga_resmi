<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class ReffAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function so_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $this->db->insert('t_spb_fg', array(
            'no_spb'=> $data['nomor_spb'],
            'tanggal'=> $data['header']['tanggal'],
            'status'=> 0,
            'flag_tolling'=> 0,
            'jenis_spb'=> 4,
            'keterangan'=> $data['header']['no_so']
        ));
        $spb_id = $this->db->insert_id();
        
        $this->db->insert('sales_order', array(
            'reff2'=> $data['header']['reff2'],
            'no_sales_order'=> $data['header']['no_so'],
            'tanggal'=> $data['header']['tanggal'],
            'm_customer_id'=> $data['header']['idkmp'],
            'flag_tolling'=> 2,
            'flag_invoice'=> 0,
            'flag_sj'=> 0,
            'keterangan'=>''
        ));
        $so_id = $this->db->insert_id();

        $this->db->insert('t_sales_order', array(
            'id'=> $so_id,
            'so_id'=> $so_id,
            'no_po'=> $data['header']['no_po'],
            'tgl_po'=> $data['header']['tgl_po'],
            'term_of_payment'=> $data['header']['term_of_payment'],
            'no_spb'=> $spb_id,
            'jenis_so'=> 0,
            'jenis_barang'=> 'FG',
            'currency'=> 'IDR',
            'kurs'=> 1
        ));

        foreach ($data['detail'] as $key => $row) {
            $this->db->insert('t_spb_fg_detail', array(
                'tanggal'=> $data['header']['tanggal'],
                't_spb_fg_id'=> $spb_id,
                'jenis_barang_id'=> $row['jenis_barang_id'],
                'uom'=> 'KG',
                'netto'=> $row['netto'],
                'keterangan'=> ''
            ));
            $id_detail = $this->db->insert_id();

            $this->db->insert('t_sales_order_detail', array(
                'reff2'=> $row['reff2'],
                't_so_id'=> $so_id,
                'no_spb_detail'=> $id_detail,
                'jenis_barang_id'=> $row['jenis_barang_id'],
                'qty'=> 1,
                'bruto'=> $row['netto'],
                'netto'=> $row['netto'],
                'amount'=> $row['amount'],
                'total_amount'=> $row['total_amount']
            ));
        }
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function so_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select so.id, tso.no_spb, tso.jenis_barang from sales_order so left join t_sales_order tso on tso.so_id = so.id where so.reff2 =".$data['header']['reff2'])->row_array();

        $this->db->where('id', $so['id']);
        $this->db->update('sales_order', array(
            'no_sales_order'=> $data['header']['no_so'],
            'tanggal'=> $data['header']['tanggal']
        ));

        $this->db->where('id', $so['id']);
        $this->db->update('t_sales_order', array(
            'term_of_payment'=> $data['header']['term_of_payment']
        ));

        $this->db->where('id', $so['no_spb']);
        $this->db->update('t_spb_fg', array(
            'status'=> 1,
            'keterangan'=> $data['header']['no_so']
        ));

        foreach ($data['detail'] as $key => $row) {
            $d = $this->db->query("select id, no_spb_detail from t_sales_order_detail where reff2 =".$row['id'])->row_array();

            $this->db->where('id', $d['no_spb_detail']);
            $this->db->update('t_spb_fg_detail', array(
                'tanggal'=> $data['header']['tanggal'],
                'jenis_barang_id'=> $row['jenis_barang_id'],
                'uom'=> 'KG',
                'netto'=> $row['netto']
            ));

            $this->db->where('id', $d['id']);
            $this->db->update('t_sales_order_detail', array(
                'jenis_barang_id'=> $row['jenis_barang_id'],
                'bruto'=> $row['netto'],
                'netto'=> $row['netto'],
                'amount'=> $row['amount'],
                'total_amount'=> $row['total_amount']
            ));
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di ubah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_post()
    {
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);
        // print_r($data); die();
        $this->db->trans_start();
        $so = $this->db->query("select tso.id, tso.no_spb, tso.jenis_barang from t_sales_order tso left join sales_order so on so.id = tso.so_id where so.reff2 =".$data['header']['r_so_id'])->row_array();
        // print_r($so); die();
        $this->db->where('id', $so['id']);
        $this->db->update('sales_order', array(
            'flag_sj' => 1
        ));
        //setting data SJ
        $data_tsj = array(
            'reff2' => $data['header']['reff2'],
            'no_surat_jalan' => $data['header']['no_sj_resmi'],
            'sales_order_id' => $so['id'],
            'po_id' => $data['header']['r_po_id'],
            'spb_id' => $so['no_spb'],
            'retur_id' => 0,
            'inv_id' => 0,
            'status' => 1,
            'tanggal' => $data['header']['tanggal'],
            'jenis_barang' => $data['header']['jenis_barang'],
            'm_customer_id' => $data['header']['idkmp'],
            'no_kendaraan' => $data['header']['no_kendaraan'],
            'supir' => $data['header']['supir'],
            'remarks' => $data['header']['remarks']
        );
        $this->db->insert('t_surat_jalan', $data_tsj);
        $sj_id = $this->db->insert_id();

        //setting insert Gudang FG
        foreach ($data['gudang'] as $k => $v) {

            if($so['jenis_barang']=='FG'){

                $this->db->where('id', $so['no_spb']);
                $this->db->update('t_spb_fg', array(
                    'status'=>1
                ));

                $a_date = $data['header']['tanggal'];
                // $date = date("Y-m-t", strtotime($a_date));//TANGGAL TERAKHIR
                $hari = date('d', strtotime($a_date));//HARI

                if($hari <= 10){
                    $last_date = date("Y-m-j", strtotime("last day of previous month"));
                    $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
                }else{
                    $last_date = $a_date;
                    $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
                }
                // echo $last_date;die();
                //cek mau insert t_bpb_fg
                $cek_bpb = $this->db->query("select id, no_bpb_fg from t_bpb_fg where tanggal ='".$last_date."' AND jenis_barang_id = ".$v['jenis_barang_id'])->row_array();
                // print_r($cek_bpb);die();
                if(empty($cek_bpb)){
                    $this->db->insert('t_bpb_fg', array(
                        'no_bpb_fg' => 'BPB-KMP.'.$tgl_bpb.'.',
                        'tanggal' => $last_date,
                        'produksi_fg_id' => 0,
                        'jenis_barang_id' => $v['jenis_barang_id'],
                        'status' => 1,
                        'keterangan' => '',
                    ));
                    $bpb_id = $this->db->insert_id();
                    $no_bpb = 'BPB-KMP.'.$tgl_bpb.'.';
                }else{
                    $bpb_id = $cek_bpb['id'];
                    $no_bpb = $cek_bpb['no_bpb_fg'];
                }

                //insert t_bpb_fg_detail
                if($bpb_id > 0){
                    $cek_bpb_detail = $this->db->query("select id from t_bpb_fg_detail where no_packing_barcode ='".$data['gudang'][$k]['no_packing']."'")->row_array();
                    if(empty($cek_bpb_detail)){
                        $this->db->insert('t_bpb_fg_detail', array(
                            't_bpb_fg_id' => $bpb_id,
                            'jenis_barang_id' => $data['gudang'][$k]['jenis_barang_id'],
                            'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
                            'no_produksi' => 0,
                            'bruto' => $data['gudang'][$k]['bruto'],
                            'netto' => $data['gudang'][$k]['netto'],
                            'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                            'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                            'flag_taken' => 0
                        ));
                        $bpb_detail_id = $this->db->insert_id();
                    }else{
                        $bpb_detail_id = $cek_bpb_detail['id'];
                    }
                }else{
                    $bpb_detail_id = 0;
                }

                //insert t_gudang_fg
                    $cek_gudang_fg = $this->db->query("select id from t_gudang_fg where no_packing ='".$data['gudang'][$k]['no_packing']."'")->row_array();
                    // $spb = $this->db->query("select id from t_spb_fg where reff1 =".$data['gudang'][$k]['t_spb_fg_id'])->row_array();
                    if(empty($cek_gudang_fg)){
                        $this->db->insert('t_gudang_fg', array(
                                'reff2' => $data['gudang'][$k]['id'],
                                'tanggal'=> $data['gudang'][$k]['tanggal_masuk'],
                                'jenis_trx' =>1, //0 masuk
                                'flag_taken'=>1, // 0 belum diambil
                                't_bpb_fg_id' => $bpb_id,
                                't_bpb_fg_detail_id' => $bpb_detail_id,
                                't_spb_fg_id' => $so['no_spb'],
                                't_sj_id' => $sj_id,
                                'jenis_barang_id' => $data['gudang'][$k]['jenis_barang_id'],
                                'nomor_BPB' => $no_bpb,
                                'no_produksi' => 0,
                                'no_packing' => $data['gudang'][$k]['no_packing'],
                                'netto' => $data['gudang'][$k]['netto'],
                                'bruto' => $data['gudang'][$k]['bruto'],
                                'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                                'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                                'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                                'keterangan' => null,
                                'tanggal_masuk' => $last_date,
                                'tanggal_keluar' => $data['header']['tanggal'],
                                'created_by'=> 0,
                                'created_at' => 0
                            ));
                        $gudang_id = $this->db->insert_id();
                    }else{
                        $this->db->where('id', $cek_gudang_fg['id']);
                        $this->db->update('t_gudang_fg', array(
                            'jenis_trx'=>1,
                            'flag_taken'=>1,
                            'tanggal'=> $last_date,
                            't_bpb_fg_id' => $bpb_id,
                            't_bpb_fg_detail_id' => $bpb_detail_id,
                            't_spb_fg_id' => $so['no_spb'],
                            'jenis_barang_id' => $data['gudang'][$k]['sj_jb'],
                            'nomor_SPB' => $spb['no_spb'],
                            'nomor_BPB' => 'BPB-KMP.'.$tgl_bpb.'.',
                            't_spb_fg_id'=> $so['no_spb'],
                            't_sj_id' => $sj_id,
                            'tanggal_masuk' => $last_date,
                            'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar']
                        ));
                        $gudang_id = $cek_gudang_fg['id'];
                    }

                //setting data SJ
                $this->db->insert('t_surat_jalan_detail', array(
                    'reff2'=> $data['detail'][$k]['reff2'],
                    't_sj_id'=> $sj_id,
                    'gudang_id'=> $gudang_id,
                    'jenis_barang_id'=> $data['detail'][$k]['jenis_barang_id'],
                    'jenis_barang_alias'=> 0,
                    'no_packing'=> $data['detail'][$k]['no_packing'],
                    'qty'=> 1,
                    'bruto'=> $data['detail'][$k]['bruto'],
                    'netto'=> $data['detail'][$k]['netto'],
                    'nomor_bobbin'=> $data['detail'][$k]['nomor_bobbin'],
                    'line_remarks'=> $data['detail'][$k]['line_remarks'],
                    'created_by'=> 0,
                    'created_at'=> 0
                ));
            }
        }

        // echo $this->db->trans_status();
        // die();
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di ubah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $sj = $this->db->query("select tsj.id, tso.no_spb from t_surat_jalan tsj left join t_sales_order tso on tso.id = tsj.sales_order_id where tsj.reff2 =".$data['reff2'])->row_array();
// print_r($sj);
        $this->db->where('id', $sj['id']);
        $this->db->update('t_surat_jalan', array(
            'no_surat_jalan'=> $data['no_sj_resmi'],
            'tanggal'=> $data['tanggal']
        ));

        $this->db->where('t_spb_fg_id', $sj['no_spb']);
        $this->db->update('t_gudang_fg', array('tanggal_keluar' => $data['tanggal']));

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di ubah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function inv_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $inv = $this->db->query("select tsj.id, tsj.sales_order_id from t_surat_jalan tsj where tsj.reff2 =".$data['header']['sjr_id'])->row_array();

        $this->db->insert('f_invoice', array(
            'reff2'=> $data['header']['reff2'],
            'no_invoice'=> $data['header']['no_invoice_jasa'],
            'nilai_invoice'=> $data['header']['nilai_invoice'],
            'term_of_payment'=> $data['header']['term_of_payment'],
            'bank_id'=> $data['header']['bank_id'],
            'diskon'=> $data['header']['diskon'],
            'add_cost'=> $data['header']['cost'],
            'materai'=> $data['header']['materai'],
            'currency'=> 'IDR',
            'kurs'=> 1,
            'nama_direktur'=> $data['header']['nama_direktur'],
            'tanggal'=> $data['header']['tanggal'],
            'tgl_jatuh_tempo'=> $data['header']['jatuh_tempo'],
            'id_customer'=> $data['header']['idkmp'],
            'id_sales_order'=> $inv['sales_order_id'],
            'id_surat_jalan'=> $inv['id'],
            'keterangan'=> $data['header']['remarks']
        ));
        
        $inv_id = $this->db->insert_id();

        $this->db->where('id', $inv['id']);
        $this->db->update('t_surat_jalan', array(
            'inv_id'=>$inv_id
        ));

        $this->db->where('id', $inv['sales_order_id']);
        $this->db->update('sales_order', array(
            'flag_invoice'=>1
        ));

        foreach ($data['detail'] as $i => $v) {
            $this->db->insert('f_invoice_detail', array(
                    'reff2'=>$v['reff2'],
                    'id_invoice'=>$inv_id,
                    'sj_detail_id'=>$inv['id'],
                    'jenis_barang_id'=>$v['jenis_barang_id'],
                    'qty'=>$v['qty'],
                    'netto'=>$v['netto'],
                    'harga'=>$v['amount'],
                    'total_harga'=>$v['total_amount'],
            ));
        }

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function inv_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $inv = $this->db->query("select id from f_invoice where reff2 =".$data['id'])->row_array();
// print_r($sj);
        $this->db->where('id', $inv['id']);
        $this->db->update('f_invoice', array(
            'no_invoice'=> $data['master']['no_invoice_jasa'],
            'nilai_invoice'=> $data['master']['nilai_invoice'],
            'tanggal'=> $data['master']['tanggal'],
            'term_of_payment' => $data['master']['term_of_payment'],
            'tgl_jatuh_tempo' => $data['master']['jatuh_tempo'],
            'bank_id' => $data['master']['bank_id'],
            'nama_direktur' => $data['master']['nama_direktur'],
            'diskon' => str_replace(',', '', $data['master']['diskon']),
            'add_cost' => str_replace(',', '', $data['master']['cost']),
            'materai' => str_replace(',', '', $data['master']['materai']),
            'keterangan'=> $data['master']['remarks'],
        ));

        foreach ($data['detail'] as $i => $row) {
            $this->db->where('reff2', $row['id']);
            $this->db->update('f_invoice_detail', array(
                'jenis_barang_id' => $row['barang_id'],
                'qty' => $row['qty'],
                'netto' => str_replace(',', '', $row['netto']),
                'harga' => str_replace(',', '', $row['amount']),
                'total_harga' => str_replace(',', '', $row['total_amount'])
            ));
        }

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di ubah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtr_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select id from sales_order where reff2 =".$data['header']['so_id'])->row_array();
        // print_r($so);die();
        if(empty($so)){
            $so['id'] = 0;
        }
        // print_r($so);die();
        $this->db->insert('dtr', array(
            'reff2'=> $data['header']['dtr_id'],
            'tanggal'=>$data['header']['tanggal'],
            'po_id'=>0,
            'so_id'=>$so['id'],
            'retur_id'=>0,
            'prd_id'=>0,
            'flag_taken'=>0,
            'supplier_id'=>0,
            'customer_id'=>$data['header']['idkmp'],
            'jenis_barang'=>'RONGSOK',
            'remarks'=>'',
            'status'=>1
        ));
        $dtr_id = $this->db->insert_id();

        $this->db->insert('ttr', array(
            'reff2'=> $data['header']['ttr_id'],
            'tanggal'=> $data['header']['tanggal'],
            'no_sj'=> $data['header']['no_sj'],
            'dtr_id'=> $dtr_id,
            'jmlh_afkiran'=> 0,
            'jmlh_pengepakan'=> $data['header']['total_berat'],
            'jmlh_lain'=> 0,
            'remarks' => '',
            'flag_bayar' => 0,
            'flag_produksi' => 0,
            'ttr_status' => 1,
        ));
        $ttr_id = $this->db->insert_id();

        foreach ($data['detail'] as $i => $v) {
            $this->db->insert('dtr_detail', array(
                    'reff2'=> $v['dtr_reff2'],
                    'dtr_id'=> $dtr_id,
                    'po_detail_id'=> 0,
                    'so_id'=> 0,
                    'retur_id'=> 0,
                    'rongsok_id'=> $v['rongsok_id'],
                    'qty'=> $v['qty'],
                    'bruto'=> $v['bruto'],
                    'berat_palette'=> $v['berat_pallete'],
                    'netto'=> $v['netto'],
                    'netto_resmi'=> 0,
                    'line_remarks'=> '',
                    'no_pallete'=> $v['no_pallete'],
                    'tanggal_masuk'=> $data['header']['tanggal'],
                    'tanggal_keluar'=> null,
                    'flag_taken'=> 0,
                    'flag_resmi'=> 0
            ));
            $dtr_detail_id = $this->db->insert_id();

            $this->db->insert('ttr_detail', array(
                    'reff2'=> $v['ttr_reff2'],
                    'ttr_id'=> $ttr_id,
                    'dtr_detail_id'=> $dtr_detail_id,
                    'rongsok_id'=> $v['rongsok_id'],
                    'ampas_id'=> 0,
                    'qty'=> $v['qty'],
                    'bruto'=> $v['bruto'],
                    'netto'=> $v['netto'],
                    'line_remarks'=> ''
            ));
        }

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtr_view_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $this->db->where('reff2', $data['dtr_id']);
        $this->db->update('dtr', array('no_dtr'=>$data['no_dtr'], 'tanggal'=> $data['tanggal']));
        $this->db->where('dtr_id', $data['dtr_id']);
        $this->db->update('dtr_detail', array(
            'tanggal_masuk'=>$data['tanggal']
        ));

        $this->db->where('reff2', $data['ttr_id']);
        $this->db->update('ttr', array('no_ttr'=>$data['no_ttr'], 'tanggal'=> $data['tanggal']));

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di ubah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function so_sync_post()
    {
        // set_time_limit(600);
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);
        $this->db->trans_start();
        foreach ($data as $key => $value) {
            $this->db->insert('t_spb_fg', array(
                'no_spb'=> $value['nomor_spb'],
                'tanggal'=> $value['tanggal'],
                'status'=> 0,
                'flag_tolling'=> 0,
                'jenis_spb'=> 4,
                'keterangan'=> $value['no_so']
            ));
            $spb_id = $this->db->insert_id();

            $this->db->insert('sales_order', [
                'reff2' => $value['id'],
                'no_sales_order' => $value['no_so'],
                'tanggal' => $value['tanggal'],
                'm_customer_id' => $value['idkmp'],
                'flag_tolling'=> 2,
                'flag_invoice'=> 0,
                'flag_sj'=> 0,
                'keterangan'=>''
            ]);
            $so_id = $this->db->insert_id();
            $this->db->insert('t_sales_order', array(
                'reff2'=> $value['id'],
                'id'=> $so_id,
                'so_id'=> $so_id,
                'no_po'=> $value['no_po'],
                'tgl_po'=> $value['tgl_po'],
                'term_of_payment'=> $value['term_of_payment'],
                'no_spb'=> $spb_id,
                'jenis_so'=> 0,
                'jenis_barang'=> 'FG',
                'currency'=> 'IDR',
                'kurs'=> 1
            ));

            foreach ($value['details'] as $key => $row) {
                $this->db->insert('t_spb_fg_detail', array(
                    'tanggal'=> $value['tanggal'],
                    't_spb_fg_id'=> $spb_id,
                    'jenis_barang_id'=> $row['jenis_barang_id'],
                    'uom'=> 'KG',
                    'netto'=> $row['netto'],
                    'keterangan'=> ''
                ));
                $id_detail = $this->db->insert_id();

                $this->db->insert('t_sales_order_detail', array(
                    'reff2'=> $row['id'],
                    't_so_id'=> $so_id,
                    'no_spb_detail'=> $id_detail,
                    'jenis_barang_id'=> $row['jenis_barang_id'],
                    'qty'=> 1,
                    'bruto'=> $row['netto'],
                    'netto'=> $row['netto'],
                    'amount'=> $row['amount'],
                    'total_amount'=> $row['total_amount']
                ));
            }
        }
        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()) {
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_sync_post()
    {
        set_time_limit(600);
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);
        // print_r($data); die();
        $this->db->trans_start();
        foreach ($data as $key => $value) {
            $so = $this->db->query("SELECT tso.id, tso.no_spb, tso.jenis_barang FROM t_sales_order tso LEFT JOIN sales_order so ON so.id = tso.so_id WHERE so.reff2 =".$value['r_so_id'])->row_array();
            // print_r($so); die();
            $this->db->where('id', $so['id']);
            $this->db->update('sales_order', array(
                'flag_sj' => 1
            ));
            //setting data SJ
            $data_tsj = array(
                'reff2' => $value['id'],
                'no_surat_jalan' => $value['no_sj_resmi'],
                'sales_order_id' => $so['id'],
                'po_id' => $value['r_po_id'],
                'spb_id' => $so['no_spb'],
                'retur_id' => 0,
                'inv_id' => 0,
                'status' => 1,
                'tanggal' => $value['tanggal'],
                'jenis_barang' => $value['jenis_barang'],
                'm_customer_id' => $value['idkmp'],
                'no_kendaraan' => $value['no_kendaraan'],
                'supir' => $value['supir'],
                'remarks' => $value['remarks']
            );
            $this->db->insert('t_surat_jalan', $data_tsj);
            $sj_id = $this->db->insert_id();
            // echo $this->db->trans_status(); die();

            //setting insert Gudang FG
            foreach ($value['gudang'] as $k => $v) {

                if($so['jenis_barang']=='FG'){

                    $this->db->where('id', $so['no_spb']);
                    $this->db->update('t_spb_fg', array(
                        'status'=>1
                    ));

                    $a_date = $value['tanggal'];
                    // $date = date("Y-m-t", strtotime($a_date));//TANGGAL TERAKHIR
                    $hari = date('d', strtotime($a_date));//HARI

                    if($hari <= 10){
                        $last_date = date("Y-m-j", strtotime("last day of previous month"));
                        $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
                    }else{
                        $last_date = $a_date;
                        $tgl_bpb = date('Ym', strtotime($last_date));//TANGGAL 201908
                    }
                    // echo $last_date;die();
                    //cek mau insert t_bpb_fg
                    $cek_bpb = $this->db->query("SELECT id, no_bpb_fg from t_bpb_fg where tanggal ='".$last_date."' AND jenis_barang_id = ".$v['jenis_barang_id'])->row_array();
                    // print_r($cek_bpb);die();
                    if(empty($cek_bpb)){
                        $this->db->insert('t_bpb_fg', array(
                            'no_bpb_fg' => 'BPB-KMP.'.$tgl_bpb.'.',
                            'tanggal' => $last_date,
                            'produksi_fg_id' => 0,
                            'jenis_barang_id' => $v['jenis_barang_id'],
                            'status' => 1,
                            'keterangan' => '',
                        ));
                        $bpb_id = $this->db->insert_id();
                        $no_bpb = 'BPB-KMP.'.$tgl_bpb.'.';
                    }else{
                        $bpb_id = $cek_bpb['id'];
                        $no_bpb = $cek_bpb['no_bpb_fg'];
                    }

                    //insert t_bpb_fg_detail
                    if($bpb_id > 0){
                        $cek_bpb_detail = $this->db->query("SELECT id from t_bpb_fg_detail where no_packing_barcode ='".$value['gudang'][$k]['no_packing']."'")->row_array();
                        if(empty($cek_bpb_detail)){
                            $this->db->insert('t_bpb_fg_detail', array(
                                't_bpb_fg_id' => $bpb_id,
                                'jenis_barang_id' => $value['gudang'][$k]['jenis_barang_id'],
                                'no_packing_barcode' => $value['gudang'][$k]['no_packing'],
                                'no_produksi' => 0,
                                'bruto' => $value['gudang'][$k]['bruto'],
                                'netto' => $value['gudang'][$k]['netto'],
                                'berat_bobbin' => $value['gudang'][$k]['berat_bobbin'],
                                'bobbin_id' => $value['gudang'][$k]['bobbin_id'],
                                'flag_taken' => 0
                            ));
                            $bpb_detail_id = $this->db->insert_id();
                        }else{
                            $bpb_detail_id = $cek_bpb_detail['id'];
                        }
                    }else{
                        $bpb_detail_id = 0;
                    }

                    //insert t_gudang_fg
                        $cek_gudang_fg = $this->db->query("SELECT id from t_gudang_fg where reff2 =".$value['gudang'][$k]['id'])->row_array();
                        // $spb = $this->db->query("select id from t_spb_fg where reff1 =".$value['gudang'][$k]['t_spb_fg_id'])->row_array();
                        if(empty($cek_gudang_fg)){
                            $this->db->insert('t_gudang_fg', array(
                                    'reff2' => $value['gudang'][$k]['id'],
                                    'tanggal'=> $value['gudang'][$k]['tanggal_masuk'],
                                    'jenis_trx' =>1, //0 masuk
                                    'flag_taken'=>1, // 0 belum diambil
                                    't_bpb_fg_id' => $bpb_id,
                                    't_bpb_fg_detail_id' => $bpb_detail_id,
                                    't_spb_fg_id' => $so['no_spb'],
                                    't_sj_id'=> $sj_id,
                                    'jenis_barang_id' => $value['gudang'][$k]['jenis_barang_id'],
                                    'nomor_BPB' => $no_bpb,
                                    'no_produksi' => 0,
                                    'no_packing' => $value['gudang'][$k]['no_packing'],
                                    'netto' => $value['gudang'][$k]['netto'],
                                    'bruto' => $value['gudang'][$k]['bruto'],
                                    'berat_bobbin' => $value['gudang'][$k]['berat_bobbin'],
                                    'bobbin_id' => $value['gudang'][$k]['bobbin_id'],
                                    'nomor_bobbin'=> $value['gudang'][$k]['nomor_bobbin'],
                                    'keterangan' => null,
                                    'tanggal_masuk' => $last_date,
                                    'tanggal_keluar' => $value['tanggal'],
                                    'created_by'=> 0,
                                    'created_at' => 0
                                ));
                            $gudang_id = $this->db->insert_id();
                        }else{
                            $this->db->where('id', $cek_gudang_fg['id']);
                            $this->db->update('t_gudang_fg', array(
                                'jenis_trx'=>1,
                                'flag_taken'=>1,
                                't_spb_fg_id'=> $so['no_spb'],
                                'tanggal_keluar' => $value['gudang'][$k]['tanggal_keluar']
                            ));
                            $gudang_id = $cek_gudang_fg['id'];
                        }

                    //setting data SJ
                    $this->db->insert('t_surat_jalan_detail', array(
                        'reff2'=> $value['details'][$k]['id'],
                        't_sj_id'=> $sj_id,
                        'gudang_id'=> $gudang_id,
                        'jenis_barang_id'=> $value['details'][$k]['jenis_barang_id'],
                        'jenis_barang_alias'=> 0,
                        'no_packing'=> $value['details'][$k]['no_packing'],
                        'qty'=> 1,
                        'bruto'=> $value['details'][$k]['bruto'],
                        'netto'=> $value['details'][$k]['netto'],
                        'nomor_bobbin'=> $value['details'][$k]['nomor_bobbin'],
                        'line_remarks'=> $value['details'][$k]['line_remarks'],
                        'created_by'=> 0,
                        'created_at'=> 0
                    ));
                }
            }

            // echo $this->db->trans_status();
            // die();
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function inv_sync_post() {
        // set_time_limit(600);
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);
        // print_r($data); die();
        $this->db->trans_start();
        foreach ($data as $key => $value) {
            $inv = $this->db->query("SELECT tsj.id, tsj.sales_order_id from t_surat_jalan tsj where tsj.reff2 =".$value['sjr_id'])->row_array();

            $this->db->insert('f_invoice', array(
                'reff2'=> $value['id'],
                'no_invoice'=> $value['no_invoice_jasa'],
                'nilai_invoice'=> $value['nilai_invoice'],
                'term_of_payment'=> $value['term_of_payment'],
                'bank_id'=> $value['bank_id'],
                'diskon'=> $value['diskon'],
                'add_cost'=> $value['cost'],
                'materai'=> $value['materai'],
                'currency'=> 'IDR',
                'kurs'=> 1,
                'nama_direktur'=> $value['nama_direktur'],
                'tanggal'=> $value['tanggal'],
                'tgl_jatuh_tempo'=> $value['jatuh_tempo'],
                'id_customer'=> $value['idkmp'],
                'id_sales_order'=> $inv['sales_order_id'],
                'id_surat_jalan'=> $inv['id'],
                'keterangan'=> $value['remarks']
            ));
            
            $inv_id = $this->db->insert_id();

            $this->db->where('id', $inv['id']);
            $this->db->update('t_surat_jalan', array(
                'inv_id'=>$inv_id
            ));

            $this->db->where('id', $inv['sales_order_id']);
            $this->db->update('sales_order', array(
                'flag_invoice'=>1
            ));

            foreach ($value['details'] as $i => $v) {
                $this->db->insert('f_invoice_detail', array(
                        'reff2'=>$v['id'],
                        'id_invoice'=>$inv_id,
                        'sj_detail_id'=>$inv['id'],
                        'jenis_barang_id'=>$v['jenis_barang_id'],
                        'qty'=>$v['qty'],
                        'netto'=>$v['netto'],
                        'harga'=>$v['amount'],
                        'total_harga'=>$v['total_amount'],
                ));
            }
        }
        

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function bpb_post() {
        // set_time_limit(600);
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);
        // print_r($data); die();
        $this->db->trans_start();
            $tgl_code = date('Ym', strtotime($data['tanggal']));
            $code_bpb = 'BPB-KMP.'.$tgl_code.'.'.$this->input->post('no_bpb');

            $data_bpb = array(
                'no_bpb_fg'=> $code_bpb,
                'status'=> 1,
                'tanggal'=> $data['tanggal'],
                'jenis_barang_id'=> $data['jb'],
                'keterangan'=> $this->input->post('remarks')
            );
            $this->db->insert('t_bpb_fg', $data_bpb);
            $bpb_id = $this->db->insert_id();

            foreach ($data['details'] as $i => $v) {
                
                //insert t_bpb_fg_detail
                    if($bpb_id > 0){
                        $cek_bpb_detail = $this->db->query("SELECT id from t_bpb_fg_detail where no_packing_barcode ='".$v['no_packing']."'")->row_array();
                        if(empty($cek_bpb_detail)){
                            $this->db->insert('t_bpb_fg_detail', array(
                                'reff1'=> $v['t_bpb_fg_detail_id'],
                                't_bpb_fg_id' => $bpb_id,
                                'jenis_barang_id' => $v['jenis_barang_id'],
                                'no_packing_barcode' => $v['no_packing'],
                                'no_produksi' => 0,
                                'bruto' => $v['bruto'],
                                'netto' => $v['netto'],
                                'berat_bobbin' => $v['berat_bobbin'],
                                'bobbin_id' => $v['bobbin_id'],
                                'flag_taken' => 0
                            ));
                            $bpb_detail_id = $this->db->insert_id();
                        }else{
                            $bpb_detail_id = $cek_bpb_detail['id'];
                        }
                    }else{
                        $bpb_detail_id = 0;
                    }

                    //insert t_gudang_fg
                    $cek_gudang_fg = $this->db->query("SELECT id from t_gudang_fg where no_packing ='".$v['no_packing']."'")->row_array();
                        // $spb = $this->db->query("select id from t_spb_fg where reff1 =".$v['t_spb_fg_id'])->row_array();
                        if(empty($cek_gudang_fg)){
                            $this->db->insert('t_gudang_fg', array(
                                    'reff1' => $v['id'],
                                    'tanggal'=> $data['tanggal'],
                                    'jenis_trx' =>0, //0 masuk
                                    'flag_taken'=>0, // 0 belum diambil
                                    't_bpb_fg_id' => $bpb_id,
                                    't_bpb_fg_detail_id' => $bpb_detail_id,
                                    't_spb_fg_id' => null,
                                    't_sj_id'=> 0,
                                    'jenis_barang_id' => $v['jenis_barang_id'],
                                    'nomor_BPB' => $code_bpb,
                                    'no_produksi' => 0,
                                    'no_packing' => $v['no_packing'],
                                    'netto' => $v['netto'],
                                    'bruto' => $v['bruto'],
                                    'berat_bobbin' => $v['berat_bobbin'],
                                    'bobbin_id' => $v['bobbin_id'],
                                    'nomor_bobbin'=> $v['nomor_bobbin'],
                                    'keterangan' => null,
                                    'tanggal_masuk' => $data['tanggal'],
                                    'tanggal_keluar' => 0,
                                    'created_by'=> 0,
                                    'created_at' => 0
                                ));
                            $gudang_id = $this->db->insert_id();
                        }
            }

        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function tolling_sync_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);
        
        $this->db->trans_start();
        foreach ($data as $key => $value) {


            $so = $this->db->query("select id from sales_order where reff2 =".$value['so_id'])->row_array();
            // print_r($so);die();
            if(empty($so)){
                $so['id'] = 0;
            }

            $dtr = $this->db->query("select id from dtr where reff2 =".$value['id'])->row_array();
            if(empty($dtr)){
                echo $value['id'].' gaada <br>';
                $this->db->insert('dtr', array(
                    'reff2'=> $value['id'],
                    'no_dtr'=> $value['no_dtr_resmi'],
                    'tanggal'=>$value['tanggal'],
                    'po_id'=>0,
                    'so_id'=>$so['id'],
                    'retur_id'=>0,
                    'prd_id'=>0,
                    'flag_taken'=>0,
                    'supplier_id'=>0,
                    'customer_id'=>$value['idkmp'],
                    'jenis_barang'=>'RONGSOK',
                    'remarks'=>'',
                    'status'=>1
                ));
                $dtr_id = $this->db->insert_id();
                // echo $dtr_id;die();
            }else{
                echo $value['id'].' ada <br>';
                $this->db->where('id', $dtr['id']);
                $this->db->update('dtr', array(
                    'no_dtr'=> $value['no_dtr_resmi'],
                    'tanggal'=> $value['tanggal'],
                    'so_id'=> $so['id']
                ));
                $dtr_id = $dtr['id'];
            }

            $ttr = $this->db->query("select id from ttr where reff2 =".$value['id_ttr'])->row_array();
            if(empty($ttr)){
                $this->db->insert('ttr', array(
                    'reff2'=> $value['id_ttr'],
                    'no_ttr'=> $value['no_ttr_resmi'],
                    'tanggal'=> $value['tanggal'],
                    'no_sj'=> $value['no_sj_resmi'],
                    'dtr_id'=> $dtr_id,
                    'jmlh_afkiran'=> 0,
                    'jmlh_pengepakan'=> 0,
                    'jmlh_lain'=> 0,
                    'remarks' => '',
                    'flag_bayar' => 0,
                    'flag_produksi' => 0,
                    'ttr_status' => 1
                ));
                $ttr_id = $this->db->insert_id();
            }else{
                $ttr_id = $ttr['id'];
            }

            foreach ($value['details'] as $i => $v) {
                $dd = $this->db->query("select id from dtr_detail where reff2 =".$v['id'])->row_array();
                if(empty($dd)){
                    $this->db->insert('dtr_detail', array(
                            'reff2'=> $v['id'],
                            'dtr_id'=> $dtr_id,
                            'po_detail_id'=> 0,
                            'so_id'=> 0,
                            'retur_id'=> 0,
                            'rongsok_id'=> $v['rongsok_id'],
                            'qty'=> $v['qty'],
                            'bruto'=> $v['bruto'],
                            'berat_palette'=> $v['berat_pallete'],
                            'netto'=> $v['netto'],
                            'netto_resmi'=> 0,
                            'line_remarks'=> '',
                            'no_pallete'=> $v['no_pallete'],
                            'tanggal_masuk'=> $value['tanggal'],
                            'tanggal_keluar'=> null,
                            'flag_taken'=> 0,
                            'flag_resmi'=> 0
                    ));
                    $dtr_detail_id = $this->db->insert_id();
                }else{
                    $dtr_detail_id = $dd['id'];
                }

                $td = $this->db->query("select id from ttr_detail where reff2 =".$v['id'])->row_array();
                if(empty($td)){
                    $this->db->insert('ttr_detail', array(
                            'reff2'=> $v['ttr_detail_id'],
                            'ttr_id'=> $ttr_id,
                            'dtr_detail_id'=> $dtr_detail_id,
                            'rongsok_id'=> $v['rongsok_id'],
                            'ampas_id'=> 0,
                            'qty'=> $v['qty'],
                            'bruto'=> $v['bruto'],
                            'netto'=> $v['netto'],
                            'line_remarks'=> ''
                    ));
                }
            }
        }
        // echo $this->db->trans_status();die();

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}