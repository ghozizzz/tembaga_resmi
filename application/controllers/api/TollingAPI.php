<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class TollingAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_jenis_barang');
	}

    public function dtt_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $po = $this->db->query("select id, status from po where reff1 =".$data['po_id'])->row_array();
        if($po['status'] != $data['po']['status']){
            $this->db->where('id', $po['id']);
            $this->db->update('po', $data['po']);
        }

        $data['dtt']['reff1'] = $data['dtt']['id'];
        $data['dtt']['po_id'] = $po['id'];
        unset($data['dtt']['id']);
        unset($data['dtt']['flag_ppn']);

        $this->db->insert('dtt', $data['dtt']);
        $dtt_id = $this->db->insert_id();

        $dtt_detail = [];
        foreach ($data['details'] as $i => $v){
            $data['details'][$i]['dtt_id'] = $dtt_id;
            $get_detail = $this->db->query("select id from po_detail where reff1=".$v['po_detail_id'])->row_array();
            $data['details'][$i]['po_detail_id'] = $get_detail['id'];
            $data['details'][$i]['reff1'] = $v['id'];
            unset($data['details'][$i]['id']);
        }

        $this->db->insert_batch('dtt_detail', $data['details']);

        if($data['jenis']=='FG'){
            $this->db->insert('t_bpb_fg', $data['data_bpb']);
            $bpb_fg_id = $this->db->insert_id();

            $bpb_detail = [];
            foreach ($data['details_bpb'] as $key => $value) {
                $data['details_bpb'][$key]['t_bpb_fg_id'] = $bpb_fg_id;
                $data['details_bpb'][$key]['reff1'] = $value['id'];
                unset($data['details_bpb'][$key]['id']);
            }

            $this->db->insert_batch('t_bpb_fg_detail', $data['details_bpb']);
        }elseif($data['jenis']=='WIP'){
            $this->db->insert('t_bpb_wip', $data['data_bpb']);
            $bpb_wip_id = $this->db->insert_id();

            $bpb_detail = [];
            foreach ($data['details_bpb'] as $key => $value) {
                $data['details_bpb'][$key]['bpb_wip_id'] = $bpb_wip_id;
                $data['details_bpb'][$key]['reff1'] = $value['id'];
                unset($data['details_bpb'][$key]['id']);
            }

            $this->db->insert_batch('t_bpb_wip_detail', $data['details_bpb']);
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtr_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select id from sales_order where reff1 =".$data['master']['so_id'])->row_array();

        $this->db->insert('dtr', array(
            'reff1'=>$data['master']['dtr_id'],
            'no_dtr'=>$data['master']['no_dtr'],
            'tanggal'=>$data['master']['tgl_dtr'],
            'so_id'=>$so['id'],
            'customer_id'=>$data['master']['customer_id'],
            'jenis_barang'=>$data['master']['jenis_barang'],
            'remarks'=>$data['master']['remarks_dtr'],
            'status'=>$data['master']['dtr_status']
        ));
        $dtr_id = $this->db->insert_id();

        $this->db->insert('ttr', array(
            'reff1'=>$data['master']['id'],
            'no_ttr'=>$data['master']['no_ttr'],
            'tanggal'=>$data['master']['tgl_ttr'],
            'dtr_id'=>$dtr_id,
            'no_sj'=>$data['master']['no_sj'],
            'jmlh_afkiran'=>$data['master']['jmlh_afkiran'],
            'jmlh_pengepakan'=>$data['master']['jmlh_pengepakan'],
            'jmlh_lain'=>$data['master']['jmlh_lain'],
            'remarks'=>$data['master']['remarks_ttr'],
            'ttr_status'=>1
        ));
        $ttr_id = $this->db->insert_id();

        $dtr_detail = [];
        $ttr_detail = [];

        foreach ($data['detail'] as $i => $v) {
            $dtr_detail[$i]['dtr_id'] = $dtr_id;
            $dtr_detail[$i]['reff1'] = $v['dtr_detail_id'];
            // $get_detail = $this->db->query("select id from po_detail where reff1=".$v['po_detail_id'])->row_array();
            $dtr_detail[$i]['po_detail_id'] = 0;
            $dtr_detail[$i]['rongsok_id'] = $v['rongsok_id'];
            $dtr_detail[$i]['qty'] = $v['qty'];
            $dtr_detail[$i]['bruto'] = $v['bruto'];
            $dtr_detail[$i]['berat_palette'] = $v['berat_palette'];
            $dtr_detail[$i]['netto'] = $v['netto'];
            $dtr_detail[$i]['line_remarks'] = $v['line_remarks'];
            $dtr_detail[$i]['no_pallete'] = $v['no_pallete'];
            $dtr_detail[$i]['tanggal_masuk'] = $v['tanggal_masuk'];
        }
        $this->db->insert_batch('dtr_detail', $dtr_detail);

        foreach ($data['detail'] as $key => $value) {
            $ttr_detail[$key]['reff1'] = $value['id'];
            $ttr_detail[$key]['ttr_id'] = $ttr_id;
            $get_ttr = $this->db->query("select id from dtr_detail where reff1=".$value['dtr_detail_id'])->row_array();
            $ttr_detail[$key]['dtr_detail_id'] = $get_ttr['id'];
            $ttr_detail[$key]['rongsok_id'] = $value['rongsok_id'];
            $ttr_detail[$key]['qty'] = $value['qty'];
            $ttr_detail[$key]['bruto'] = $value['bruto'];
            $ttr_detail[$key]['netto'] = $value['netto'];
            $ttr_detail[$key]['line_remarks'] = $value['line_remarks'];
        }

        // print_r($ttr_detail);
        // die();
        $this->db->insert_batch('ttr_detail', $ttr_detail);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sj_keluar_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $jb = $data['tsj']['jenis_barang'];

        $so = $this->db->query("select id from po where reff1 =".$data['tsj']['po_id'])->row_array();

        //insert spb

        if($jb == 'FG'){
            $spb = $this->db->query("select id from t_spb_wip where reff1=".$data['spb']['id'])->row_array();
            if(empty($spb['id'])){
                $data_spb = array(
                    'reff1'=> $data['spb']['id'],
                    'no_spb'=> $data['spb']['no_spb'],
                    'tanggal'=> $data['spb']['tanggal'],
                    'jenis_spb'=> $data['spb']['jenis_spb'],
                    'flag_tolling'=> $data['spb']['flag_tolling'],
                    'keterangan'=> $data['spb']['keterangan'],
                    'status'=> $data['spb']['status']
                );
                $this->db->insert('t_spb_fg', $data_spb);
                $spbid = $this->db->insert_id();

                foreach ($data['spb_detail'] as $i => $item) {
                    $data['spb_detail'][$i]['reff1'] = $data['spb_detail'][$i]['id'];
                    $data['spb_detail'][$i]['t_spb_fg_id'] = $spbid;
                    unset($data['spb_detail'][$i]['id']);
                    $this->db->insert('t_spb_fg_detail', $data['spb_detail'][$i]);
                }
                // $this->db->insert_batch('spb_detail', $data['spb_detail']);
            }else{
                $this->db->where('id', $spb['id']);
                $this->db->update('t_spb_fg', array(
                    'flag_tolling'=> $data['spb']['flag_tolling'],
                    'status'=> $data['spb']['status']
                ));
                $spbid = $spb['id'];
            }
        }elseif($jb == 'WIP'){
            $spb = $this->db->query("select id from t_spb_wip where reff1=".$data['spb']['id'])->row_array();
            if(empty($spb['id'])){
                $data_spb = array(
                    'reff1'=> $data['spb']['id'],
                    'no_spb_wip'=> $data['spb']['no_spb_wip'],
                    'tanggal'=> $data['spb']['tanggal'],
                    'flag_produksi'=> $data['spb']['flag_produksi'],
                    'flag_tolling'=> $data['spb']['flag_tolling'],
                    'keterangan'=> $data['spb']['keterangan'],
                    'status'=> $data['spb']['status']
                );
                $this->db->insert('t_spb_wip', $data_spb);
                $spbid = $this->db->insert_id();
                // print_r($data_spb);

                foreach ($data['spb_detail'] as $i => $item) {
                    $data['spb_detail'][$i]['reff1'] = $data['spb_detail'][$i]['id'];
                    $data['spb_detail'][$i]['t_spb_wip_id'] = $spbid;
                    unset($data['spb_detail'][$i]['id']);
                    $this->db->insert('t_spb_wip_detail', $data['spb_detail'][$i]);
                }
                // $this->db->insert_batch('spb_detail', $data['spb_detail']);
            }else{
                $this->db->where('id', $spb['id']);
                $this->db->update('t_spb_wip', array(
                    'flag_tolling'=> $data['spb']['flag_tolling'],
                    'status'=> $data['spb']['status']
                ));
                $spbid = $spb['id'];
            }
        }elseif($jb == 'RONGSOK'){
            $spb = $this->db->query("select id from spb where reff1=".$data['spb']['id'])->row_array();
            if(empty($spb['id'])){
                $data_spb = array(
                    'reff1'=> $data['spb']['id'],
                    'no_spb'=> $data['spb']['no_spb'],
                    'tanggal'=> $data['spb']['tanggal'],
                    'produksi_ingot_id'=> $data['spb']['produksi_ingot_id'],
                    'jenis_barang'=> $data['spb']['jenis_barang'],
                    'flag_tolling'=> $data['spb']['flag_tolling'],
                    'jenis_spb'=> $data['spb']['jenis_spb'],
                    'jumlah'=> $data['spb']['jumlah'],
                    'remarks'=> $data['spb']['remarks'],
                    'status'=> $data['spb']['status']
                );
                $this->db->insert('spb', $data_spb);
                $spbid = $this->db->insert_id();
                // print_r($data_spb);

                foreach ($data['spb_detail'] as $i => $item) {
                    $data['spb_detail'][$i]['reff1'] = $data['spb_detail'][$i]['id'];
                    $data['spb_detail'][$i]['spb_id'] = $spbid;
                    unset($data['spb_detail'][$i]['id']);
                    unset($data['spb_detail'][$i]['created']);
                    unset($data['spb_detail'][$i]['created_by']);
                    unset($data['spb_detail'][$i]['modified']);
                    unset($data['spb_detail'][$i]['modified_by']);

                    $this->db->insert('spb_detail', $data['spb_detail'][$i]);
                }
                // $this->db->insert_batch('spb_detail', $data['spb_detail']);
            }else{
                $this->db->where('id', $spb['id']);
                $this->db->update('spb', array(
                    'flag_tolling'=> $data['spb']['flag_tolling'],
                    'status'=> $data['spb']['status']
                ));
                $spbid = $spb['id'];
            }
        }
        // print_r($data['spb_detail']);
        // die();

        //setting data SJ
        $data_tsj = array(
            'reff1' => $data['tsj']['id'],
            'no_surat_jalan' => $data['tsj']['no_surat_jalan'],
            'sales_order_id' => $data['tsj']['sales_order_id'],
            'po_id' => $so['id'],
            'spb_id' => $spbid,
            'retur_id' => $data['tsj']['retur_id'],
            'inv_id' => $data['tsj']['inv_id'],
            'status' => $data['tsj']['status'],
            'tanggal' => $data['tsj']['tanggal'],
            'jenis_barang' => $data['tsj']['jenis_barang'],
            'm_customer_id' => $data['tsj']['m_customer_id'],
            'no_kendaraan' => $data['tsj']['no_kendaraan'],
            'supir' => $data['tsj']['supir'],
            'remarks' => $data['tsj']['remarks']
        );
        $this->db->insert('t_surat_jalan', $data_tsj);
        $sj_id = $this->db->insert_id();

        //setting insert Gudang FG
        foreach ($data['gudang'] as $k => $v) {

        if($jb=='FG'){
            //cek mau insert produksi_fg
            if($data['gudang'][$k]['id_prd']){
                $cek_pf = $this->db->query("select id from produksi_fg where reff1 =".$data['gudang'][$k]['id_prd'])->row_array();
                if(empty($cek_pf)){
                    $this->db->insert('produksi_fg', array(
                        'reff1' => $data['gudang'][$k]['id_prd'],
                        'no_laporan_produksi' => $data['gudang'][$k]['no_laporan_produksi'],
                        'tanggal' => $data['gudang'][$k]['tgl_prd'],
                        'flag_result' => $data['gudang'][$k]['flag_result'],
                        'remarks' => $data['gudang'][$k]['remarks_prd'],
                        'jenis_barang_id' => $data['gudang'][$k]['jb_prd'],
                        'jenis_packing_id' => $data['gudang'][$k]['jenis_packing_id']
                    ));
                    $pf_id = $this->db->insert_id();
                }else{
                    $pf_id = $cek_pf['id'];
                }
            }else{
                $pf_id = 0;
            }

            //insert produksi_fg_detail
            if($pf_id > 0){
                $this->db->insert('produksi_fg_detail', array(
                    'produksi_fg_id' => $pf_id,
                    'tanggal' => $data['gudang'][$k]['tgl_prd'],
                    'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
                    'no_produksi' => $data['gudang'][$k]['no_produksi'],
                    'bruto' => $data['gudang'][$k]['bruto'],
                    'netto' => $data['gudang'][$k]['netto'],
                    'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                    'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                    'keterangan' => $data['gudang'][$k]['remarks_prd']
                ));
            }

            //cek mau insert t_bpb_fg
            $cek_bpb = $this->db->query("select id from t_bpb_fg where reff1 =".$data['gudang'][$k]['t_bpb_fg_id'])->row_array();
            if(empty($cek_bpb)){
                if($cek_bpb['id']!=0){
                    $this->db->insert('t_bpb_fg', array(
                        'reff1' => $data['gudang'][$k]['t_bpb_fg_id'],
                        'no_bpb_fg' => $data['gudang'][$k]['no_bpb_fg'],
                        'tanggal' => $data['gudang'][$k]['tgl_bpb'],
                        'produksi_fg_id' => $pf_id,
                        'jenis_barang_id' => $data['gudang'][$k]['jb_bpb'],
                        'status' => 1,
                        'keterangan' => $data['gudang'][$k]['ket_bpb']
                    ));
                    $bpb_id = $this->db->insert_id();
                }else{
                    $bpb_id = 0;
                }
            }else{
                $bpb_id = $cek_bpb['id'];
            }

            //insert t_bpb_fg_detail
            if($bpb_id > 0){
                $cek_bpb_detail = $this->db->query("select id from t_bpb_fg_detail where reff1 =".$data['gudang'][$k]['id_bpb_d'])->row_array();
                if(empty($cek_bpb_detail)){
                    $this->db->insert('t_bpb_fg_detail', array(
                        'reff1' => $data['gudang'][$k]['id_bpb_d'],
                        't_bpb_fg_id' => $bpb_id,
                        'jenis_barang_id' => $data['gudang'][$k]['jenis_barang_id'],
                        'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
                        'no_produksi' => $data['gudang'][$k]['no_produksi'],
                        'bruto' => $data['gudang'][$k]['bruto'],
                        'netto' => $data['gudang'][$k]['netto'],
                        'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                        'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                        'flag_taken' => 0
                    ));
                    $bpb_detail_id = $this->db->insert_id();
                }else{
                    $bpb_detail_id = $cek_bpb_detail['id'];
                }
            }else{
                $bpb_detail_id = 0;
            }

            //insert t_gudang_fg
                $cek_gudang_fg = $this->db->query("select id from t_gudang_fg where reff1 =".$data['gudang'][$k]['id'])->row_array();
                $spb = $this->db->query("select id from t_spb_fg where reff1 =".$data['gudang'][$k]['t_spb_fg_id'])->row_array();
                if(empty($cek_gudang_fg)){
                    $this->db->insert('t_gudang_fg', array(
                            'reff1' => $data['gudang'][$k]['id'],
                            'tanggal'=> $data['gudang'][$k]['tanggal'],
                            'jenis_trx' =>1, //0 masuk
                            'flag_taken'=>1, // 0 belum diambil
                            't_bpb_fg_id' => $bpb_id,
                            't_bpb_fg_detail_id' => $bpb_detail_id,
                            't_spb_fg_id' => $spb['id'],
                            'jenis_barang_id' => $data['gudang'][$k]['jenis_barang_id'],
                            'nomor_BPB' => $data['gudang'][$k]['nomor_BPB'],
                            'no_produksi' => $data['gudang'][$k]['no_produksi'],
                            'no_packing' => $data['gudang'][$k]['no_packing'],
                            'netto' => $data['gudang'][$k]['netto'],
                            'bruto' => $data['gudang'][$k]['bruto'],
                            'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                            'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                            'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                            'keterangan' => $data['gudang'][$k]['keterangan'],
                            'tanggal_masuk' => $data['gudang'][$k]['tanggal_masuk'],
                            'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar'],
                            'created_by'=> 0,
                            'created_at' => 0
                        ));
                    $gudang_id = $this->db->insert_id();
                }else{
                    $this->db->where('id', $cek_gudang_fg['id']);
                    $this->db->update('t_gudang_fg', array(
                        'jenis_trx'=>1,
                        'flag_taken'=>1,
                        't_spb_fg_id'=> $spb['id'],
                        'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar']
                    ));
                    $gudang_id = $cek_gudang_fg['id'];
                }

            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $data['gudang'][$k]['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $gudang_id,
                'jenis_barang_id'=> $data['gudang'][$k]['jenis_barang_id'],
                'jenis_barang_alias'=> $data['gudang'][$k]['jenis_barang_alias'],
                'no_packing'=> $data['gudang'][$k]['no_packing'],
                'qty'=> $data['gudang'][$k]['jb_qty'],
                'bruto'=> $data['gudang'][$k]['bruto'],
                'netto'=> $data['gudang'][$k]['netto'],
                'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                'line_remarks'=> $data['gudang'][$k]['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }elseif($jb=='WIP'){
            $spb_detail = $this->db->query("select id from t_spb_wip_detail where t_spb_wip_id =".$spbid." and jenis_barang_id=".$v['jenis_barang_id'])->row_array();

            $this->db->insert('t_spb_wip_fulfilment', array(
                't_spb_wip_id'=>$spbid,
                't_spb_wip_detail_id'=>$spb_detail['id'],
                'jenis_barang_id'=>$v['jenis_barang_id'],
                'qty'=>$v['qty'],
                'berat'=>$v['berat'],
                'keterangan'=>$v['keterangan']
            ));

            //setting t_gudang_wip
            $this->db->insert('t_gudang_wip', array(
                'reff1'=>$v['id'],
                'tanggal'=>$v['tanggal'],
                'flag_taken'=>$v['flag_taken'],
                'jenis_trx'=>$v['jenis_trx'],
                't_hasil_wip_id'=>$v['t_hasil_wip_id'],
                'jenis_barang_id'=>$v['jenis_barang_id'],
                't_spb_wip_id'=>$spbid,
                't_spb_wip_detail_id'=>$spb_detail['id'],
                't_bpb_wip_detail_id'=>$v['t_bpb_wip_detail_id'],
                'qty'=>$v['qty'],
                'uom'=>$v['uom'],
                'berat'=>$v['berat'],
                'keterangan'=>$v['keterangan']
            ));
            $gudang_id = $this->db->insert_id();

            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $data['gudang'][$k]['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $gudang_id,
                'jenis_barang_id'=> $data['gudang'][$k]['jenis_barang_id'],
                'jenis_barang_alias'=> $data['gudang'][$k]['jenis_barang_alias'],
                'no_packing'=> 0,
                'qty'=> $data['gudang'][$k]['jb_qty'],
                'bruto'=> 0,
                'netto'=> $data['gudang'][$k]['berat'],
                'nomor_bobbin'=> 0,
                'line_remarks'=> $data['gudang'][$k]['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }elseif($jb=='RONGSOK'){
            //NOTE ADA DI NOTE ROOT FOLDER!!!!
            // cek insert dtr pertama udah apa belum
                $cek_dtr = $this->db->query("select dtr.id, ttr.id as id_ttr from dtr left join ttr on ttr.dtr_id = dtr.id where dtr.reff2 =".$sj_id)->row_array();
                if(empty($cek_dtr)){
                    $nomor = substr($data['tsj']['no_surat_jalan'],7,11);
                    // echo $nomor;
                    // die();
                    $this->db->insert('dtr', array(
                        'reff2' => $sj_id,
                        'no_dtr' => 'DTR-KMP.'.$nomor,
                        'tanggal' => $data['tsj']['tanggal'],
                        'po_id' => $data['tsj']['po_id'],
                        'so_id' => 0,
                        'retur_id' => $data['tsj']['retur_id'],
                        'prd_id' => 0,
                        'flag_taken' => 0,
                        'supplier_id' => 0,
                        'customer_id' => 0,
                        'jenis_barang' => $data['tsj']['jenis_barang'],
                        'remarks' => '',
                        'status' => 1
                    ));
                    $dtr_id = $this->db->insert_id();

                    $this->db->insert('ttr', array(
                        'reff2' => $sj_id,
                        'no_ttr' => 'TTR-KMP.'.$nomor,
                        'tanggal' => $data['tsj']['tanggal'],
                        'no_sj' => '',
                        'dtr_id' => $dtr_id,
                        'jmlh_afkiran' => 0,
                        'jmlh_pengepakan' => 0,
                        'jmlh_lain' => 0,
                        'remarks' => 0,
                        'flag_bayar' => 0,
                        'flag_produksi' => 0,
                        'ttr_status' => 1
                    ));
                    $ttr_id = $this->db->insert_id();
                }else{
                    $dtr_id = $cek_dtr['id'];
                    $ttr_id = $cek_dtr['id_ttr'];
                }

                //insert produksi_fg_detail

                //CEK DTR DETAILNYA UDAH ADA APA BELUM
                $cek_dd = $this->db->query("select dd.id, td.id as id_ttr from dtr_detail dd left join ttr_detail td on td.dtr_detail_id = dd.id where dd.reff1 =".$v['id'])->row_array();

                if(empty($cek_dd['id'])){
                    $this->db->insert('dtr_detail', array(
                        'reff1'=> $v['id'],
                        'dtr_id' => $dtr_id,
                        'po_detail_id' => 0,
                        'so_id' => 0,
                        'retur_id' => 0,
                        'rongsok_id' => $v['rongsok_id'],
                        'qty' => $v['qty'],
                        'bruto' => $v['bruto'],
                        'berat_palette' => $v['berat_palette'],
                        'netto' => $v['netto'],
                        'no_pallete' => $v['no_pallete'],
                        'tanggal_masuk' => $v['tanggal_masuk'],
                        'tanggal_keluar'=> $v['tanggal_keluar'],
                        'flag_taken'=> 1,
                        'flag_sj'=> 1,
                        'line_remarks' => ''
                    ));
                    $dtr_detail_id = $this->db->insert_id();

                    $this->db->insert('ttr_detail', array(
                        'reff1'=> $v['id'],
                        'ttr_id'=> $ttr_id,
                        'dtr_detail_id'=> $dtr_detail_id,
                        'rongsok_id'=> $v['rongsok_id'],
                        'ampas_id'=> 0,
                        'qty' => $v['qty'],
                        'bruto' => $v['bruto'],
                        'netto' => $v['netto'],
                        'line_remarks' => ''
                    ));
                    $ttr_detail_id = $this->db->insert_id();
                }else{
                    $this->db->where('id', $cek_dd['id']);
                    $this->db->update('dtr_detail', array(
                        'flag_sj'=>1
                    ));
                    $dtr_detail_id = $cek_dd['id'];
                    $ttr_detail_id = $cek_dd['id_ttr'];
                }

                    $this->db->insert('spb_detail_fulfilment',array(
                        'spb_id'=>$spbid,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'ttr_id'=>$ttr_id
                    ));

                // echo $this->db->insert_id();
                // die();
            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $v['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $dtr_detail_id,
                'jenis_barang_id'=> $v['rongsok_id'],
                'jenis_barang_alias'=> $v['jenis_barang_alias'],
                'no_packing'=> $v['no_pallete'],
                'qty'=> $v['jb_qty'],
                'bruto'=> $v['bruto'],
                'berat'=> $v['berat_palette'],
                'netto'=> $v['netto'],
                'nomor_bobbin'=> $v['nomor_bobbin'],
                'line_remarks'=> $v['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }//IF JENIS BARANG
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtwip_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select id from sales_order where reff1 =".$data['so_id'])->row_array();

            $this->db->where('id', $so['id']);
            $this->db->update('sales_order', array(
                'flag_tolling' => $data['flag_tolling']
            ));

        $data['dtwip']['reff1'] = $data['dtwip']['id'];
        $data['dtwip']['so_id'] = $so['id'];
        unset($data['dtwip']['api']);
        unset($data['dtwip']['id']);
        unset($data['dtwip']['flag_ppn']);

        $this->db->insert('dtwip', $data['dtwip']);
        $dtwip_id = $this->db->insert_id();

        $dtwip_detail = [];
        foreach ($data['details'] as $i => $v){
            $data['details'][$i]['dtwip_id'] = $dtwip_id;
            $data['details'][$i]['reff1'] = $v['id'];
            unset($data['details'][$i]['id']);
        }
        
        $this->db->insert_batch('dtwip_detail', $data['details']);

        $this->db->insert('t_bpb_wip', $data['data_bpb']);
        $bpb_wip_id = $this->db->insert_id();

        $bpb_detail = [];
        foreach ($data['details_bpb'] as $key => $value) {
            $data['details_bpb'][$key]['bpb_wip_id'] = $bpb_wip_id;
            $data['details_bpb'][$key]['reff1'] = $value['id'];
            unset($data['details_bpb'][$key]['id']);
        }

        $this->db->insert_batch('t_bpb_wip_detail', $data['details_bpb']);

        // echo $this->db->trans_status();die();
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}