<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class BeliRongsokAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_jenis_barang');
	}

	public function po_post(){
    	$data = [
            'reff1'=> $this->post('reff1'),
            'no_po'=> $this->post('no_po'),
            'tanggal'=> $this->post('tanggal'),
            'flag_tolling'=> $this->post('flag_tolling'),
            'ppn'=>$this->post('ppn'),
            'diskon'=>$this->post('diskon'),
            'materai'=>$this->post('materai'),
            'currency'=>$this->post('currency'),
            'kurs'=>$this->post('kurs'),
            'supplier_id'=>$this->post('supplier_id'),
            'term_of_payment'=>$this->post('term_of_payment'),
            'jenis_po'=>$this->post('jenis_po')
        ];

    		$this->db->insert('po', $data);
        	$id = $this->db->insert_id();

    	if($id > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah',
    			'id'=> $id
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function po_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $get = $this->db->query("select id from po where reff1 =".$data['po_id'])->row_array();

        $this->db->where('id', $get['id']);
        $this->db->update('po', $data['master']);
        
        $this->db->where('po_id', $get['id']);
        $this->db->delete('po_detail');

        foreach ($data['details'] as $i => $item) {
            $data['details'][$i]['po_id'] = $get['id'];
            $data['details'][$i]['reff1'] = $data['details'][$i]['id'];
            unset($data['details'][$i]['id']);
        }

        if($this->db->insert_batch('po_detail', $data['details'])){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function po_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $get = $this->db->query("select id from po where reff1 =".$data['po_id'])->row_array();

        $this->db->where('id', $get['id']);
        $this->db->update('po', $data['master']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function dtr_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $po = $this->db->query("select id, status from po where reff1 =".$data['master']['po_id'])->row_array();
        if($po['status'] != $data['master']['po_status']){
            $this->db->where('id', $po['id']);
            $this->db->update('po', array('status'=>$data['master']['po_status']));
        }

        $this->db->insert('dtr', array(
            'reff1'=>$data['master']['dtr_id'],
            'no_dtr'=>$data['master']['no_dtr'],
            'tanggal'=>$data['master']['tgl_dtr'],
            'po_id'=>$po['id'],
            'supplier_id'=>$data['master']['supplier_id'],
            'jenis_barang'=>$data['master']['jenis_barang'],
            'remarks'=>$data['master']['remarks_dtr'],
            'status'=>$data['master']['dtr_status']
        ));
        $dtr_id = $this->db->insert_id();

        $this->db->insert('ttr', array(
            'reff1'=>$data['master']['id'],
            'no_ttr'=>$data['master']['no_ttr'],
            'tanggal'=>$data['master']['tgl_ttr'],
            'dtr_id'=>$dtr_id,
            'no_sj'=>$data['master']['no_sj'],
            'jmlh_afkiran'=>$data['master']['jmlh_afkiran'],
            'jmlh_pengepakan'=>$data['master']['jmlh_pengepakan'],
            'jmlh_lain'=>$data['master']['jmlh_lain'],
            'remarks'=>$data['master']['remarks_ttr'],
            'ttr_status'=>1
        ));
        $ttr_id = $this->db->insert_id();

        $dtr_detail = [];
        $ttr_detail = [];

        foreach ($data['detail'] as $i => $v) {
            $dtr_detail[$i]['dtr_id'] = $dtr_id;
            $dtr_detail[$i]['reff1'] = $v['dtr_detail_id'];
            $get_detail = $this->db->query("select id from po_detail where reff1=".$v['po_detail_id'])->row_array();
            $dtr_detail[$i]['po_detail_id'] = $get_detail['id'];
            $dtr_detail[$i]['rongsok_id'] = $v['rongsok_id'];
            $dtr_detail[$i]['qty'] = $v['qty'];
            $dtr_detail[$i]['bruto'] = $v['bruto'];
            $dtr_detail[$i]['berat_palette'] = $v['berat_palette'];
            $dtr_detail[$i]['netto'] = $v['netto'];
            $dtr_detail[$i]['line_remarks'] = $v['line_remarks'];
            $dtr_detail[$i]['no_pallete'] = $v['no_pallete'];
            $dtr_detail[$i]['tanggal_masuk'] = $v['tanggal_masuk'];
        }
        $this->db->insert_batch('dtr_detail', $dtr_detail);

        foreach ($data['detail'] as $key => $value) {
            $ttr_detail[$key]['reff1'] = $value['id'];
            $ttr_detail[$key]['ttr_id'] = $ttr_id;
            $get_ttr = $this->db->query("select id from dtr_detail where reff1=".$value['dtr_detail_id'])->row_array();
            $ttr_detail[$key]['dtr_detail_id'] = $get_ttr['id'];
            $ttr_detail[$key]['rongsok_id'] = $value['rongsok_id'];
            $ttr_detail[$key]['qty'] = $value['qty'];
            $ttr_detail[$key]['bruto'] = $value['bruto'];
            $ttr_detail[$key]['netto'] = $value['netto'];
            $ttr_detail[$key]['line_remarks'] = $value['line_remarks'];
        }

        // print_r($ttr_detail);
        // die();
        $this->db->insert_batch('ttr_detail', $ttr_detail);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function voucher_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $po = $this->db->query("select id, status from po where reff1 =".$data['voucher']['po_id'])->row_array();

        $this->db->insert('f_kas', $data['f_kas']);
        $id_fk = $this->db->insert_id();

        $data['voucher']['po_id'] = $po['id'];
        $data['voucher']['id_fk'] = $id_fk;

            $this->db->insert('voucher', $data['voucher']);
            $id = $this->db->insert_id();

        $this->db->where('id', $po['id']);
        $this->db->update('po', $data['update_po']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function close_po_post(){
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        $po = $this->db->query("select id, status from po where reff1 =".$this->post('po_id'))->row_array();

        $data = array(
                'status'=> $this->post('status'),
                'remarks'=>$this->post('remarks')
            );
        
        $this->db->where('id', $po['id']);
        $this->db->update('po', $data);
        
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}