<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class R_RongsokAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_beli_rongsok');
        $this->load->model('Model_jenis_barang');
	}

	public function dtr_post(){
    	$data = [
            'reff1'=> $this->post('reff1'),
            'no_dtr'=> $this->post('no_dtr'),
            'tanggal'=> $this->post('tanggal'),
            'supplier_id'=> $this->post('supplier_id'),
            'jenis_barang'=> 'Rongsok',
            'remarks'=> $this->post('remarks')
        ];

    		$this->db->insert('dtr', $data);
        	$id = $this->db->insert_id();

    	if($id > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah',
    			'id'=> $id
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function dtr_delete_post(){
        $id = $this->post('id');

        $this->db->where('reff1',$id);
        $this->db->delete('dtr');

        $this->db->where('dtr_id');
        $this->db->delete('dtr_detail');

            if($this->db->affected_rows() > 0){
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'Berhasil di delete'
                ],REST_Controller::HTTP_OK);
            }else{  
                $this->response([
                    'status' => false,
                    'message' => 'ID tidak ditemukan'
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
    }
}