<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class FinanceAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
	}

    public function inv_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $sj = $this->db->query("select id, sales_order_id from t_surat_jalan where reff1 =".$data['master']['id_surat_jalan'])->row_array();

        unset($data['master']['created_at']);
        unset($data['master']['created_by']);
        unset($data['master']['flag_ppn']);
        $data['master']['id_sales_order'] = $sj['sales_order_id'];
        $data['master']['id_surat_jalan'] = $sj['id'];

        //insert f_invoice
        $this->db->insert('f_invoice', $data['master']);
        $f_id = $this->db->insert_id();

        //update t_surat_jalan
        $this->db->where('id', $sj['id']);
        $this->db->update('t_surat_jalan', array(
            'inv_id'=> $f_id
        ));

        //update sales_order
        $this->db->where('id', $sj['sales_order_id']);
        $this->db->update('sales_order', array(
            'flag_invoice' => $data['flag_invoice']
        ));

        //insert f_invoice_detail
        foreach ($data['detail'] as $i => $item) {
            $data['detail'][$i]['id_invoice'] = $f_id;
            $data['detail'][$i]['sj_detail_id'] = $sj['id'];
            unset($data['detail'][$i]['id']);
        }

        $this->db->insert_batch('f_invoice_detail', $data['detail']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function inv_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $invoice = $this->db->query("select id from f_invoice where reff1 =".$data['id'])->row_array();
        $this->db->where('id', $invoice['id']);
        $this->db->update('f_invoice', $data['master']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function um_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        // $sj = $this->db->query("select id, sales_order_id from t_surat_jalan where reff1 =".$data['master']['id_surat_jalan'])->row_array();

        unset($data['fum']['flag_ppn']);
        $this->db->insert('f_uang_masuk', $data['fum']);
        $fum_id = $this->db->insert_id();

        unset($data['f_kas']['flag_ppn']);
        $data['f_kas']['id_um'] = $fum_id;
        $this->db->insert('f_kas', $data['f_kas']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function um_update_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $id = $this->db->query("select id from f_uang_masuk where reff1 =".$data['id'])->row_array();

        $this->db->where('id_um', $id['id']);
        $this->db->update('f_kas', $data['f_kas']);

        $this->db->where('id', $id['id']);
        $this->db->update('f_uang_masuk', $data['fum']);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function inv_del_get(){
        $id = $this->get('id');

        if($id == 0){
            $this->response([
                'status' => false,
                'message' => 'ID belum dikirim'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $get = $this->db->query('select id, id_sales_order, id_surat_jalan from f_invoice where reff1='.$id)->row_array();
            
            $this->load->model('Model_finance');

            $this->db->update('t_surat_jalan', ['inv_id'=>NULL], ['id'=>$get['id_surat_jalan']]);

            $this->db->update('sales_order',['flag_invoice'=>2] ,['id'=>$get['id_sales_order']]);

            $this->db->delete('f_invoice', ['id' => $get['id']]);
            $this->db->delete('f_invoice_detail', ['id_invoice' => $get['id']]);
            if($this->db->affected_rows() > 0){
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'Berhasil di delete'
                ],REST_Controller::HTTP_OK);
            }else{  
                $this->response([
                    'status' => false,
                    'message' => 'ID tidak ditemukan'
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function um_del_get(){
        $id = $this->get('id');

        if($id == 0){
            $this->response([
                'status' => false,
                'message' => 'ID belum dikirim'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $get = $this->db->query('select id from f_uang_masuk where reff1='.$id)->row_array();
            $this->db->delete('f_uang_masuk', ['id' => $get['id'] ]);
            $this->db->delete('f_kas', ['id_um' => $get['id'] ]);

            if($this->db->affected_rows() > 0){
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'Berhasil di delete'
                ],REST_Controller::HTTP_OK);
            }else{  
                $this->response([
                    'status' => false,
                    'message' => 'ID tidak ditemukan'
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function sj_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $so = $this->db->query("select id from sales_order where reff1 =".$data['tsj']['sales_order_id'])->row_array();

        //update flag sj

        $this->db->where('id', $so['id']);
        $this->db->update('sales_order', array(
            'flag_sj' => $data['flag_sj'],
            'flag_invoice' => 0
        ));
        //setting data SJ
        $data_tsj = array(
            'reff1' => $data['tsj']['id'],
            'no_surat_jalan' => $data['tsj']['no_surat_jalan'],
            'sales_order_id' => $so['id'],
            'po_id' => $data['tsj']['po_id'],
            'spb_id' => $data['tsj']['spb_id'],
            'retur_id' => $data['tsj']['retur_id'],
            'inv_id' => $data['tsj']['inv_id'],
            'status' => $data['tsj']['status'],
            'tanggal' => $data['tsj']['tanggal'],
            'jenis_barang' => $data['tsj']['jenis_barang'],
            'm_customer_id' => $data['tsj']['m_customer_id'],
            'no_kendaraan' => $data['tsj']['no_kendaraan'],
            'supir' => $data['tsj']['supir'],
            'remarks' => $data['tsj']['remarks']
        );
        $this->db->insert('t_surat_jalan', $data_tsj);
        $sj_id = $this->db->insert_id();

        //setting insert Gudang FG
        foreach ($data['gudang'] as $k => $v) {
            //cek mau insert produksi_fg
            if($data['gudang'][$k]['id_prd']){
                $cek_pf = $this->db->query("select id from produksi_fg where reff1 =".$data['gudang'][$k]['id_prd'])->row_array();
                if(empty($cek_pf)){
                    $this->db->insert('produksi_fg', array(
                        'reff1' => $data['gudang'][$k]['id_prd'],
                        'no_laporan_produksi' => $data['gudang'][$k]['no_laporan_produksi'],
                        'tanggal' => $data['gudang'][$k]['tgl_prd'],
                        'flag_result' => $data['gudang'][$k]['flag_result'],
                        'remarks' => $data['gudang'][$k]['remarks_prd'],
                        'jenis_barang_id' => $data['gudang'][$k]['jb_prd'],
                        'jenis_packing_id' => $data['gudang'][$k]['jenis_packing_id']
                    ));
                    $pf_id = $this->db->insert_id();
                }else{
                    $pf_id = $cek_pf['id'];
                }
            }else{
                $pf_id = 0;
            }

            //insert produksi_fg_detail
            if($pf_id > 0){
                $this->db->insert('produksi_fg_detail', array(
                    'produksi_fg_id' => $pf_id,
                    'tanggal' => $data['gudang'][$k]['tgl_prd'],
                    'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
                    'no_produksi' => $data['gudang'][$k]['no_produksi'],
                    'bruto' => $data['gudang'][$k]['bruto'],
                    'netto' => $data['gudang'][$k]['netto'],
                    'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                    'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                    'keterangan' => $data['gudang'][$k]['remarks_prd']
                ));
            }

            //cek mau insert t_bpb_fg
            $cek_bpb = $this->db->query("select id from t_bpb_fg where reff1 =".$data['gudang'][$k]['t_bpb_fg_id'])->row_array();
            if(empty($cek_bpb)){
                $this->db->insert('t_bpb_fg', array(
                    'reff1' => $data['gudang'][$k]['t_bpb_fg_id'],
                    'no_bpb_fg' => $data['gudang'][$k]['no_bpb_fg'],
                    'tanggal' => $data['gudang'][$k]['tgl_bpb'],
                    'produksi_fg_id' => $pf_id,
                    'jenis_barang_id' => $data['gudang'][$k]['jb_bpb'],
                    'status' => 1,
                    'keterangan' => $data['gudang'][$k]['ket_bpb']
                ));
                $bpb_id = $this->db->insert_id();
            }else{
                $bpb_id = $cek_bpb['id'];
            }

            //insert t_bpb_fg_detail
            if($bpb_id > 0){
                $cek_bpb_detail = $this->db->query("select id from t_bpb_fg_detail where reff1 =".$data['gudang'][$k]['id_bpb_d'])->row_array();
                if(empty($cek_bpb_detail)){
                    $this->db->insert('t_bpb_fg_detail', array(
                        'reff1' => $data['gudang'][$k]['id_bpb_d'],
                        't_bpb_fg_id' => $bpb_id,
                        'jenis_barang_id' => $data['gudang'][$k]['jenis_barang_id'],
                        'no_packing_barcode' => $data['gudang'][$k]['no_packing'],
                        'no_produksi' => $data['gudang'][$k]['no_produksi'],
                        'bruto' => $data['gudang'][$k]['bruto'],
                        'netto' => $data['gudang'][$k]['netto'],
                        'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                        'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                        'flag_taken' => 0
                    ));
                    $bpb_detail_id = $this->db->insert_id();
                }else{
                    $bpb_detail_id = $cek_bpb_detail['id'];
                }
            }

            //insert t_gudang_fg
                $cek_gudang_fg = $this->db->query("select id from t_gudang_fg where reff1 =".$data['gudang'][$k]['id'])->row_array();
                if(empty($cek_gudang_fg)){
                    $this->db->insert('t_gudang_fg', array(
                            'reff1' => $data['gudang'][$k]['id'],
                            'tanggal'=> $data['gudang'][$k]['tanggal'],
                            'jenis_trx' => 0, //0 masuk
                            'flag_taken'=>0, // 0 belum diambil
                            't_bpb_fg_id' => $bpb_id,
                            't_bpb_fg_detail_id' => $bpb_detail_id,
                            'jenis_barang_id' => $data['gudang'][$k]['jenis_barang_id'],
                            'nomor_BPB' => $data['gudang'][$k]['nomor_BPB'],
                            'no_produksi' => $data['gudang'][$k]['no_produksi'],
                            'no_packing' => $data['gudang'][$k]['no_packing'],
                            'netto' => $data['gudang'][$k]['netto'],
                            'bruto' => $data['gudang'][$k]['bruto'],
                            'berat_bobbin' => $data['gudang'][$k]['berat_bobbin'],
                            'bobbin_id' => $data['gudang'][$k]['bobbin_id'],
                            'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                            'keterangan' => $data['gudang'][$k]['keterangan'],
                            'tanggal_masuk' => $data['gudang'][$k]['tanggal_masuk'],
                            'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar'],
                            'created_by'=> 0,
                            'created_at' => 0
                        ));
                    $gudang_id = $this->db->insert_id();
                }else{
                    $this->db->where('id', $cek_gudang_fg['id']);
                    $this->db->update('t_gudang_fg', array(
                        'tanggal_keluar' => $data['gudang'][$k]['tanggal_keluar']
                    ));
                    $gudang_id = $cek_gudang_fg['id'];
                }

            //setting data SJ
            $this->db->insert('t_surat_jalan_detail', array(
                'reff1'=> $data['gudang'][$k]['id_sj_d'],
                't_sj_id'=> $sj_id,
                'gudang_id'=> $gudang_id,
                'jenis_barang_id'=> $data['gudang'][$k]['jenis_barang_id'],
                'jenis_barang_alias'=> $data['gudang'][$k]['jenis_barang_alias'],
                'no_packing'=> $data['gudang'][$k]['no_packing'],
                'qty'=> $data['gudang'][$k]['jb_qty'],
                'bruto'=> $data['gudang'][$k]['bruto'],
                'netto'=> $data['gudang'][$k]['netto'],
                'nomor_bobbin'=> $data['gudang'][$k]['nomor_bobbin'],
                'line_remarks'=> $data['gudang'][$k]['line_remarks'],
                'created_by'=> 0,
                'created_at'=> 0
            ));
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}