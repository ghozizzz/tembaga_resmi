<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class BeliSparepartAPI extends REST_Controller{

	public function __construct(){
		parent::__construct();
        $this->load->model('Model_beli_sparepart');
        $this->load->model('Model_jenis_barang');
	}

	public function pps_post(){
    	$data = [
    		'reff1' => $this->post('reff1'),
    		'no_pengajuan' => $this->post('no_pengajuan'),
    		'tgl_pengajuan' => $this->post('tgl_pengajuan'),
    		'jenis_kebutuhan' => $this->post('jenis_kebutuhan'),
    		'nama_pengaju' => $this->post('nama_pengaju'),
    		'remarks' => $this->post('remarks'),
    		'tgl_sparepart_dibutuhkan' => $this->post('tgl_sparepart_dibutuhkan'),
    		'status' => $this->post('status')
    	];

    		$this->db->insert('beli_sparepart', $data);
        	$id = $this->db->insert_id();

    	if($id > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah',
    			'id'=> $id
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function pps_detail_post(){
		
		$json = file_get_contents('php://input');

		// Converts it into a PHP object
		$data = json_decode($json, true);
		// print_r($data);
		// die();
		// foreach($data as $i => $item) {
		//      echo $data[$i]['id'];
		//      echo $data[$i]['beli_sparepart_id'];
		//  	echo $data[$i]['beli_sparepart_id'];
		//  	echo $data[$i]['sparepart_id'];
		//  	echo $data[$i]['qty'];
		//  	echo $data[$i]['keterangan'];
		//  	echo $data[$i]['flag_po'];
		//  	echo $data[$i]['nama_item'];
		//  	echo $data[$i]['uom'];
		// }

		// $this->db->insert_batch('beli_sparepart_detail', $data); 
  //   		$this->db->insert('beli_sparepart', $data);
		// 	$id = $this->db->insert_id();

    	if($this->db->insert_batch('beli_sparepart_detail', $data)){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah'
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function pps_get(){
    	$id = (int) $this->get('id');

        if($id == 0){
        	$this->response([
    			'status' => false,
    			'message' => 'ID belum dikirim'
    		],REST_Controller::HTTP_BAD_REQUEST);
        }else{
    		$data = $this->db->query('select id from beli_sparepart where reff1='.$id)->row_array();
        	$this->db->delete('beli_sparepart', ['id'=> $data['id']]);
        	$this->db->delete('beli_sparepart_detail', ['beli_sparepart_id' => $data['id']]);

        	if($this->db->affected_rows() > 0){
        		$this->response([
	    			'status' => true,
	    			'id' => $id,
	    			'message' => 'Berhasil di delete'
	    		],REST_Controller::HTTP_OK);
        	}else{	
	        	$this->response([
	    			'status' => false,
	    			'message' => 'ID tidak ditemukan'
	    		],REST_Controller::HTTP_BAD_REQUEST);
        	}
        }

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

    /**PO SECTION**/
    public function po_post(){

        $pps = $this->db->query("select id from beli_sparepart where reff1 =".$this->post('beli_sparepart_id'))->row_array();

    	$data = [
    		'reff1'=> $this->post('reff1'),
    		'no_po'=> $this->post('no_po'),
            'tanggal'=> $this->post('tanggal'),
            'beli_sparepart_id'=> $pps['id'],
            'ppn'=>$this->post('ppn'),
            'diskon'=>$this->post('diskon'),
            'materai'=>$this->post('materai'),
            'currency'=>$this->post('currency'),
            'kurs'=>$this->post('kurs'),
            'supplier_id'=>$this->post('supplier_id'),
            'term_of_payment'=>$this->post('term_of_payment'),
            'jenis_po'=>$this->post('jenis_po'),
            'created'=>'',
            'created_by'=>'',
            'modified'=>'',
            'modified_by'=>''
    	];

    		$this->db->insert('po', $data);
        	$id = $this->db->insert_id();

    	if($id > 0){
    		$this->response([
    			'status' => true,
    			'message' => 'Berhasil di tambah',
    			'id'=> $id
    		],REST_Controller::HTTP_CREATED);
    	}else{
    		$this->response([
    			'status' => false,
    			'message' => 'Gagal menambah data'
    		],REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function po_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        foreach ($data as $i => $item) {
            $get = $this->db->query("select id from beli_sparepart_detail where reff1 =".$data[$i]['beli_sparepart_detail_id'])->row_array();
            $data[$i]['beli_sparepart_detail_id'] = $get['id'];
        }

        if($this->db->insert_batch('po_detail', $data)){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function po_update_post(){

        $json = file_get_contents('php://input');
        $data = json_decode($json, true);

        $this->db->trans_start();

        $po = $this->db->query("select id from po where reff1 =".$data['id'])->row_array();

        unset($data['master']['modified']);
        unset($data['master']['modified_by']);
        $this->db->where('id', $po['id']);
        $this->db->update('po', $data['master']);

            foreach ($data['detail'] as $i => $row){
                    $pod = $this->db->query("select id from po_detail where reff1=".$row['po_detail_id'])->row_array();
                    $this->db->where('id', $pod['id']);
                    $this->db->update('po_detail', array(
                        'amount'=>str_replace(',', '', $row['amount']),
                        'qty'=>str_replace(',', '', $row['qty']),
                        'total_amount'=>str_replace(',', '', $row['total_amount'])
                    ));
            }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function bpb_post(){

        $get_po = $this->db->query("select id from po where reff1 =".$this->post('po_old'))->row_array();

        $data = [
            'reff1'=> $this->post('reff1'),
            'no_bpb'=> $this->post('no_bpb'),
            'tanggal'=> $this->post('tanggal'),
            'po_id'=> $get_po['id'],
            'remarks'=> $this->post('remarks'),
            'pengirim'=> $this->post('pengirim'),
            'created'=> '',
            'created_by'=> '',
            'modified'=> '',
            'modified_by'=> ''
        ];

            $this->db->insert('lpb', $data);
            $id = $this->db->insert_id();

        if($id > 0){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah',
                'id'=> $id,
                'po_id'=> $get_po['id']
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function bpb_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();
        foreach ($data['lpb_detail'] as $i => $item) {
            $get = $this->db->query("select id from po_detail where reff1 =".$item['po_detail_id'])->row_array();
            $item['po_detail_id'] = $get['id'];
            // echo $item['reff1'] = $item['id'];

            //create array for insert batch
            $batch[$i]['reff1'] = $item['lpb_detail_id'];
            $batch[$i]['lpb_id'] = $data['lpb_id'];
            $batch[$i]['po_detail_id'] = $get['id'];
            $batch[$i]['sparepart_id'] = $item['sparepart_id'];
            $batch[$i]['qty'] = $item['qty'];
            $batch[$i]['line_remarks'] = $item['line_remarks'];
            //

            $this->db->insert('lpb_detail', array(
                'reff1'=>$item['lpb_detail_id'],
                'lpb_id'=>$data['lpb_id'],
                'po_detail_id'=>$get['id'],
                'sparepart_id'=>$item['sparepart_id'],
                'qty'=>$item['qty'],
                'line_remarks'=>$item['line_remarks']
            ));
            $lpb_detail_id = $this->db->insert_id();

                    #Save data ke gudang sp
                    $this->db->insert('t_gudang_sp', array(
                        'reff1'=>$item['gudang_id'],
                        'tanggal'=>$data['tgl_input'],
                        'flag_taken'=>0,
                        'sparepart_id'=>$item['sparepart_id'],
                        't_spb_id'=>0,
                        't_spb_keluar_id'=>0,
                        'lpb_detail_id'=>$lpb_detail_id,
                        'qty'=>$item['qty'],
                        'amount'=>$item['amount'],
                        'remarks'=>'Pembelian'
                    ));

                $this->db->where('id', $item['po_detail_id']);
                $this->db->update('po_detail', array('flag_lpb'=>$data['flag_lpb']));
        }

            $this->load->model('Model_beli_sparepart');
            $get_status = $this->Model_beli_sparepart->po_list_cek($data['po_id'])->row_array();

            if($get_status['ready_to_lpb'] == 0){           
                        $this->db->where('id', $get_status['id']);
                        $this->db->update('po', array(
                                'status'=>3,
                                'flag_pelunasan'=>0
                            ));
            }else{
                        #Update status PO
                        $this->db->where('id', $get_status['id']);
                        $this->db->update('po', array(
                            'status'=>2,
                            'flag_pelunasan'=>0
                            ));
            }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function bpb_del_post(){

        $this->db->trans_start();

        $get_lpb = $this->db->query('select id, po_id from lpb where reff1 ='.$this->post('id'))->row_array();
        $get = $this->db->query("select * from lpb_detail where lpb_id =".$get_lpb['id'])->result();

        foreach ($get as $value) {
            $this->db->where('lpb_detail_id', $value->id);
            $this->db->delete('t_gudang_sp');

            $this->db->where('id', $value->id);
            $this->db->delete('lpb_detail');
        }

        $this->db->where('id', $get_lpb['po_id']);
        $this->db->update('po', array( 'status'=> 2));

        $this->db->where('id', $get_lpb['id']);
        $this->db->delete('lpb');

        // echo $this->db->trans_status();die();
        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function spb_post(){

        $this->db->trans_start();

        $data = [
            'reff1'=>$this->post('spb_id'),
            'no_spb_sparepart'=> $this->post('no_spb_sparepart'),
            'tanggal'=> $this->post('tanggal'),
            'keterangan'=>$this->post('keterangan'),
            'created_at'=> '',
            'created_by'=> ''
        ];

            $this->db->insert('t_spb_sparepart', $data);

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function spb_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $get = $this->db->query("select id from t_spb_sparepart where reff1 =".$data['id'])->row_array();

        $this->db->where('id', $get['id']);
        $this->db->update('t_spb_sparepart', array(
            'request_by'=>$data['request_by'],
            'keterangan'=>$data['keterangan']
        ));

        $this->db->where('t_spb_sparepart_id', $get['id']);
        $this->db->delete('t_spb_sparepart_detail');

        foreach ($data['data_detail'] as $i => $item){
            $data['data_detail'][$i]['reff1'] = $item['id'];
            $data['data_detail'][$i]['t_spb_sparepart_id'] = $get['id'];
            unset($data['data_detail'][$i]['id']);
        }

        if($this->db->insert_batch('t_spb_sparepart_detail', $data['data_detail'])){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function spb_keluar_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $get = $this->db->query("select id from t_spb_sparepart where reff1 =".$data['id'])->row_array();

        $this->db->where('id', $get['id']);
        $this->db->update('t_spb_sparepart', array(
            'status'=>$data['header']['status'],
            'keterangan'=>$data['header']['keterangan']
        ));

        foreach ($data['detail'] as $i => $item){

            $this->db->insert('t_spb_sparepart_detail_keluar', array(
                'reff1'=>$item['id'],
                't_spb_sparepart_id'=>$get['id'],
                'sparepart_id'=>$item['sparepart_id'],
                'qty'=>$item['qty'],
                'uom'=>$item['uom'],
                'keterangan'=>$item['keterangan']
            ));

            $id_detail = $this->db->insert_id();

            $this->db->insert('t_gudang_sp', array(
                'reff1'=> $item['id_gudang'],
                'sparepart_id'=> $item['sparepart_id'],
                'tanggal'=> $data['tgl_input'],
                't_spb_id'=> $get['id'],
                't_spb_keluar_id'=> $id_detail,
                'lpb_detail_id'=> 0,
                'qty'=> $item['qty'],
                'amount'=> 0,
                'remarks'=> $item['keterangan']
            ));
        }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function vk_post(){

        $data = [
            'reff1' => $this->post('reff1'),
            'no_vk' => $this->post('no_vk'),
            'supplier_id' => $this->post('supplier_id'),
            'tanggal' => $this->post('tanggal'),
            'keterangan' => $this->post('keterangan')
        ];

            $this->db->insert('f_vk', $data);
            $id = $this->db->insert_id();

        if($id > 0){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function vk_detail_post(){

        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json, true);

        $this->db->trans_start();

        $get = $this->db->query('select id from f_vk where reff1 ='.$data['vk_id'])->row_array();
        $data['voucher']['vk_id'] = $get['id'];

        unset($data['kas']['flag_ppn']);
        $this->db->insert('f_kas', $data['kas']);
        $id_fk = $this->db->insert_id();
        $data['voucher']['id_fk'] = $id_fk;

        foreach ($data['v_tambahan'] as $v => $i) {
            $data['v_tambahan'][$v]['reff1']= $data['v_tambahan'][$v]['id'];
            $data['v_tambahan'][$v]['vk_id']= 0;
            $data['v_tambahan'][$v]['id_fk']= $id_fk;
            unset($data['v_tambahan'][$v]['id']);
            $this->db->insert('voucher', $data['v_tambahan'][$v]);
        }
        $this->db->insert('voucher', $data['voucher']);

        foreach($data['lpb_detail'] as $row)
        {
            $new_arr[] = $row['id'];
        }
        
        $this->db->where_in('reff1', $new_arr);
        $this->db->update('lpb', ["vk_id" => $get['id']]);

        $this->load->model('Model_beli_sparepart');
        $po_group = $this->Model_beli_sparepart->get_po_group($get['id'])->result();
            foreach ($po_group as $key) {
                $check = $this->Model_beli_sparepart->check_po_lpb($key->po_id)->result();
                if(empty($check) && $key->status == 3){
                    $this->db->where('id', $key->po_id);
                    $this->db->update('po', array(
                        'flag_pelunasan'=>1,
                        'status'=>4
                    ));
                }else{
                    $this->db->where('id', $key->po_id);
                    $this->db->update('po', array(
                        'flag_dp'=>1
                    ));
                }
            }

        if($this->db->trans_complete()){
            $this->response([
                'status' => true,
                'message' => 'Berhasil di tambah'
            ],REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'Gagal menambah data'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function numbering_get(){
        $id = $this->get('id');
        $tgl_input = $this->get('tgl');

        if(empty($id)){
            $this->response([
                'status' => false,
                'message' => 'ID / Tanggal belum dikirim'
            ],REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $this->load->model('Model_m_numberings');
            $code = $this->Model_m_numberings->getNumbering($id, $tgl_input);
            
            if($this->db->affected_rows() > 0){
                $this->response([
                    'status' => true,
                    'code' => $code,
                    'message' => 'Penomoran berhasil di generate'
                ],REST_Controller::HTTP_OK);
            }else{  
                $this->response([
                    'status' => false,
                    'message' => 'ID tidak ditemukan'
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
        }

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }
}