<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rsk extends CI_Controller{
    function __construct(){
        parent::__construct();

        if($this->session->userdata('status') != "login"){
            redirect(base_url("index.php/Login"));
        }
        $this->load->model('Model_rsk');
    }

    function save_detail_rsk(){
        $return_data = array();
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $dtr_id = $this->input->post('id_dtr');
        $dtr_detail_id = $this->input->post('dtr_detail_id');

        $this->db->trans_start();
        #update netto resmi
        $validasi = $this->db->query("select * from spb_detail_fulfilment where dtr_detail_id = ".$dtr_detail_id." and spb_id = ".$this->input->post('id_spb'))->row_array();

        if(isset($validasi)){
            $get_data = $this->db->query("select dd.netto, dd.netto_resmi, td.ttr_id from dtr_detail dd
                                left join ttr_detail td on td.dtr_detail_id = dd.id
                                where dd.id = ".$dtr_detail_id)->row_array();
            $update_netto = 0;
            $update_netto = $get_data['netto_resmi'] + $this->input->post(' netto');

            if($update_netto == $get_data['netto']){
                $this->db->where('id', $dtr_detail_id);
                $this->db->update('dtr_detail', array('flag_resmi' => 1, 'netto_resmi' => $update_netto));
            } else {
                $this->db->where('id', $dtr_detail_id);
                $this->db->update('dtr_detail', array('flag_resmi' => 0, 'netto_resmi' => $update_netto));
            }

            $this->db->query("update spb_detail_fulfilment set netto = netto + ".$this->input->post('netto')." where dtr_detail_id = ".$dtr_detail_id." and spb_id = ".$this->input->post('id_spb'));
        }else{
            $get_data = $this->db->query("select dd.netto, dd.netto_resmi, td.ttr_id from dtr_detail dd
                                    left join ttr_detail td on td.dtr_detail_id = dd.id
                                    where dd.id = ".$dtr_detail_id)->row_array();
            $update_netto = 0;
            $update_netto = $get_data['netto_resmi'] + $this->input->post('netto');
            $this->db->where('id', $dtr_detail_id);
            $this->db->update('dtr_detail', array('flag_resmi' => 1, 'netto_resmi' => $update_netto));

            $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));
            $this->load->model('Model_m_numberings');
            $code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);
            
            $no_pallete = $tgl_code.substr($code,13,5);

            $this->db->insert('spb_detail_fulfilment', array(
                'spb_id'=>$this->input->post('id_spb'),
                'dtr_detail_id'=>$dtr_detail_id,
                'netto'=>$this->input->post('netto'),
                'ttr_id'=>$get_data['ttr_id']
            ));
        }

        if($this->db->trans_complete()){
            $return_data['message_type']= "sukses";
            $return_data['id_dtr'] = $this->input->post('id_dtr');
            $return_data['jenis_barang'] = $this->input->post('id_barang');
            $return_data['dtr_detail_id'] = $this->input->post('dtr_detail_id');
        }else{
            $return_data['message_type']= "error";
            $return_data['message']= "Gagal menambahkan item barang! Silahkan coba kembali";
        }
        header('Content-Type: application/json');
        echo json_encode($return_data); 
    }

    function save_dtr_detail_parsial(){
        $return_data = array();
        $tgl_input = date("Y-m-d", strtotime($this->input->post('tanggal')));
        $dtr_id = $this->input->post('id_dtr');
        $dtr_detail_id = $this->input->post('dtr_detail_id');
        $spb_id = $this->input->post('spb_id');

        $this->db->trans_start();

        $validasi = $this->db->query("select * from spb_detail_fulfilment where dtr_detail_id = ".$dtr_detail_id." and spb_id = ".$spb_id)->row_array();

        $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));
        $this->load->model('Model_m_numberings');
        $code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);
        
        $no_pallete = $tgl_code.substr($code,13,5);

        if (isset($validasi)) {
            #update netto resmi
            $get_data = $this->db->query("select dd.netto, dd.netto_resmi, td.ttr_id from dtr_detail dd
                                left join ttr_detail td on td.dtr_detail_id = dd.id
                                where dd.id = ".$dtr_detail_id)->row_array();
            $update_netto = 0;
            $update_netto = $get_data['netto_resmi'] + $this->input->post('u_netto');

            if($update_netto == $get_data['netto']){
                $this->db->where('id', $dtr_detail_id);
                $this->db->update('dtr_detail', array('flag_resmi' => 1, 'netto_resmi' => $update_netto));
            } else {
                $this->db->where('id', $dtr_detail_id);
                $this->db->update('dtr_detail', array('flag_resmi' => 0, 'netto_resmi' => $update_netto));
            }

            $this->db->query("update spb_detail_fulfilment set netto = netto + ".$this->input->post('u_netto')." where dtr_detail_id = ".$dtr_detail_id." and spb_id = ".$spb_id);

            if($this->db->trans_complete()){
                $return_data['message_type']= "sukses";
                $return_data['id_dtr'] = $this->input->post('id_dtr');
                $return_data['jenis_barang'] = $this->input->post('id_barang');
                $return_data['dtr_detail_id'] = $this->input->post('dtr_detail_id');
            }else{
                $return_data['message_type']= "error";
                $return_data['message']= "Gagal menambahkan item barang! Silahkan coba kembali";
            }
            header('Content-Type: application/json');
            echo json_encode($return_data); 
        } else {
            #update netto resmi
            $get_data = $this->db->query("select dd.netto, dd.netto_resmi, td.ttr_id from dtr_detail dd
                                left join ttr_detail td on td.dtr_detail_id = dd.id
                                where dd.id = ".$dtr_detail_id)->row_array();
            $update_netto = 0;
            $update_netto = $get_data['netto_resmi'] + $this->input->post('u_netto');

            if($update_netto == $get_data['netto']){
                $this->db->where('id', $dtr_detail_id);
                $this->db->update('dtr_detail', array('flag_resmi' => 1, 'netto_resmi' => $update_netto));
            } else {
                $this->db->where('id', $dtr_detail_id);
                $this->db->update('dtr_detail', array('flag_resmi' => 0, 'netto_resmi' => $update_netto));
            }

                    $this->db->insert('spb_detail_fulfilment', array(
                        'spb_id'=>$spb_id,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'netto'=>$this->input->post('u_netto'),
                        'ttr_id'=>$get_data['ttr_id']
                    ));

            if($this->db->trans_complete()){
                $return_data['message_type']= "sukses";
                $return_data['id_dtr'] = $this->input->post('id_dtr');
                $return_data['jenis_barang'] = $this->input->post('id_barang');
                $return_data['dtr_detail_id'] = $this->input->post('dtr_detail_id');
            }else{
                $return_data['message_type']= "error";
                $return_data['message']= "Gagal menambahkan item barang! Silahkan coba kembali";
            }
            header('Content-Type: application/json');
            echo json_encode($return_data); 
        }
        
    }

    function delete_dtr_detail(){
        $id = $this->input->post('id_dtr_detail');
        $id_dtr = $this->input->post('id_dtr');
        $detail_id_matching = $this->input->post('detail_id_matching');
        $check = 0;
        $reset_netto = 0;
        $netto = $this->input->post('netto');

        $this->db->trans_start();

        $data = $this->db->query("select * from dtr_detail where id = ".$id)->row_array();
        $reset_netto = (int)$data['netto_resmi'] - (int)$netto;
        
        $this->db->where('id', $id);
        $this->db->update('dtr_detail', array('flag_resmi' => 0, 'netto_resmi' => $reset_netto));

        $return_data = array();
        $this->db->where('id', $detail_id_matching);
        $this->db->delete('spb_detail_fulfilment');
        if($this->db->trans_complete()){
            $return_data['message_type']= "sukses";
            $return_data['dtr_id'] = $id_dtr;
            $return_data['jenis_barang'] = $this->input->post('id_barang');
            $return_data['check'] = $check;
        }else{
            $return_data['message_type']= "error";
            $return_data['message']= "Gagal menghapus item rongsok! Silahkan coba kembali";
        }           
        header('Content-Type: application/json');
        echo json_encode($return_data);
    }

    function load_detail_dtr(){
        $id = $this->input->post('id');
        $kurangnya = 0;
        $tabel = "";
        $no    = 1;
        $total = 0;

        $myDetail = $this->Model_rsk->show_detail_dtr($id)->result();
        foreach ($myDetail as $row){
            $tabel .= '<tr>';
            $tabel .= '<td style="text-align:center">'.$no.'</td>';
            $tabel .= '<input type="hidden" id="detail_id_matching_'.$row->id.'" name="detail_id_matching" value="'.$row->id.'"/>';
            $tabel .= '<input type="hidden" id="dtr_id_'.$row->dtr_detail_id.'" name="dtr_id" value="'.$row->dtr_detail_id.'"/>';
            $tabel .= '<td>'.$row->nama_item.'</td>';
            $tabel .= '<td style="text-align:right;">'.$row->bruto.'</td>';
            $tabel .= '<td style="text-align:right;"><label id="l_netto_'.$row->dtr_detail_id.'">'.$row->netto.'</label><input style="display:none;" type="number" min="1" max="'.$row->netto.'" id="u_netto_'.$row->dtr_detail_id.'" name="u_update['.$no.'][netto]" value="'.$row->netto.'" class="form-control myline" /></td>';
            $tabel .= '<td style="text-align:right;">'.$row->berat_palette.'</td>';
            $tabel .= '<td>'.$row->no_pallete.'</td>';
            $tabel .= '<td style="text-align:center"><a href="javascript:;" class="btn btn-xs btn-circle '
                    . 'red" onclick="hapusDetail('.$row->dtr_detail_id.','.$row->rongsok_id.','.$row->netto.','.$row->id.');" style="margin-top:5px"> '
                    . '<i class="fa fa-trash"></i> Delete </a></td>';
            $tabel .= '</tr>';
            $total += $row->netto;
            $no++;
        }

        $tabel .= '<tr>';
        $tabel .= '<td colspan="3" style="text-align:right"><strong>Total (Kg) </strong></td>';
        $tabel .= '<td style="text-align:right; background-color:green; color:white"><strong>'.$total.'</strong></td>';
        $tabel .= '<input type="hidden" id="total_spb" name="total_spb" value="'.$total.'">';
        $tabel .= '<td colspan="4"></td>';
        $tabel .= '</tr>';

        header('Content-Type: application/json');
        echo json_encode($tabel);
    }

    function load_list_dtr(){
        $tabel = "";
        $no = 1;

        $list_dtr = $this->Model_rsk->list_dtr()->result();
        foreach ($list_dtr as $row) {
            $tabel .= '<tr>';
            $tabel .= '<td style="text-align: center;">'.$no.'</td>';
            $tabel .= '<td>'.$row->no_dtr.'</td>';
            $tabel .= '<td>'.$row->netto.'</td>';
            $tabel .= '</tr>';
            $no++;
        }

        header('Content-Type: application/json');
        echo json_encode($tabel);
    }

    function save_fulfilment(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date("Y-m-d", strtotime($this->input->post('tanggal')));
        $spb_id = $this->input->post('id');
        
        $this->db->trans_start();

        if($this->input->post('total_spb') >= $this->input->post('jmlh')){
            $status = 1;
        }else{
            $status = 4;
        }

        // echo $this->input->post('jmlh');die();

        #Update status SPB
        $this->db->where('id', $spb_id);
        $this->db->update('spb', array(
                        'no_spb'=> $this->input->post('no_spb'),
                        'jumlah'=> $this->input->post('jmlh'),
                        'tanggal'=> $tgl_input,
                        'status'=> $status,
                        'modified'=> $tanggal,
                        'modified_by'=>$user_id
        ));

        $this->db->query("update dtr_detail set tanggal_keluar = '".$tgl_input."' where id in (select dtr_detail_id from spb_detail_fulfilment where spb_id =".$spb_id.")");

        if($this->db->trans_complete()){    
            $this->session->set_flashdata('flash_msg', 'SPB sudah di-save. Detail Pemenuhan SPB sudah disimpan');                 
        }else{
            $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
        }                 

       redirect('index.php/GudangRongsok/view_spb/'.$spb_id);
    }
}