<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class R_BPB extends CI_Controller{
    function __construct(){
        parent::__construct();

        if($this->session->userdata('status') != "login"){
            redirect(base_url("index.php/Login"));
        }
        $this->load->model('Model_bpb');
    }
    
    function index(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');       
        $reff_cv = $this->session->userdata('cv_id'); 
        if($group_id != 9){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        $data['content']= "resmi/bpb/index";
        $data['list_bpb']= $this->Model_bpb->list_bpb()->result();
        // }
        $this->load->view('layout', $data);
    }

    function bpb_list(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');     
        $user_ppn    = $this->session->userdata('user_ppn');   
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        $data['content']= "resmi/bpb/bpb_list";
        $this->load->model('Model_gudang_fg');
        $data['list_data']= $this->Model_bpb->list_bpb()->result();

        $this->load->view('layout', $data);
    }

    function edit(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "resmi/bpb/edit";
            $this->load->model('Model_gudang_fg');
            $data['header']  = $this->Model_gudang_fg->show_header_bpb($id)->row_array(); 
            $data['details'] = $this->Model_gudang_fg->show_detail_bpb($id)->result();
            $data['packing'] = $this->Model_gudang_fg->show_data_packing($data['header']['jenis_packing_id'])->row_array()['packing'];
            $data['list_bpb'] = $this->Model_bpb->list_bpb()->result();
            $data['list_jb'] = $this->Model_gudang_fg->barang_fg_list()->result();
            
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/bpb_list');
        }
    }

    function update_bpb(){
        $user_id  = $this->session->userdata('user_id');
        $user_ppn = $this->session->userdata('user_ppn');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        
        $this->db->trans_start();
        $data = array(
                'no_bpb_fg'=>$this->input->post('no_bpb'),
                'jenis_barang_id'=>$this->input->post('jb'),
                'tanggal'=> $tgl_input,
                'keterangan'=>$this->input->post('remarks')
            );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('t_bpb_fg', $data);

        $this->db->where('t_bpb_fg_id', $this->input->post('id'));
        $this->db->update('t_gudang_fg', array(
            'nomor_BPB'=>$this->input->post('no_bpb'),
            'tanggal'=>$tgl_input,
            'tanggal_masuk'=>$tgl_input
        ));

        if($this->db->trans_complete()){
            $this->session->set_flashdata('flash_msg', 'Data BPB berhasil disimpan');
            redirect('index.php/R_BPB/edit/'.$this->input->post('id'));
        }else{
            $this->session->set_flashdata('flash_msg', 'Data BPB gagal disimpan');
            redirect('index.php/R_BPB/edit/'.$this->input->post('id'));
        }
    }

    function add_bpb(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        $ppn = $this->session->userdata('user_ppn');
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['content']= "resmi/bpb/add_bpb";
        
        $this->load->model('Model_sales_order');
        $data['customer_list'] = $this->Model_sales_order->customer_list()->result();
        $data['jenis_barang'] = $this->Model_sales_order->jenis_barang_fg()->result();

        $this->load->view('layout', $data);
    }

    function get_no_bpb(){
        $tgl_code = date('Ym', strtotime($this->input->post('tanggal')));

        $code_bpb = 'BPB-KMP.'.$tgl_code.'.'.$this->input->post('no_bpb');
        // print_r($code_bpb);
        // die();
        $count = $this->db->query("select count(id) as count from t_bpb_fg where no_bpb_fg ='".$code_bpb."'")->row_array();
        // print_r($count);die();
        if($count['count']>0){
            $data['type'] = 'duplicate';
        }else{
            $data['type'] = 'sukses';
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function save_bpb(){
        $user_id   = $this->session->userdata('user_id');
        $tanggal   = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $tgl_code = date('Ym', strtotime($this->input->post('tanggal')));
        
        $this->db->trans_start();
        $code_bpb = 'BPB-KMP.'.$tgl_code.'.'.$this->input->post('no_bpb');
        $data = array(
            'no_bpb_fg'=> $code_bpb,
            'status'=> 1,
            'tanggal'=> $tgl_input,
            'jenis_barang_id'=> $this->input->post('jb'),
            'keterangan'=> $this->input->post('remarks')
        );
        $this->db->insert('t_bpb_fg', $data);

        if($this->db->trans_complete()){
            redirect('index.php/R_BPB/bpb_list');  
        }else{
            $this->session->set_flashdata('flash_msg', 'BPB gagal disimpan, silahkan dicoba kembali!');
            redirect('index.php/R_BPB/bpb_list');  
        }            
    }    

    function delete_bpb(){
        $this->db->trans_start();

        $this->db->where('id', $this->uri->segment(3));
        $this->db->delete('t_bpb_fg');

        if($this->db->trans_complete()){
            redirect('index.php/R_BPB/bpb_list');  
        }else{
            $this->session->set_flashdata('flash_msg', 'BPB gagal disimpan, silahkan dicoba kembali!');
            redirect('index.php/R_BPB/bpb_list');  
        }
    }

    function simpan_pindah_data(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');

        $this->db->trans_start();

        // echo $this->input->post('new_bpb_id');die();
        $get = $this->Model_bpb->get_bpb($this->input->post('new_bpb_id'))->row_array();

            $details = $this->input->post('myDetails');
            // echo '<pre>';print_r($details);echo '</pre>';
            // die();

            foreach ($details as $row){
                if(isset($row['check']) && $row['check']==1){
                    $this->db->where('id', $row['id_detail']);
                    $this->db->update('t_bpb_fg_detail', array(
                        't_bpb_fg_id'=>$get['id']
                    ));

                    $this->db->where('t_bpb_fg_detail_id', $row['id_detail']);
                    $this->db->update('t_gudang_fg', array(
                        'nomor_BPB'=>$get['no_bpb_fg'],
                        't_bpb_fg_id'=>$get['id'],
                        'tanggal'=>$get['tanggal'],
                        'tanggal_masuk'=>$get['tanggal']
                    ));
                }
            }

            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'BPP Berhasil di Ubah');                 
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat create PO, silahkan coba kembali!');
            }
            redirect('index.php/R_BPB/edit/'.$get['id']);
    }
}