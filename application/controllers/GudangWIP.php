<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GudangWIP extends CI_Controller{   
    function __construct(){
        parent::__construct();

        if($this->session->userdata('status') != "login"){
            redirect(base_url("index.php/Login"));
        }
    }
    
    function index(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang WIP";
        $data['content']   = "gudangwip/index";
        
       $this->load->model('Model_gudang_wip');
       $data['gudang_wip'] = $this->Model_gudang_wip->gudang_wip_list()->result();
 		
        $this->load->view('layout', $data);  
    }

    function produksi_wip(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');
        $jenis = $this->uri->segment(3);
        $jenis = str_replace('%20', ' ', $jenis);

        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang WIP";
        $data['content']   = "gudangwip/hasil_produksi";
        
       $this->load->model('Model_gudang_wip');
       $data['gudang_wip'] = $this->Model_gudang_wip->gudang_wip_produksi_list($jenis)->result();
        
        $this->load->view('layout', $data);  
    }

    function proses_wip(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id'); 
        $jenis = $this->uri->segment(3);
        $jenis = str_replace('%20', ' ', $jenis);

        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Proses Barang WIP";
        $data['content']   = "gudangwip/proses_wip";
        
       $this->load->model('Model_gudang_wip');
       // $pilihan_jenis_masak = array(
       //                  'ROLLING' => 'ROLLING',
       //                  'BAKAR ULANG' => 'BAKAR ULANG',
       //                  'CUCI' => 'CUCI'
       //                  );
       // $data['pil_masak'] = $pilihan_jenis_masak;
       $data['pil_masak'] = [
            $jenis => $jenis
       ];
       $data['spb_ingot'] = $this->Model_gudang_wip->spb_ingot()->result();
       $data['stok_keras'] = $this->Model_gudang_wip->stok_keras()->row_array();
       $data['spb_kawat_hitam'] = $this->Model_gudang_wip->spb_kawat_hitam()->result();
       $data['jenis_barang'] = $this->Model_gudang_wip->jenis_barang_list()->result();
        
       $this->load->view('layout', $data);  
    }

    function get_spb(){
        $id = $this->input->post('id');
        $jb = $this->input->post('jb');
        $this->load->model('Model_gudang_wip');
        $barang= $this->Model_gudang_wip->get_spb($id)->row_array();
        
        header('Content-Type: application/json');
        echo json_encode($barang); 
    }

    function save_proses_wip(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $tgl_code = date('Ym', strtotime($this->input->post('tanggal')));

        $this->db->trans_start();

        $this->load->model('Model_m_numberings');
        if($this->input->post('jenis_masak')=='ROLLING'){
            $code = 'PRD-ROL.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_bpb = 'BPB-ROL.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_dtr = 'DTR-ROL.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_ttr = 'BPBR-ROL.'.$tgl_code.'.'.$this->input->post('no_produksi');
        }elseif($this->input->post('jenis_masak')=='BAKAR ULANG'){
            $code = 'PRD-BU.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_bpb = 'BPB-BU.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_dtr = 'DTR-BU.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_ttr = 'BPBR-BU.'.$tgl_code.'.'.$this->input->post('no_produksi');
        }elseif($this->input->post('jenis_masak')=='CUCI'){
            $code = 'PRD-CC.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_bpb = 'BPB-CC.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_dtr = 'DTR-CC.'.$tgl_code.'.'.$this->input->post('no_produksi');
            $code_ttr = 'BPBR-CC.'.$tgl_code.'.'.$this->input->post('no_produksi');
        }

        if($code){
            if($this->input->post('jenis_masak') == 'CUCI'){
                if($this->input->post('id_spb')){
                    $this->db->where('id', $this->input->post('id_spb'));
                    $this->db->update('t_spb_wip', array(
                        'flag_produksi'=>1
                        ));
                }
                $susut = (int)$this->input->post('susut_berat_keras');
            }else{
                if($this->input->post('id_spb')){
                    $this->db->where('id', $this->input->post('id_spb'));
                    $this->db->update('t_spb_wip', array(
                        'used'=>1
                        ));
                }
                $susut = (int)$this->input->post('susut_berat_keras');
            }
            #insert hasil WIP
            $data = array(
                    'no_produksi_wip' => $code,
                    'jenis_masak' => $this->input->post('jenis_masak'),
                    'tanggal'=> $tgl_input,
                    'jenis_barang_id'=> $this->input->post('jenis_barang'),
                    't_spb_wip_id'=> $this->input->post('id_spb'),
                    'qty'=>(int)($this->input->post('qty_kh')!= null) ? $this->input->post('qty_kh'): $this->input->post('qty_km'),
                    'uom' => 'ROLL',
                    'gas'=> (int)$this->input->post('gas'),
                    'berat' => (int)($this->input->post('berat_kh')!=null) ? $this->input->post('berat_kh') : $this->input->post('berat_km'),
                    'susut' => $susut,
                    'keras' => (int)$this->input->post('berat_keras'),
                    'qty_keras' => (int)$this->input->post('jml_keras'),
                    'bs' => (int)($this->input->post('bs')!= null)? $this->input->post('bs') : $this->input->post('bs_rolling'),
                    'bs_ingot' => (int)($this->input->post('bs_ingot')!= null)? $this->input->post('bs_ingot') : 0,
                    'serbuk' => (int)$this->input->post('serbuk'),
                    'tali_rolling' => (int)$this->input->post('tali_rolling'),
                    'created_by'=> $user_id
                );
            $this->db->insert('t_hasil_wip', $data);
            $insert_id = $this->db->insert_id();

            #Insert t_bpb_wip
            $data_t_bpb_wip = array(
                'no_bpb' => $code_bpb,
                'tanggal'=>$tgl_input,
                'status' => '0',
                'spb_wip_id' => 0,
                'keterangan' => $this->input->post('keterangan'),
                'hasil_wip_id' => $insert_id,
                'created_by' => $user_id,
                'created' => $tanggal
            );
            $this->db->insert('t_bpb_wip', $data_t_bpb_wip);

            #Insert bpb_wip_detail
            $insert_id_bpb_wip = $this->db->insert_id();
            if ($this->input->post('jenis_masak') == 'CUCI') {
                $data_bpb_wip_detail = array(
                'bpb_wip_id' => $insert_id_bpb_wip,
                'created' => $tgl_input,
                'jenis_barang_id' => $this->input->post('jenis_barang'), //Copper Rod 8 MM Cuci
                'spb_wip_detail_id' => 0,
                'qty' => $this->input->post('qty_km'),
                'uom' => 'ROLL',
                'berat' => $this->input->post('berat_km'),
                'keterangan' => $this->input->post('keterangan'),
                'created_by' => $user_id
                );
                $this->db->insert('t_bpb_wip_detail', $data_bpb_wip_detail);
            } else {
                $data_bpb_wip_detail = array(
                'bpb_wip_id' => $insert_id_bpb_wip,
                'created' => $tgl_input,
                'jenis_barang_id' => $this->input->post('jenis_barang'), //Copper Rod 8 MM
                'spb_wip_detail_id' => 0,
                'qty' => $this->input->post('qty_kh'),
                'uom' => 'ROLL',
                'berat' => $this->input->post('berat_kh'),
                'keterangan' => $this->input->post('keterangan'),
                'created_by' => $user_id
                );
                $this->db->insert('t_bpb_wip_detail', $data_bpb_wip_detail);
                if($this->input->post('jenis_masak') == 'BAKAR ULANG'){
                    $this->db->insert('t_gudang_keras', array(
                        'jenis_trx'=>1,
                        't_hasil_wip_id'=>$insert_id,
                        'jenis_barang_id'=>15,
                        'qty'=>(int)$this->input->post('jml_ingot_keras') - (int)$this->input->post('susut_jumlah_keras'),
                        'berat'=>(int)$this->input->post('jml_berat_keras') - (int)$this->input->post('susut_berat_keras'),
                        'created_at'=>$tanggal,
                        'created_by'=>$user_id
                    ));
                }
            }

        if($this->input->post('jml_keras') && $this->input->post('berat_keras') != 0){
            $data = array(
                'jenis_trx'=>0,
                't_hasil_wip_id'=>$insert_id,
                'jenis_barang_id'=>15,
                'qty'=>$this->input->post('jml_keras'),
                'berat'=> $this->input->post('berat_keras'),
                'created_at'=> $tanggal,
                'created_by'=> $user_id
            );
            #insert data hasil masak
            $this->db->insert('t_gudang_keras', $data);
        }

        if($this->input->post('bs') != 0 || $this->input->post('bs_rolling') != 0 || $this->input->post('bs_ingot') != 0 || $this->input->post('serbuk') != 0 || $this->input->post('bs_8m') != 0){
        
            #insert dtr
            $data_dtr = array(
                        'no_dtr'=> $code_dtr,
                        'tanggal'=> $tgl_input,
                        'supplier_id'=> 580,//ROLLING
                        'status'=> 1,
                        'prd_id'=> $insert_id,
                        'jenis_barang'=> 'RONGSOK',
                        'remarks'=> 'SISA PRODUKSI',
                        'created'=> $tanggal,
                        'created_by'=> $user_id
                    );
            $this->db->insert('dtr', $data_dtr);
            $dtr_id = $this->db->insert_id();
           
            #Create TTR
            $data = array(
                    'no_ttr' => $code_ttr,
                    'tanggal' => $tgl_input,
                    'no_sj' => '',
                    'jmlh_afkiran' => 0,
                    'jmlh_pengepakan' => 0,
                    'jmlh_lain'=> 0,
                    'ttr_status'=> 1,
                    'tgl_approve'=> $tanggal,
                    'tanggal'=> $tgl_input,
                    'dtr_id'=> $dtr_id
            );
            $this->db->insert('ttr', $data);
            $ttr_id = $this->db->insert_id();

            if($this->input->post('jenis_masak') == 'ROLLING'){
                
                #insert bs rolling ke rongsok
                if($this->input->post('bs_rolling') != 0){

                $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));

                $bs_code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);

                $bs_packing = $tgl_code.substr($bs_code,13,5);

                    $this->db->insert('dtr_detail', array(
                        'dtr_id'=>$dtr_id,
                        'rongsok_id'=>20,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs_rolling'),
                        'netto'=>$this->input->post('bs_rolling'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'no_pallete'=>$bs_packing,
                        'created'=>$tanggal,
                        'created_by'=>$user_id,
                        'modified'=>$tanggal,
                        'modified_by'=>$user_id,
                        'tanggal_masuk'=>$tgl_input
                    ));
                    $dtr_detail_id = $this->db->insert_id();

                    $this->db->insert('ttr_detail', array(
                        'ttr_id'=>$ttr_id,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'rongsok_id'=>20,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs_rolling'),
                        'netto'=>$this->input->post('bs_rolling'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'created'=>$tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    ));
                }
                #insert bs ingot ke rongsok
                if($this->input->post('bs_ingot') != 0){

                $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));

                $bs_code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);

                $bs_packing = $tgl_code.substr($bs_code,13,5);
                    $this->db->insert('dtr_detail', array(
                        'dtr_id'=>$dtr_id,
                        'rongsok_id'=>22,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs_ingot'),
                        'netto'=>$this->input->post('bs_ingot'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'no_pallete'=>$bs_packing,
                        'created'=>$tanggal,
                        'created_by'=>$user_id,
                        'modified'=>$tanggal,
                        'modified_by'=>$user_id,
                        'tanggal_masuk'=>$tgl_input
                    ));

                    $dtr_detail_id = $this->db->insert_id();
                    $this->db->insert('ttr_detail', array(
                        'ttr_id'=>$ttr_id,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'rongsok_id'=>22,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs_ingot'),
                        'netto'=>$this->input->post('bs_ingot'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'created'=>$tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    ));
                }

                #insert bs 8mm ke rongsok
                if($this->input->post('bs_8m') != 0){

                $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));

                $bs_code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);

                $bs_packing = $tgl_code.substr($bs_code,13,5);
                    $this->db->insert('dtr_detail', array(
                        'dtr_id'=>$dtr_id,
                        'rongsok_id'=>52,//BS 8,00MMM
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs_8m'),
                        'netto'=>$this->input->post('bs_8m'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'no_pallete'=>$bs_packing,
                        'created'=>$tanggal,
                        'created_by'=>$user_id,
                        'modified'=>$tanggal,
                        'modified_by'=>$user_id,
                        'tanggal_masuk'=>$tgl_input
                    ));
                    $dtr_detail_id = $this->db->insert_id();

                    $this->db->insert('ttr_detail', array(
                        'ttr_id'=>$ttr_id,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'rongsok_id'=>52,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs_8m'),
                        'netto'=>$this->input->post('bs_8m'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'created'=>$tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    ));
                }
                
            }else if($this->input->post('jenis_masak') == 'BAKAR ULANG'){
                #insert bs ke rongsok
                if($this->input->post('bs') != 0){
                    $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));

                    $bs_code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);

                    $bs_packing = $tgl_code.substr($bs_code,13,5);
                    $this->db->insert('dtr_detail', array(
                        'dtr_id'=>$dtr_id,
                        'rongsok_id'=>52,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs'),
                        'netto'=>$this->input->post('bs'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'no_pallete'=>$bs_packing,
                        'created'=>$tanggal,
                        'created_by'=>$user_id,
                        'modified'=>$tanggal,
                        'modified_by'=>$user_id,
                        'tanggal_masuk'=>$tgl_input
                    ));
                    $dtr_detail_id = $this->db->insert_id();

                    $this->db->insert('ttr_detail', array(
                        'ttr_id'=>$ttr_id,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'rongsok_id'=>52,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs'),
                        'netto'=>$this->input->post('bs'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'created'=>$tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    ));
                }

            }else if($this->input->post('jenis_masak') == 'CUCI'){
                #insert bs ke gudang bs
                $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));

                $bs_code = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);

                $bs_packing = $tgl_code.substr($bs_code,13,5);
                    $this->db->insert('dtr_detail', array(
                        'dtr_id'=>$dtr_id,
                        'rongsok_id'=>51,
                        'qty'=>0,
                        'netto'=>$this->input->post('bs'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'no_pallete'=>$bs_packing,
                        'created'=>$tanggal,
                        'created_by'=>$user_id,
                        'modified'=>$tanggal,
                        'modified_by'=>$user_id,
                        'tanggal_masuk'=>$tgl_input
                    ));

                    $dtr_detail_id = $this->db->insert_id();

                    $this->db->insert('ttr_detail', array(
                        'ttr_id'=>$ttr_id,
                        'dtr_detail_id'=>$dtr_detail_id,
                        'rongsok_id'=>51,
                        'qty'=>0,
                        'bruto'=>$this->input->post('bs'),
                        'netto'=>$this->input->post('bs'),
                        'line_remarks'=>'SISA PRODUKSI',
                        'created'=>$tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    ));
            }
        }
            $jenis = $this->input->post('jenis_masak');
            $jenis = str_replace('%20', ' ', $jenis);
            if($this->db->trans_complete()){
                $this->session->set_flashdata('flash_msg','Simpan Data Produksi WIP Berhasil.');
            } else{
                $this->session->set_flashdata('flash_msg','Simpan Data Produksi WIP Gagal, Silahkan Coba Lagi.');
                redirect('index.php/GudangWIP/proses_wip'.$jenis);    
            } 
        } else {
            $this->session->set_flashdata('flash_msg','Penyimpanan Data Produksi WIP Gagal, Penomoran Produksi WIP Belum di Set');
        }  
        redirect('index.php/GudangWIP/proses_bpb/'.$insert_id_bpb_wip);  
    }

    function edit_produksi_wip(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id'); 
        $id = $this->uri->segment(3);

        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Edit Hasil Barang WIP";
        $data['content']   = "gudangwip/edit_produksi_wip";

        $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_thw($id)->row_array();
            // $data['details'] = $this->Model_gudang_wip->show_detail_bpb($id)->result();
            $data['jenis_barang'] = $this->Model_gudang_wip->jenis_barang_list()->result();
        
        $this->load->view('layout', $data);  
    }

    function update_proses_wip(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        
        $this->db->trans_start();
        
        #Update status SPB
        $this->db->where('id', $this->input->post('id_thw'));
        $this->db->update('t_hasil_wip', array(
                        'tanggal'=>$tgl_input,
                        'jenis_barang_id'=>$this->input->post('jenis_barang')
        ));

        $this->db->where('bpb_wip_id', $this->input->post('id_bpb'));
        $this->db->update('t_bpb_wip_detail', array(
            'jenis_barang_id'=>$this->input->post('jenis_barang')
        ));

        $this->db->where('id', $this->input->post('id_bpb'));
        $this->db->update('t_bpb_wip', array(
            'tanggal'=>$tgl_input
        ));
            
            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'Detail SPB sudah disimpan');            
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
            }             
        
       redirect('index.php/GudangWIP/produksi_wip/'.$this->input->post('jenis_masak'));
    }

    function update_proses_wip_app(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $jenis = $this->input->post('jenis_masak');
        
        $this->db->trans_start();

        if($jenis == 'ROLLING'){
            #Update status SPB
            $this->db->where('id', $this->input->post('id_thw'));
            $this->db->update('t_hasil_wip', array(
                            'tanggal'=>$tgl_input,
                            'jenis_barang_id'=>$this->input->post('jenis_barang'),
                            'qty'=> $this->input->post('kh'),
                            'berat'=> $this->input->post('berat_kh'),
                            'susut'=> $this->input->post('susut'),
                            'bs'=> $this->input->post('bs'),
                            'bs_ingot'=> $this->input->post('bs_ingot')
            ));

            $this->load->model('Model_gudang_wip');
            $get = $this->Model_gudang_wip->get_bpb_detail($this->input->post('id_bpb'))->row_array();

            $this->db->where('id', $get['id']);
            $this->db->update('t_bpb_wip_detail', array(
                'created'=> $tgl_input,
                'jenis_barang_id'=>$this->input->post('jenis_barang'),
                'qty'=> $this->input->post('kh'),
                'berat'=> $this->input->post('berat_kh')
            ));

            $this->db->where('t_bpb_wip_detail_id', $get['id']);
            $this->db->update('t_gudang_wip', array(
                'tanggal'=>$tgl_input,
                'qty'=> $this->input->post('kh'),
                'berat'=> $this->input->post('berat_kh')
            ));

            $this->db->where('id', $this->input->post('id_bpb'));
            $this->db->update('t_bpb_wip', array(
                'tanggal'=>$tgl_input
            ));

            if($this->input->post('bs_old')==0 && $this->input->post('bs_ingot_old')==0){
                $nomor = explode('.', $this->input->post('no_produksi'));

                $no_produksi = $nomor[2];
                if($this->input->post('jenis_masak')=='ROLLING'){
                    $code_dtr = 'DTR-ROL.'.$tgl_code.'.'.$no_produksi;
                    $code_ttr = 'BPBR-ROL.'.$tgl_code.'.'.$no_produksi;
                }elseif($this->input->post('jenis_masak')=='CUCI'){
                    $code_dtr = 'DTR-CC.'.$tgl_code.'.'.$no_produksi;
                    $code_ttr = 'BPBR-CC.'.$tgl_code.'.'.$no_produksi;
                }

                #insert dtr
                $data_dtr = array(
                            'no_dtr'=> $code_dtr,
                            'tanggal'=> $tgl_input,
                            'supplier_id'=> 580,//ROLLING
                            'status'=> 1,
                            'prd_id'=> $this->input->post('id_thw'),
                            'jenis_barang'=> 'RONGSOK',
                            'remarks'=> 'SISA PRODUKSI',
                            'created'=> $tgl_input,
                            'created_by'=> $user_id
                        );
                $this->db->insert('dtr', $data_dtr);
                $dtr_id = $this->db->insert_id();
               
                #Create TTR
                $data = array(
                        'no_ttr' => $code_ttr,
                        'tanggal' => $tgl_input,
                        'no_sj' => '',
                        'jmlh_afkiran' => 0,
                        'jmlh_pengepakan' => 0,
                        'jmlh_lain'=> 0,
                        'ttr_status'=> 1,
                        'tgl_approve'=> $tgl_input,
                        'tanggal'=> $tgl_input,
                        'dtr_id'=> $dtr_id
                );
                $this->db->insert('ttr', $data);
                $ttr_id = $this->db->insert_id();
            }else{
                #get dtr
                $get_dtr = $this->Model_gudang_wip->get_dtr_prd($this->input->post('id_thw'))->row_array();

                $dtr_id = $get_dtr['id'];
                $ttr_id = $get_dtr['id_ttr'];
            }

            // echo $dtr_id.' | '.$ttr_id;die();

            //20 BS ROLLING
            if($this->input->post('bs') > 0 && $this->input->post('bs_old')==0){
                        $this->db->insert('dtr_detail', array(
                            'dtr_id'=>$dtr_id,
                            'rongsok_id'=>20,
                            'qty'=>0,
                            'bruto'=>$this->input->post('bs'),
                            'netto'=>$this->input->post('bs'),
                            'line_remarks'=>'SISA PRODUKSI',
                            'no_pallete'=>$bs_packing,
                            'created'=>$tanggal,
                            'created_by'=>$user_id,
                            'modified'=>$tanggal,
                            'modified_by'=>$user_id,
                            'tanggal_masuk'=>$tgl_input
                        ));
                        $dtr_detail_id = $this->db->insert_id();

                        $this->db->insert('ttr_detail', array(
                            'ttr_id'=>$ttr_id,
                            'dtr_detail_id'=>$dtr_detail_id,
                            'rongsok_id'=>20,
                            'qty'=>0,
                            'bruto'=>$this->input->post('bs'),
                            'netto'=>$this->input->post('bs'),
                            'line_remarks'=>'SISA PRODUKSI',
                            'created'=>$tanggal,
                            'created_by'=> $user_id,
                            'modified'=> $tanggal,
                            'modified_by'=> $user_id
                        ));
            }elseif($this->input->post('bs')!=$this->input->post('bs_old')){
                $get_id = $this->db->query("select id from dtr_detail where dtr_id =".$dtr_id." and rongsok_id= 20")->row_array();
                $this->db->where('id', $get_id['id']);
                $this->db->update('dtr_detail', array(
                    'bruto'=>$this->input->post('bs'),
                    'netto'=>$this->input->post('bs'),
                    'tanggal_masuk'=>$tgl_input
                ));

                $this->db->where('dtr_detail_id', $get_id['id']);
                $this->db->update('ttr_detail', array(
                    'bruto'=>$this->input->post('bs'),
                    'netto'=>$this->input->post('bs')
                ));
            }

            //22 BS INGOT
            if($this->input->post('bs_ingot') > 0 && $this->input->post('bs_ingot_old')==0){
                        $this->db->insert('dtr_detail', array(
                            'dtr_id'=>$dtr_id,
                            'rongsok_id'=>22,
                            'qty'=>0,
                            'bruto'=>$this->input->post('bs_ingot'),
                            'netto'=>$this->input->post('bs_ingot'),
                            'line_remarks'=>'SISA PRODUKSI',
                            'no_pallete'=>$bs_packing,
                            'created'=>$tanggal,
                            'created_by'=>$user_id,
                            'modified'=>$tanggal,
                            'modified_by'=>$user_id,
                            'tanggal_masuk'=>$tgl_input
                        ));
                        $dtr_detail_id = $this->db->insert_id();

                        $this->db->insert('ttr_detail', array(
                            'ttr_id'=>$ttr_id,
                            'dtr_detail_id'=>$dtr_detail_id,
                            'rongsok_id'=>22,
                            'qty'=>0,
                            'bruto'=>$this->input->post('bs_ingot'),
                            'netto'=>$this->input->post('bs_ingot'),
                            'line_remarks'=>'SISA PRODUKSI',
                            'created'=>$tanggal,
                            'created_by'=> $user_id,
                            'modified'=> $tanggal,
                            'modified_by'=> $user_id
                        ));
            }elseif($this->input->post('bs_ingot')!=$this->input->post('bs_ingot_old')){
                $get_id = $this->db->query("select id from dtr_detail where dtr_id =".$dtr_id." and rongsok_id= 22")->row_array();
                $this->db->where('id', $get_id['id']);
                $this->db->update('dtr_detail', array(
                    'bruto'=>$this->input->post('bs_ingot'),
                    'netto'=>$this->input->post('bs_ingot'),
                    'tanggal_masuk'=>$tgl_input
                ));

                $this->db->where('dtr_detail_id', $get_id['id']);
                $this->db->update('ttr_detail', array(
                    'bruto'=>$this->input->post('bs_ingot'),
                    'netto'=>$this->input->post('bs_ingot')
                ));
            }
        }elseif($jenis == 'CUCI'){
            // echo 'cuci';die();
            $this->db->where('id', $this->input->post('id_thw'));
            $this->db->update('t_hasil_wip', array(
                            'tanggal'=>$tgl_input,
                            'jenis_barang_id'=>$this->input->post('jenis_barang'),
                            'qty'=> $this->input->post('kh'),
                            'berat'=> $this->input->post('berat_kh'),
                            'susut'=> $this->input->post('susut')
            ));

            $this->load->model('Model_gudang_wip');
            $get = $this->Model_gudang_wip->get_bpb_detail($this->input->post('id_bpb'))->row_array();

            $this->db->where('id', $get['id']);
            $this->db->update('t_bpb_wip_detail', array(
                'created'=> $tgl_input,
                'jenis_barang_id'=>$this->input->post('jenis_barang'),
                'qty'=> $this->input->post('kh'),
                'berat'=> $this->input->post('berat_kh')
            ));

            $this->db->where('t_bpb_wip_detail_id', $get['id']);
            $this->db->update('t_gudang_wip', array(
                'tanggal'=>$tgl_input,
                'qty'=> $this->input->post('kh'),
                'berat'=> $this->input->post('berat_kh')
            ));

            $this->db->where('id', $this->input->post('id_bpb'));
            $this->db->update('t_bpb_wip', array(
                'tanggal'=>$tgl_input
            ));
        }
            
            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'Detail Produksi sudah disimpan');            
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat update Produksi, silahkan coba kembali!');
            }             
        
       redirect('index.php/GudangWIP/edit_wip/'.$this->input->post('id_thw'));
    }

    function delete_produksi_wip(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $this->db->trans_start();

            $this->load->model('Model_gudang_wip');
            $header  = $this->Model_gudang_wip->show_header_thw($id)->row_array();
            $this->db->where('id', $header['id']);
            $this->db->delete('t_hasil_wip');

            $this->db->where('id', $header['hasil_masak_id']);
            $this->db->delete('t_hasil_masak');

            if(!empty($header['id_produksi_ingot'])){
                $this->db->where('id', $header['id_produksi_ingot']);
                $this->db->delete('produksi_ingot');

                $this->db->where('produksi_ingot_id', $header['id_produksi_ingot']);
                $this->db->delete('produksi_ingot_detail');
            }

            $this->db->where('id', $header['id_bpb']);
            $this->db->delete('t_bpb_wip');

            $this->db->where('bpb_wip_id', $header['id_bpb']);
            $this->db->delete('t_bpb_wip_detail');

            if(!empty($header['id_dtr'])){
                $this->db->where('id', $header['id_dtr']);
                $this->db->delete('dtr');

                $this->db->where('dtr_id', $header['id_dtr']);
                $this->db->delete('dtr_detail');
            }

            if($header['jenis_masak']=='CUCI'){
                $this->db->where('id', $header['t_spb_wip_id']);
                $this->db->update('t_spb_wip', array(
                    'flag_produksi'=>3
                ));
            }elseif($header['jenis_masak']=='BAKAR ULANG'){
                $this->db->where('t_hasil_wip_id', $header['id']);
                $this->db->delete('t_gudang_keras');
            }

            if($this->db->trans_complete()){
                $this->session->set_flashdata('flash_msg', 'Data Produksi berhasil dihapus');
                if ($header['jenis_masak'] == 'ROLLING') {
                    redirect('index.php/GudangWIP/produksi_wip/ROLLING');
                } else if ($header['jenis_masak'] == 'CUCI') {
                    redirect('index.php/GudangWIP/produksi_wip/CUCI');
                } else if ($header['jenis_masak'] == 'INGOT') {
                    redirect('index.php/Ingot/hasil_produksi');
                } else {
                    redirect('index.php/GudangWIP/produksi_wip/BAKAR%20ULANG');
                }
            }else{
                $this->session->set_flashdata('flash_msg', 'Data Produksi gagal dihapus');
                if ($header['jenis_masak'] == 'ROLLING') {
                    redirect('index.php/GudangWIP/produksi_wip/ROLLING');
                } else if ($header['jenis_masak'] == 'CUCI'){
                    redirect('index.php/GudangWIP/produksi_wip/CUCI');
                } else if ($header['jenis_masak'] == 'INGOT'){
                    redirect('index.php/Ingot/hasil_produksi');
                } else {
                    redirect('index.php/GudangWIP/produksi_wip/BAKAR%20ULANG');
                }
            }
        }
    }

    function edit_wip(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id'); 
        $id = $this->uri->segment(3);

        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Edit Hasil Barang WIP";
        $data['content']   = "gudangwip/edit_wip";

        $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_thw_edit($id)->row_array();
            $data['details'] = $this->Model_gudang_wip->show_dtr_produksi($id)->result();
            foreach ($data['details'] as $k => $value) {
                $query = $this->Model_gudang_wip->get_spb_detail($value->id)->result_array();
                $data['details_s']=$query;
            }
            // print_r($data['details_s']);die();
            $data['jenis_barang'] = $this->Model_gudang_wip->jenis_barang_list()->result();
        
        $this->load->view('layout', $data);  
    }

    function send(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Send Rongsok";
        $data['content']   = "Gudang/add";
        
        
        $this->load->view('layout', $data);  
    }

    function save_sendrongsok(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));

        $data = array(
             
                'tanggal'=> $tgl_input,
                'no_spb'=>$this->input->post('no_spb'),
                'keterangan'=>$this->input->post('keterangan'),
                'dibuat_oleh'=> $user_id,
            );

                $this->db->insert('t_spb_rongsok', $data);
               
           
                redirect('index.php/Gudang');  
           
    }

    function bpb_list(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');   
        $user_ppn    = $this->session->userdata('user_ppn');

        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        $data['content']= "gudangwip/bpb_list";
        $this->load->model('Model_gudang_wip');
        $data['list_data'] = $this->Model_gudang_wip->bpb_list($user_ppn)->result();

        $this->load->view('layout', $data);
    }

    function proses_bpb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "gudangwip/proses_bpb";
            $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_bpb($id)->row_array(); 
            $data['details'] = $this->Model_gudang_wip->show_detail_bpb($id)->result();
            
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/bpb_list');
        }
    }
   
    function approve_bpb(){
        $bpb_id = $this->input->post('id_bpb_wip');
        $user_id  = $this->session->userdata('user_id');
        $user_ppn = $this->session->userdata('user_ppn');

        $hasil_wip_id = $this->input->post('id_hasil_wip');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $return_data = array();
        
        $this->db->trans_start();       
         
            #Update status BPB
            $bpb_update = array(
                    'status'=>1,
                    'tanggal'=>$tgl_input,
                    'keterangan' => $this->input->post('remarks'),
                    'approved_date'=>$tanggal,
                    'approved_by'=>$user_id);

            $this->db->where('id', $bpb_id);
            $this->db->update('t_bpb_wip', $bpb_update);
            
            $detail_push = [];

            #Create Inventori WIP
            $details = $this->input->post('details');
            foreach ($details as $k => $v) {    
                $data = array(
                        'tanggal'=> $tgl_input,
                        'flag_ppn'=> $user_ppn,
                        'flag_taken'=>0,
                        't_spb_wip_detail_id' =>$v['id_spb_detail'],
                        't_hasil_wip_id'=> $hasil_wip_id,
                        'jenis_barang_id' => $v['id_jenis_barang'] ,
                        't_bpb_wip_detail_id'=>$v['id'],
                        'qty' =>$v['qty'],
                        'uom' =>$v['uom'],
                        'berat' =>str_replace('.', '', $v['berat']),
                        'keterangan' =>null,
                        'created_by'=> $user_id
                );
                $this->db->insert('t_gudang_wip', $data);
                    if($user_ppn == 1){
                        $tgf_id = $this->db->insert_id();
                        $data_id = array('reff1' => $tgf_id);
                        unset($data['flag_ppn']);
                        unset($data['created_by']);
                        $detail_push[$k] = array_merge($details[$k], $data_id);
                    }
            }

                if($user_ppn == 1 && strpos($this->input->post('remarks'), 'BARANG PO') !== false ){
                    $this->load->helper('target_url');

                    $data_post['bpb_id'] = $bpb_id;
                    $data_post['tgl_input'] = $tgl_input;
                    $data_post['bpb'] = $bpb_update;
                    $data_post['details'] = $detail_push;
                    $detail_post = json_encode($data_post);

                    // print_r($detail_post);
                    // die();

                    $ch = curl_init(target_url().'api/BeliWIPAPI/bpb');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY: 34a75f5a9c54076036e7ca27807208b8'));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $detail_post);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    $result = json_decode($response, true);
                    curl_close($ch);
                }

            if($this->db->trans_complete()){
                
                $this->session->set_flashdata("message", "Inventori WIP sudah dibuat dan masuk gudang");
            }else{
                $this->session->set_flashdata("message","Pembuatan Inventori WIP gagal, silahkan coba lagi!");
            }                  
        
      redirect("index.php/GudangWIP/bpb_list");
    }

    function print_bpb(){
        $id = $this->uri->segment(3);
        if($id){        
            $this->load->helper('tanggal_indo_helper');
            $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_bpb($id)->row_array();
            $data['details'] = $this->Model_gudang_wip->show_detail_bpb($id)->result();

            $this->load->view('gudangwip/print_bpb', $data);
        }else{
            redirect('index.php'); 
        }
    }

    function edit_bpb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "gudangwip/edit_bpb";
            $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_bpb($id)->row_array(); 
            $data['details'] = $this->Model_gudang_wip->show_detail_bpb($id)->result();
            
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/bpb_list');
        }
    }

    function update_bpb(){
        $bpb_id = $this->input->post('id_bpb_wip');
        $user_id  = $this->session->userdata('user_id');
        $user_ppn = $this->session->userdata('user_ppn');

        $hasil_wip_id = $this->input->post('id_hasil_wip');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $return_data = array();
        
        $this->db->trans_start();       
         
            #Update status BPB
            $bpb_update = array(
                    'no_bpb'=> $this->input->post('no_bpb'),
                    'tanggal'=>$tgl_input,
                    'keterangan' => $this->input->post('remarks'));

            $this->db->where('id', $bpb_id);
            $this->db->update('t_bpb_wip', $bpb_update);

            #Create Inventori WIP
            $details = $this->input->post('details');
            // print_r($details);die();
            foreach ($details as $k => $v) {    
                $data = array(
                        'created'=> $tgl_input,
                        'qty' =>$v['qty'],
                        'berat' =>$v['berat']
                );
                $this->db->where('id', $v['id']);
                $this->db->update('t_bpb_wip_detail', $data);

                $data_g = array(
                        'tanggal'=> $tgl_input,
                        'qty' =>$v['qty'],
                        'berat' =>$v['berat']
                );
                $this->db->where('t_bpb_wip_detail_id', $v['id']);
                $this->db->update('t_gudang_wip', $data_g);
            }

            if($this->db->trans_complete()){
                $this->session->set_flashdata("message", "Inventori WIP sudah dibuat dan masuk gudang");
            }else{
                $this->session->set_flashdata("message", "Pembuatan Inventori WIP gagal, silahkan coba lagi!");
            }                  
        
      redirect("index.php/GudangWIP/edit_bpb/".$bpb_id);
    }

    function spb_list(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');
        $jenis = $this->uri->segment(3);
        if ($jenis == "CUCI") {
            $flag_produksi = 3;
        } else if($jenis == "ROLLING") {
            $flag_produksi = 2;
        } else {
            $flag_produksi = null;
        }

        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        $data['content']= "gudangwip/spb_list";
        $this->load->model('Model_gudang_wip');
        $data['list_data'] = $this->Model_gudang_wip->spb_list($flag_produksi)->result();

        $this->load->view('layout', $data);
    }

    function add_spb(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        $data['content']= "gudangwip/add_spb";
        $this->load->model('Model_gudang_wip');
        
        $this->load->view('layout', $data);
    }

    function spb_kirim_rongsok($id){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

        if($id!=null){
            $id_barang_gudang = $id;
            $this->load->model('Model_gudang_wip');
            $data['barang'] =  $this->Model_gudang_wip->show_barang_wip($id_barang_gudang)->row_array();
            $data['content']= "gudangwip/kirim_rongsok";
            $this->load->view('layout', $data);
            
        }else{
             redirect('index.php/GudangWIP/');
        }
    }

    function save_spb_kirim_rongsok(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        
        $this->load->model('Model_m_numberings');
        $code = $this->Model_m_numberings->getNumbering('SPB-WIP', $tgl_input); 
        
        if($code){     
            $this->db->trans_start();
            #insert data spb wip
            $data = array(
                'no_spb_wip'=> $code,
                'tanggal'=> $tgl_input,
                'keterangan'=>$this->input->post('remarks'),
                'created'=> $tanggal,
                'created_by'=> $user_id
            );
            $this->db->insert('t_spb_wip', $data);
            $id_spb = $this->db->insert_id();

            #insert data spb wip detail
            $data_detail = array(
                't_spb_wip_id' => $id_spb,
                'qty'=>$this->input->post('qty'),
                'uom'=>$this->input->post('uom'),
                'berat' => $this->input->post('berat'),
                'tanggal' => $tgl_input,
                'jenis_barang_id'=>$this->input->post('id_jenis_barang'),
                'keterangan'=>$this->input->post('keterangan')
                );
            $this->db->insert('t_spb_wip_detail',$data_detail);

            #insert DTR ke gudang rongsok
            $code_DTR = $this->Model_m_numberings->getNumbering('DTR', $tgl_input); 
               
            $data = array(
                        'no_dtr'=> $code_DTR,
                        'tanggal'=> $tgl_input,
                        'jenis_barang'=> 'RONGSOK',
                        'remarks'=> $this->input->post('remarks'),
                        'created'=> $tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    );
            $this->db->insert('dtr', $data);
            $dtr_id = $this->db->insert_id();
            
            #insert DTR_Detail ke gudang rongsok
            $rand = strtoupper(substr(md5(microtime()),rand(0,26),3));
            $this->db->insert('dtr_detail', array(
                        'dtr_id'=>$dtr_id,
                        //sisa WIP id 8
                        'rongsok_id' => 8,
                        'qty'=>$this->input->post('qty'),
                        'netto'=>$this->input->post('berat'),
                        'no_pallete'=>date("dmyHis").$rand,
                        'line_remarks'=>$this->input->post('keterangan')
                    ));
                   
               
            if($this->db->trans_complete()){
                redirect('index.php/GudangWIP/');  
            }else{
                $this->session->set_flashdata('flash_msg', 'Data SPB WIP Kirim Rongsok gagal disimpan, silahkan dicoba kembali!');
                redirect('index.php/GudangWIP/spb_kirim_rongsok'.$this->input->post('id_gudang'));  
            }            
        }else{
            $this->session->set_flashdata('flash_msg', 'Data SPB WIP Kirim Rongsok gagal disimpan, penomoran belum disetup!');
            redirect('index.php/GudangWIP/');
        }
    }

    function save_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $tgl_code = date('Ym', strtotime($this->input->post('tanggal')));
        
        // $this->load->model('Model_m_numberings');
        if($this->input->post('jenis_masak')=='ROLLING' || $this->input->post('flag_produksi')==2){
            $code = 'SPB-ROL.'.$tgl_code.'.'.$this->input->post('no_spb');
        }elseif($this->input->post('jenis_masak')=='BAKAR ULANG'){
            $code = 'SPB-BU.'.$tgl_code.'.'.$this->input->post('no_spb');
        }elseif($this->input->post('jenis_masak')=='CUCI' || $this->input->post('flag_produksi')==3){
            $code = 'SPB-CC.'.$tgl_code.'.'.$this->input->post('no_spb');
        }else{
            $code = 'SPB-WIP.'.$tgl_code.'.'.$this->input->post('no_spb');
        }
        // $code = $this->Model_m_numberings->getNumbering('SPB-WIP', $tgl_input); 
        
        if($code){
            if($this->input->post('flag_produksi') == "2"){
                $remarks = "ROLLING | ".$this->input->post('remarks');
            } else if ($this->input->post('flag_produksi') == "3"){
                $remarks = "CUCI | ".$this->input->post('remarks');
            } else {
                $remarks = $this->input->post('remakrs');
            }        
            $data = array(
                'no_spb_wip'=> $code,
                'flag_produksi'=> $this->input->post('flag_produksi'),
                'tanggal'=> $tgl_input,
                'keterangan'=>$remarks,
                'created'=> $tanggal,
                'created_by'=> $user_id
            );

            if($this->db->insert('t_spb_wip', $data)){
                redirect('index.php/GudangWIP/edit_spb/'.$this->db->insert_id());  
            }else{
                $this->session->set_flashdata('flash_msg', 'Data SPB WIP gagal disimpan, silahkan dicoba kembali!');
                redirect('index.php/GudangWIP/add_spb');  
            }            
        }else{
            $this->session->set_flashdata('flash_msg', 'Data SPB WIP gagal disimpan, penomoran belum disetup!');
            redirect('index.php/GudangWIP/spb_list');
        }
    }

    function edit_spb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "gudangwip/edit_spb";
            $this->load->model('Model_gudang_wip');
            $data['header'] = $this->Model_gudang_wip->show_header_spb($id)->row_array();
            $jenis = $data['header']['flag_produksi'];
            if($jenis==2){
                $data['list_barang'] = $this->Model_gudang_wip->jenis_barang_spb(2)->result();
            }else if($jenis==3){
                $data['list_barang'] = $this->Model_gudang_wip->jenis_barang_spb_cuci()->result();
            }else if($jenis==5){
                $data['list_barang'] = $this->Model_gudang_wip->jenis_barang_list()->result();
                $this->load->model('Model_beli_rongsok');
                $data['rongsok'] = $this->Model_beli_rongsok->show_data_rongsok()->result();
            }else{
                $data['list_barang'] = $this->Model_gudang_wip->jenis_barang_list()->result();
            }

            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/GudangWIP/spb_list');
        }
    }

    function delete_spb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        $flag_produksi = $this->uri->segment(4);
        if($id){
            $this->db->trans_start();

            $this->db->where('id', $id);
            $this->db->delete('t_spb_wip');

            $this->db->where('t_spb_wip_id', $id);
            $this->db->delete('t_spb_wip_detail');

            if($this->db->trans_complete()){
                $this->session->set_flashdata('flash_msg', 'Data SPB WIP berhasil dihapus');
                if ($flag_produksi == 1 || $flag_produksi == 3) {
                    redirect('index.php/GudangWIP/spb_list/CUCI');
                } else {
                    redirect('index.php/GudangWIP/spb_list/');
                }
                
            }else{
                $this->session->set_flashdata('flash_msg', 'Data SPB WIP gagal dihapus');
                if ($flag_produksi == 1 || $flag_produksi == 3) {
                    redirect('index.php/GudangWIP/spb_list/CUCI');
                } else {
                    redirect('index.php/GudangWIP/spb_list/');
                }
            }
        }
    }

    function load_detail_spb(){
        $id = $this->input->post('id_spb');
        $tabel = "";
        $no    = 1;
        $arr_barang = array();
        $this->load->model('Model_gudang_wip');
        $list_barang = $this->Model_gudang_wip->jenis_barang_list()->result();
        
        $myDetail = $this->Model_gudang_wip->load_detail($id)->result(); 
        foreach ($myDetail as $row){
            $tabel .= '<tr>';
            $tabel .= '<td style="text-align:center">'.$no.'</td>';
            $tabel .= '<td>'.$row->jenis_barang.'</td>';
            $tabel .= '<td>'.$row->qty.' '.$row->uom.'</td>';
            $tabel .= '<td>'.$row->berat.'</td>';
            $tabel .= '<td>'.$row->keterangan.'</td>';
            $tabel .= '</tr>';
            $arr_barang[] = $row->jenis_barang;            
            $no++;
        }

        header('Content-Type: application/json');
        echo json_encode(array('tabel'=>$tabel,'barang'=>implode($arr_barang, ',')));
    }

    function load_detail(){
        $id = $this->input->post('id');
        
        $tabel = "";
        $no    = 1;
        $this->load->model('Model_gudang_wip');
        
        $myDetail = $this->Model_gudang_wip->load_detail($id)->result(); 
        foreach ($myDetail as $row){
            $tabel .= '<tr>';
            $tabel .= '<td style="text-align:center">'.$no.'</td>';
            $tabel .= '<td>'.$row->jenis_barang.'</td>';
            $tabel .= '<td>'.$row->uom.'</td>';
            $tabel .= '<td>'.$row->qty.'</td>';
            $tabel .= '<td>'.$row->berat.'</td>';
            $tabel .= '<td>'.$row->keterangan.'</td>';
            $tabel .= '<td style="text-align:center"><a href="javascript:;" class="btn btn-xs btn-circle '
                    . 'red" onclick="hapusDetail('.$row->id.');" style="margin-top:5px"> '
                    . '<i class="fa fa-trash"></i> Delete </a></td>';
            $tabel .= '</tr>';            
            $no++;
        }

        header('Content-Type: application/json');
        echo json_encode($tabel); 
    }

    function get_uom_spb(){
        $id = $this->input->post('id');
        $this->load->model('Model_gudang_wip');
        $barang= $this->Model_gudang_wip->show_data_barang_spb($id)->row_array();
        
        header('Content-Type: application/json');
        echo json_encode($barang); 
    }

    function get_uom_view_spb(){
        $id = $this->input->post('id');
        $spb_id = $this->input->post('spb_id');
        $this->load->model('Model_gudang_wip');
        $barang= $this->Model_gudang_wip->show_data_barang_view_spb($id,$spb_id)->row_array();
        
        header('Content-Type: application/json');
        echo json_encode($barang); 
    }

    function get_uom(){
        $id = $this->input->post('id');
        $this->load->model('Model_gudang_wip');
        $barang= $this->Model_gudang_wip->show_data_barang($id)->row_array();
        
        header('Content-Type: application/json');
        echo json_encode($barang); 
    }

    function save_detail(){
        $return_data = array();
        $tgl_input = date("Y-m-d");
        
        if($this->db->insert('t_spb_wip_detail', array(
            't_spb_wip_id'=>$this->input->post('id'),
            'qty'=>$this->input->post('qty'),
            'uom'=>$this->input->post('uom'),
            'berat' => $this->input->post('berat'),
            'tanggal' => $tgl_input,
            'jenis_barang_id'=>$this->input->post('barang_id'),
            'keterangan'=>$this->input->post('line_remarks')
        ))){
            $return_data['message_type']= "sukses";
        }else{
            $return_data['message_type']= "error";
            $return_data['message']= "Gagal menambahkan item barang! Silahkan coba kembali";
        }
        header('Content-Type: application/json');
        echo json_encode($return_data); 
    }

    function delete_detail(){
        $id = $this->input->post('id');
        $return_data = array();
        $this->db->where('id', $id);
        if($this->db->delete('t_spb_wip_detail')){
            $return_data['message_type']= "sukses";
        }else{
            $return_data['message_type']= "error";
            $return_data['message']= "Gagal menghapus item barang! Silahkan coba kembali";
        }           
        header('Content-Type: application/json');
        echo json_encode($return_data);
    }

    function update_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');        
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        
        $this->db->trans_start();

        $data = array(
                'no_spb_wip'=>$this->input->post('no_produksi'),
                'tanggal'=> $tgl_input,
                'keterangan'=>$this->input->post('remarks'),
                'modified_date'=> $tanggal,
                'modified_by'=> $user_id
            );
        
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('t_spb_wip', $data);
        
        if($this->input->post('flag_produksi')==5){
            #insert DTR ke gudang rongsok
            $this->load->model('Model_m_numberings');
            $code_DTR = $this->Model_m_numberings->getNumbering('DTR', $tgl_input); 
               
            $data = array(
                        'no_dtr'=> $code_DTR,
                        'tanggal'=> $tgl_input,
                        'jenis_barang'=> 'RONGSOK',
                        'remarks'=> 'BARANG WIP TRANSFER KE RONGSOK',
                        'created'=> $tanggal,
                        'created_by'=> $user_id,
                        'modified'=> $tanggal,
                        'modified_by'=> $user_id
                    );
            $this->db->insert('dtr', $data);
            $dtr_id = $this->db->insert_id();
            
            #insert DTR_Detail ke gudang rongsok
            $this->load->model('Model_gudang_wip');

            $tgl_code = date('ymd', strtotime($this->input->post('tanggal')));

            $loop = $this->Model_gudang_wip->load_detail($this->input->post('id'))->result();
            foreach ($loop as $row) {
                // $rand = strtoupper(substr(md5(microtime()),rand(0,26),3));
                $code_palette = $this->Model_m_numberings->getNumbering('RONGSOK',$tgl_input);
                $no_pallete= $tgl_code.substr($code_palette,13,5);

                $this->db->insert('dtr_detail', array(
                            'dtr_id'=>$dtr_id,
                            //sisa WIP id 8
                            'rongsok_id' => $this->input->post('rongsok_id'),
                            'qty'=> $row->qty,
                            'bruto'=> $row->berat,
                            'netto'=> $row->berat,
                            'no_pallete'=> $no_pallete,
                            'line_remarks'=> 'Kirim Rongsok dari WIP'
                        ));
            }
        }

        if($this->input->post('flag_produksi')==3){
            $r = 'CUCI';
        }else{
            $r = '';
        }
        if($this->db->trans_complete()){
            $this->session->set_flashdata('flash_msg', 'Data SPB WIP berhasil disimpan');
            redirect('index.php/GudangWIP/view_spb/'.$this->input->post('id'));
        }else{
            $this->session->set_flashdata('flash_msg', 'Gagal');
            redirect('index.php/GudangWIP/edit_spb/'.$this->input->post('id'));
        }
    }

    function view_spb(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['content']= "gudangwip/view_spb";

            $this->load->model('Model_gudang_wip');
            $data['list_barang'] = $this->Model_gudang_wip->jenis_barang_list_by_spb($id)->result();
            $data['myData'] = $this->Model_gudang_wip->show_header_spb($id)->row_array();           
            $data['myDetail'] = $this->Model_gudang_wip->show_detail_spb($id)->result(); 
            $data['detailSPB'] = $this->Model_gudang_wip->show_detail_wip_fulfilment($id)->result();
            $data['detailFulfilment'] = $this->Model_gudang_wip->show_detail_spb_fulfilment($id)->result();
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/GudangWIP/spb_list');
        }
    }

    function delSPBSudahDipenuhi(){
        $id = $this->uri->segment(3);
        $id_spb = $this->uri->segment(4);
        
        $this->db->where('id', $id);
        if($this->db->delete('t_gudang_wip')){
            redirect('index.php/GudangWIP/view_spb/'.$id_spb);
        }else{
            redirect('index.php/GudangWIP/spb_list');
        }
    }

    function save_spb_fulfilment(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d');
        $spb_id = $this->input->post('id');
        
        $this->db->trans_start();
        
        #Update status SPB
        $this->db->where('id', $spb_id);
        $this->db->update('t_spb_wip', array(
                        'status'=> 3,
                        'keterangan' => $this->input->post('remarks'),
                        'modified_date'=> $tanggal,
                        'modified_by'=>$user_id
        ));
            
        #Create Gudang WIP list
        $details = $this->input->post('details');
        foreach ($details as $v) {
            if($v['id_barang']!=''){   
            $this->db->insert('t_spb_wip_fulfilment', array(
                            'jenis_barang_id' => $v['id_barang'],
                            't_spb_wip_id'=> $spb_id,
                            't_spb_wip_detail_id'=> $v['spb_detail_id'],
                            'qty' => $v['qty'],
                            'berat' => $v['berat'],
                            'keterangan' => $v['keterangan'],
                        ));
            }   
        }
            
            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'Detail SPB sudah disimpan');            
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
            }             
        
       redirect('index.php/GudangWIP/view_spb/'.$spb_id);
    }

    function input_ulang_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d');
        $spb_id = $this->input->post('id');
        
        $this->db->trans_start();

        #Update status SPB
        $this->db->where('id', $spb_id);
        $this->db->update('t_spb_wip', array(
                        'status'=> 0,
                        'modified_date'=> $tanggal,
                        'modified_by'=>$user_id
        ));
            
            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'SPB sudah disimpan');            
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
            }             
        
       redirect('index.php/GudangWIP/view_spb/'.$spb_id);
    }

    function simpan_tanggal_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $spb_id = $this->input->post('id');
        $this->db->trans_start();

        #Update status SPB
        $this->db->where('id', $spb_id);
        $this->db->update('t_spb_wip', array(
                        'tanggal'=> $tgl_input,
                        'modified_date'=> $tanggal,
                        'modified_by'=>$user_id
        ));

        $this->db->where('t_spb_wip_id', $spb_id);
        $this->db->update('t_gudang_wip', array(
            'tanggal'=>$tgl_input
        ));
            
            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'SPB sudah disimpan');            
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
            }             
        
       redirect('index.php/GudangWIP/view_spb/'.$spb_id);
    }

    function tambah_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $spb_id = $this->input->post('id');
        
        $this->db->trans_start();

        #Update status SPB
        $this->db->where('id', $spb_id);
        $this->db->update('t_spb_wip', array(
                        'tanggal'=> $tgl_input,
                        'status'=> 4,
                        'modified_date'=> $tanggal,
                        'modified_by'=>$user_id
        ));

        if($this->db->trans_complete()){    
            $this->session->set_flashdata('flash_msg', 'Silahkan tambah barang');                 
        }else{
            $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
        }                 

       redirect('index.php/GudangWIP/view_spb/'.$spb_id);
    }

    function reject_fulfilment(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d');
        $spb_id = $this->input->post('id');

        $this->db->trans_start();

            $this->load->model('Model_gudang_wip');
            $details = $this->Model_gudang_wip->show_detail_spb_fulfilment($spb_id)->result();
            $this->db->where('t_spb_wip_id',$spb_id);
            $this->db->where('approved_by', 0);
            $this->db->delete('t_spb_wip_fulfilment');
            // echo '<pre>';print_r($details);echo'</pre>';
            // die();

            $check = $this->Model_gudang_wip->check_spb_reject($spb_id)->row_array();
            if($check['count'] > 0){
                $status = 4;
            }else{
                $status = 0;
            }
            $this->db->update('t_spb_wip',['status'=>$status],['id'=>$spb_id]);
            // echo $status;
            // die();

            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'SPB sudah di-approve. Detail Pemenuhan SPB sudah disimpan');                 
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
            }
       redirect('index.php/GudangWIP/view_spb/'.$spb_id);
    }

    function approve_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $tgl_input = date('Y-m-d', strtotime($this->input->post('tanggal')));
        $spb_id = $this->input->post('id');
        $flag_produksi = $this->input->post('flag_produksi');

        $this->db->trans_start();
        
        $this->load->model('Model_gudang_wip');
        #Create Gudang WIP list
        $details = $this->Model_gudang_wip->show_detail_spb_fulfilment($spb_id)->result();
        foreach ($details as $v) {
            $this->db->where('id', $v->id);
            $this->db->update('t_spb_wip_fulfilment', array(
                            'approved_at'=> $tanggal,
                            'approved_by'=> $user_id
            ));

            $this->db->insert('t_gudang_wip', array(
                            'jenis_trx' => 1,
                            'flag_taken' => 0,
                            'tanggal' => $tgl_input,
                            'jenis_barang_id' => $v->jenis_barang_id,
                            't_spb_wip_id'=> $spb_id,
                            't_spb_wip_detail_id'=> $v->t_spb_wip_detail_id,
                            'qty' => $v->qty,
                            'uom' => $v->uom,
                            'berat' => $v->berat,
                            'keterangan' => $v->keterangan,
                            'created_by' => $user_id
                        ));
        }
 
        $data['check'] = $this->Model_gudang_wip->check_spb($spb_id)->row_array();
        if(((int)$data['check']['tot_fulfilment']) >= ((int)$data['check']['tot_spb'])){
            $status = 1;
        }else{
            $status = 4;
        }

        #Update status SPB
        $this->db->where('id', $spb_id);
        $this->db->update('t_spb_wip', array(
                        'status'=> $status,
                        'keterangan' => $this->input->post('remarks'),
                        'approved_at'=> $tanggal,
                        'approved_by'=>$user_id
        ));

        // echo $flag_produksi;die();            
            if($this->db->trans_complete()){    
                $this->session->set_flashdata('flash_msg', 'SPB sudah di-approve. Detail SPB sudah disimpan');            
            }else{
                $this->session->set_flashdata('flash_msg', 'Terjadi kesalahan saat pembuatan Balasan SPB, silahkan coba kembali!');
            }             

        if($flag_produksi == 1 || $flag_produksi == 3){
            redirect('index.php/GudangWIP/spb_list/CUCI');
        }elseif($flag_produksi == 2){
            redirect('index.php/GudangWIP/spb_list/ROLLING');
        }else{
            redirect('index.php/GudangWIP/spb_list/');
        }
    }

    function reject_spb(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        
        $data = array(
                'status'=> 9,
                'rejected_at'=> $tanggal,
                'rejected_by'=>$user_id,
                'reject_remarks'=>$this->input->post('reject_remarks')
            );
        
        $this->db->where('id', $this->input->post('header_id'));
        $this->db->update('t_spb_wip', $data);
        
        $this->session->set_flashdata('flash_msg', 'Data SPB WIP berhasil direject');
        redirect('index.php/GudangWIP/spb_list');
    }

    function print_spb(){
        $id = $this->uri->segment(3);
        if($id){
            $this->load->helper('tanggal_indo_helper');
            $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_spb($id)->row_array();
            $data['details'] = $this->Model_gudang_wip->show_detail_spb($id)->result();

            $this->load->view('gudangwip/print_spb', $data);
        }else{
            redirect('index.php'); 
        }
    }

    function print_spb_fulfilment(){
        $id = $this->uri->segment(3);
        if($id){
            $this->load->helper('tanggal_indo_helper');
            $this->load->model('Model_gudang_wip');
            $data['header']  = $this->Model_gudang_wip->show_header_spb($id)->row_array();
            $data['details'] = $this->Model_gudang_wip->show_detail_wip_fulfilment($id)->result();

            $this->load->view('gudangwip/print_spb_fulfilment', $data);
        }else{
            redirect('index.php'); 
        }
    }

    function laporan_list(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;

            $data['content']= "gudangwip/laporan_list";
            $this->load->model('Model_gudang_wip'); 
            //$data['detailTanggal'] = $this->Model_beli_sparepart->show_laporan()->result();
            $data['list'] = $this->Model_gudang_wip->show_laporan()->result();
            $this->load->view('layout', $data);   
    }

    function view_laporan(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $data['end'] = date("Y-m-t", strtotime($id));

            $data['content']= "gudangwip/view_laporan";
            $this->load->model('Model_gudang_fg');
            $data['detailLaporan'] = $this->Model_gudang_fg->show_view_laporan('WIP',$id)->result();
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/GudangFG/laporan_list');
        }
    }

    function print_laporan_bulanan(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $this->load->helper('tanggal_indo');  
            $tgl=explode("-",$id);
            $tahun=$tgl[0];
            $bulan=$tgl[1];

            $tgl = $tahun.'-'.$bulan.'-01';

            $data['tgl'] = array(
                'tahun' => $tahun,
                'bulan' => $bulan
            );          

            $this->load->model('Model_gudang_wip');
            $data['detailLaporan'] = $this->Model_gudang_wip->show_laporan_barang('WIP', $id)->result();
            $this->load->view("gudangwip/print_laporan_bulanan", $data);
        }else{
            redirect('index.php/GudangWIP/laporan_list');
        }
    }

    function view_detail_laporan(){
        $module_name = $this->uri->segment(1);
        $id = $this->uri->segment(3);
        $id_barang = $this->uri->segment(4);
        if($id){
            $group_id    = $this->session->userdata('group_id');        
            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;

            $items = strval($id);
            $tgl=str_split($id,4);
            $tahun=$tgl[0];
            $bulan=$tgl[1];

            $data['tgl'] = array(
                'tahun' => $tahun,
                'bulan' => $bulan
            );

            $data['content']= "gudangwip/view_detail_laporan";
            $this->load->model('Model_gudang_wip');
            $data['detailLaporan'] = $this->Model_gudang_wip->show_laporan_detail($bulan,$tahun,$id_barang)->result();
            $this->load->view('layout', $data);   
        }else{
            redirect('index.php/GudangWIP/laporan_list');
        }
    }

    function proses_inventory(){
        $user_id  = $this->session->userdata('user_id');
        $tanggal  = date('Y-m-d h:m:s');
        $jb = $this->uri->segment(3);
        $tgl_proses = $this->uri->segment(4);

        $this->db->trans_start();
        $this->load->model('Model_gudang_wip');
        $this->load->model('Model_gudang_fg');

            $tgl=explode("-",$tgl_proses);
            $tahun=$tgl[1];
            $bulan=$tgl[0];

            $start = $tahun.'-'.$bulan.'-01';

                if($bulan==01){
                  $bulan_b = 12;
                  $tahun_b = $tahun-1;
                } else {
                  $bulan_b= intval($bulan)-1;
                  if($bulan_b < 10){
                    $bulan_b= '0'.$bulan_b;
                    $tahun_b= $tahun;
                  }
                  $tahun_b = $tahun;
                }

            $before = $tahun_b.'-'.$bulan_b.'-01';

            $end = date("Y-m-t", strtotime($start));
            // echo $start.' | '.$end;

            $jenis_barang = $this->Model_gudang_wip->jenis_barang_list()->result();
            $this->db->where(array(
                'tanggal' => $start,
                'jenis_barang' => $jb
            ));
            $this->db->delete('inventory');
            $no = 0;
            foreach ($jenis_barang as $key => $value) {
                $stok_before = $this->Model_gudang_fg->inventory_stok_before($jb,$before,$value->id)->row_array();
                $t = 1;
                if(empty($stok_before)){
                    $stok_before = $this->Model_gudang_wip->show_kartu_stok_before($start,$end,$value->id)->row_array();
                    $t = 2;
                }
                $trx = $this->Model_gudang_wip->kartu_stok_inventory($start,$end,$value->id)->row_array();
                if(!empty($stok_before) || !empty($trx)){
                    // echo $value->jenis_barang.' | '.$stok_before['netto_masuk'].$stok_before['netto_keluar'].' | ';
                    // echo $trx['netto_masuk'].' - '.$trx['netto_keluar'].'<br>';
                    if($t==1){
                        $stok_awal = $stok_before['stok_akhir'];
                    }else{
                        $stok_awal = $stok_before['berat_in']-$stok_before['berat_out'];
                    }

                    // print_r($trx);
                    $stok_awal_next_month = $stok_awal + $trx['berat_in'] - $trx['berat_out'];

                    // print_r($trx);
                    // echo $value->jenis_barang.' | '.$stok_awal.' | '.$trx['netto_masuk'].' & '.$trx['netto_keluar'].' | '.$stok_awal_next_month.'<br>';
                    if($stok_awal > 0 || $trx['berat_in'] > 0 || $trx['berat_out'] > 0){
                        //stok akhir
                        $this->db->insert('inventory', array(
                            'jenis_barang'=>$jb,
                            'bulan'=>$bulan,
                            'tahun'=>$tahun,
                            'tanggal'=>$start,
                            'jenis_barang_id'=>$value->id,
                            'qty'=> 0,
                            'stok_awal'=>$stok_awal,
                            'netto_masuk'=>((empty($trx['berat_in']))? 0: $trx['berat_in']),
                            'netto_keluar'=>((empty($trx['berat_out']))? 0: $trx['berat_out']),
                            'stok_akhir'=>$stok_awal_next_month,
                            'created_at'=>$tanggal,
                            'created_by'=>$user_id
                        ));
                    }
                }
            }
// die();
        if($this->db->trans_complete()){
            $this->session->set_flashdata('flash_msg', 'Laporan WIP berhasil diproses !');
            redirect(base_url('index.php/GudangWIP/laporan_list'));
        } else {
            $this->session->set_flashdata('flash_msg', 'Laporan WIP gagal diproses, silahkan dicoba kembali!');
            redirect(base_url('index.php/GudangWIP/laporan_list'));
        }
    }

    function stok_wip(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang WIP";
        $data['content']   = "gudangwip/stok_wip";
        
       $this->load->model('Model_gudang_wip');
       $data['gudang_wip'] = $this->Model_gudang_wip->stok_wip()->result();
        
        $this->load->view('layout', $data);  
    }

    
    function kartu_stok_wip(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang wip";
        $data['content']   = "gudangwip/kartu_stok_index";

        $this->load->model('Model_gudang_wip'); 
        $data['list_fg'] = $this->Model_gudang_wip->jenis_barang_list()->result();

        $this->load->view('layout', $data);  
    }

    function kartu_stok(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');

        $jb_id = $_GET['r'];
        $start = date('Y/m/d', strtotime($_GET['ts']));
        $end = date('Y/m/d', strtotime($_GET['te']));
        // echo $start;die();

            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $data['judul']     = "Gudang WIP";

        $this->load->model('Model_beli_fg');
        $data['jb'] = $this->Model_beli_fg->get_jb($jb_id)->row_array();
        $data['start'] = $start;
        $data['end'] = $end;

            $this->load->model('Model_gudang_wip');
            $this->load->model('Model_gudang_fg');
            // $data['stok_before'] = $this->Model_gudang_wip->show_kartu_stok_before($start,$end,$jb_id)->row_array();
        if($_GET['bl']==0){
            if($jb_id==0){
                $loop = $this->Model_gudang_fg->show_view_laporan('WIP',$start)->result_array();
                // print("<pre>".print_r($loop,true)."</pre>");die();
                foreach ($loop as $key => $value) {
                    $data['loop'][$key] = array('header'=>$loop[$key],'detailLaporan'=> $this->Model_gudang_wip->show_kartu_stok_detail($start,$end,$value['jenis_barang_id'])->result());
                }
                    $this->load->view('gudangwip/kartu_stok_all', $data);
            }else{
                $data['stok_before'] = $this->Model_gudang_fg->inventory_stok_before('WIP',$start,$jb_id)->row_array();
                $data['detailLaporan'] = $this->Model_gudang_wip->show_kartu_stok_detail($start,$end,$jb_id)->result();
                $this->load->view('gudangwip/kartu_stok', $data);
            }
        }else{
            $data['stok_before'] = $this->Model_gudang_fg->inventory_stok_before('WIP',$start,$jb_id)->row_array();
            $data['detailLaporan'] = $this->Model_gudang_wip->show_kartu_stok_detail_packing($start,$end,$jb_id)->result();
            $this->load->view('gudangwip/kartu_stok_packing', $data);
        }
    }

    function laporan_masak(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');        
        if($group_id != 1){
            $this->load->model('Model_modules');
            $roles = $this->Model_modules->get_akses($module_name, $group_id);
            $data['hak_akses'] = $roles;
        }
        $data['group_id']  = $group_id;
        $data['judul']     = "Gudang wip";
        $data['content']   = "gudangwip/laporan_masak";

        $this->load->view('layout', $data);  
    }

    function print_laporan_masak(){
        $module_name = $this->uri->segment(1);
        $group_id    = $this->session->userdata('group_id');

        $jb_id = $_GET['r'];
        $start = date('Y/m/d', strtotime($_GET['ts']));
        $end = date('Y/m/d', strtotime($_GET['te']));
        // echo $start;die();

            if($group_id != 1){
                $this->load->model('Model_modules');
                $roles = $this->Model_modules->get_akses($module_name, $group_id);
                $data['hak_akses'] = $roles;
            }
            $data['group_id']  = $group_id;
            $data['judul']     = "Gudang WIP";

        $data['start'] = $start;
        $data['end'] = $end;

            $this->load->model('Model_gudang_wip');
            $data['detailLaporan'] = $this->Model_gudang_wip->print_laporan_masak($start,$end,$jb_id)->result();
            if($jb_id == 1){
                $this->load->view('gudangwip/print_laporan_masak', $data);
            }elseif($jb_id == 2){
                $this->load->view('gudangwip/print_laporan_masak_rolling', $data);
            }elseif($jb_id == 4){
                $this->load->view('gudangwip/print_laporan_masak_cuci', $data);
            }
    }
}