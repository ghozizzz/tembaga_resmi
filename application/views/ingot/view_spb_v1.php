<div class="row">
    <div class="col-md-12 alert-warning alert-dismissable">        
        <h5 style="color:navy">
            <a href="<?php echo base_url(); ?>"> <i class="fa fa-home"></i> Home </a> 
            <i class="fa fa-angle-right"></i> Produksi Ingot
            <i class="fa fa-angle-right"></i> 
            <a href="<?php echo base_url('index.php/Ingot/view_spb'); ?>"> View Surat Permintaan Barang (SPB)</a> 
        </h5>          
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">                            
    <div class="col-md-12">
        <h3 align="center"><b> Konfirmasi Permintaan SPB</b></h3>
        <hr class="divider" />
        <div class="modal fade" id="myModal" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button>
                                    <span id="message">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                        <form class="eventInsForm" method="post" target="_self" name="frmReject" 
                              id="frmReject">                            
                            <div class="row">
                                <div class="col-md-4">
                                    Reject Remarks <font color="#f00">*</font>
                                </div>
                                <div class="col-md-8">
                                    <textarea id="reject_remarks" name="reject_remarks" 
                                        class="form-control myline" style="margin-bottom:5px" 
                                        onkeyup="this.value = this.value.toUpperCase()" rows="3"></textarea>
                                    
                                    <input type="hidden" id="header_id" name="header_id">
                                </div>
                            </div>                           
                        </form>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn blue" onClick="rejectData();">Simpan</button>
                        <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="closeModal" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button>
                                    <span id="message_close">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                        <form class="eventInsForm" method="post" target="_self" name="frmClose" 
                              id="frmClose">                            
                            <div class="row">
                                <div class="col-md-4">
                                    Close Remarks <font color="#f00">*</font>
                                </div>
                                <div class="col-md-8">
                                    <textarea id="close_remarks" name="close_remarks" 
                                        class="form-control myline" style="margin-bottom:5px" 
                                        onkeyup="this.value = this.value.toUpperCase()" rows="3"></textarea>
                                    
                                    <input type="hidden" id="close_spb_id" name="header_id">
                                </div>
                            </div>                           
                        </form>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn blue" onClick="closeSPB();">Simpan</button>
                        <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="parsialModal" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger display-hide" id="box_error_modal">
                                    <button class="close" data-close="alert"></button>
                                    <span id="msg_modal">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                        <!-- <form class="eventInsForm" method="post" target="_self" name="formku" 
                              id="formku">                             -->
                            <!-- <input type="hidden" id="status_vc" name="status_vc"> -->
                            <div class="row">
                                <div class="col-md-5">
                                    Netto
                                </div>
                                <div class="col-md-7">
                                    <input type="text" id="netto" name="netto" 
                                        class="form-control myline" style="margin-bottom:5px" 
                                        readonly="readonly">
                                    
                                    <input type="hidden" id="id_dtr_detail" name="id_dtr_detail" class="form-control">
                                    <input type="hidden" id="id_dtr" name="id_dtr">
                                    <input type="hidden" id="id_barang" name="id_barang">
                                    <input type="hidden" id="nama_item" name="nama_item">
                                    <input type="hidden" id="bruto" name="bruto">
                                    <input type="hidden" id="berat_palette" name="berat_palette">
                                    <input type="hidden" id="no_pallete" name="no_pallete">
                                    <input type="hidden" id="line_remarks" name="line_remarks">
                                    <input type="hidden" id="tanggal_p" name="tanggal_p">
                                    
                                </div>
                            </div>                  
                            <div class="row">
                                <div class="col-md-5">
                                    Netto diambil <font color="#f00">*</font>
                                </div>
                                <div class="col-md-7">
                                    <input type="number" id="netto_ambil" name="netto_ambil" 
                                        class="form-control myline" style="margin-bottom:5px" 
                                        >                                                                       
                                </div>
                            </div> 
                            
                        <!-- </form> -->
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn blue" onClick="saveDetailParsial();">Simpan</button>
                        <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if( ($group_id==1 || $group_id==21)||($hak_akses['view_spb']==1) ){
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="message">&nbsp;</span>
                </div>
            </div>
        </div>
        <form class="eventInsForm" method="post" target="_self" name="formku" 
              id="formku">  
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            No. SPB <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="no_spb" name="no_spb"
                                class="form-control myline" style="margin-bottom:5px" 
                                value="<?php echo $myData['no_spb']; ?>">

                            <input type="hidden" id="id" name="id" value="<?php echo $myData['id']; ?>">
                            <input type="hidden" id="id_pi" name="id_pi" value="<?php echo $myData['id_pi']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Tanggal <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="tanggal" name="tanggal" readonly="readonly"
                                class="form-control myline input-small" style="margin-bottom:5px; float: left;" 
                                value="<?php echo date('d-m-Y', strtotime($myData['tanggal'])); ?>">
                        </div>
                    </div>                    <div class="row">
                        <div class="col-md-4">
                            No. Produksi <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">                       
                            <input type="text" id="no_produksi" name="no_produksi" class="form-control myline" readonly
                                   style="margin-bottom:5px" value="<?php echo $myData['no_produksi']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Jumlah Rongsok yang Dibutuhkan
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="jumlah" name="jmlh" 
                                class="form-control myline" style="margin-bottom:5px;" value="<?php echo $myData['jumlah']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Status SPB
                        </div>
                        <div class="col-md-8">
                            <?php
                                if($myData['status']==0){
                                    echo '<div style="background-color:darkkhaki; padding:3px">Waiting Review</div>';
                                }else if($myData['status']==1){
                                    echo '<div style="background-color:green; padding:3px; color:white">Approved</div>';
                                }else if($myData['status']==2){
                                    echo '<div style="background-color:green; color:#fff; padding:3px">Finished</div>';
                                }else if($myData['status']==3){
                                    echo '<div style="background-color:blue; color:#fff; padding:3px">Waiting Approval</div>';
                                }else if($myData['status']==4){
                                    echo '<div style="background-color:orange; color:#fff; padding:3px">Belum Dipenuhi Semua</div>';
                                }else if($myData['status']==9){
                                    echo '<div style="background-color:red; color:#fff; padding:3px">Rejected</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            Jenis Barang
                        </div>
                        <div class="col-md-9">
                            <input type="text" id="jenis_barang" name="jenis_barang" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $myData['jenis_barang']; ?>">
                        </div>
                    </div> 
                    <?php if($myData['status']==1){?>
                    <div class="row">
                        <div class="col-md-3">
                            Tipe Apolo<font color="#f00">*</font>
                        </div>
                        <div class="col-md-9">
                            <input type="text" id="tipe_apolo" name="tipe_apolo" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $myData['tipe_apolo']; ?>">
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="row">
                        <div class="col-md-3">
                            Tipe Apolo <font color="#f00">*</font>
                        </div>
                        <div class="col-md-6">
                            <select id="id_apolo" name="id_apolo" class="form-control myline select2me" data-placeholder="Silahkan pilih..." style="margin-bottom:5px; ">
                                <option></option>
                                <?php
                                foreach ($apolo as $row) {
                                echo "<option value='".$row->id."'".(($row->id==$myData['id_apolo'])? "selected='selected'":"").">".$row->tipe_apolo."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <a style="margin-top:5px;" href="javascript:;" class="btn btn-circle btn-xs green" onclick="editApolo();"><i class="fa fa-check"></i> Save </a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-3">
                            Catatan
                        </div>
                        <div class="col-md-9">
                            <textarea id="remarks" name="remarks" rows="2" onkeyup="this.value = this.value.toUpperCase()"
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly"><?php echo $myData['remarks']; ?></textarea>                           
                        </div>
                    </div>
                    <?php
                        if($myData['status']=="9"){
                    ?>
                    <div class="row">
                        <div class="col-md-3">
                            Rejected By
                        </div>
                        <div class="col-md-9">
                            <input type="text" id="rejected_by" name="rejected_by" readonly="readonly"
                                   class="form-control myline" style="margin-bottom:5px" value="<?php echo $myData['reject_name']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Reject Remarks
                        </div>
                        <div class="col-md-9">
                            <textarea id="reject_remarks" name="reject_remarks" rows="3" readonly="readonly"
                                class="form-control myline" style="margin-bottom:5px"><?php echo $myData['reject_remarks']; ?></textarea>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>              
            </div>
            
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                                <h4 align="center">Detail SPB dan Ketersediaan Stok</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <th style="width:40px">No</th>
                                            <th>Nama Item</th>
                                            <th>UOM</th>
                                            <th>Quantity</th>
                                            <th>Keterangan</th>
                                            <th>Status</th>
                                            <th>Stok Tersedia</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $no = 1;
                                            $total = 0;
                                            $stok = 0;
                                            foreach ($myDetail as $row){
                                            ($row->qty <= $row->stok) ? $stat = '<div style="background:green;color:white;"><span class="fa fa-check"></span> OK </div>' : $stat = '<div style="background:red;color:white;"> <span class="fa fa-times"></span> NOK</div>';
                                                echo '<tr>';
                                                echo '<td style="text-align:center">'.$no.'</td>';
                                                echo '<td>'.$row->nama_item.'</td>';
                                                echo '<td>'.$row->uom.'</td>';
                                                echo '<td>'.number_format($row->qty,2,',','.').'</td>';
                                                echo '<td>'.$row->line_remarks.'</td>';
                                                echo '<td>'.$stat.'</td>';   
                                                echo '<td>'.number_format($row->stok,2,',','.').'</td>'; 
                                                echo '</tr>';
                                                $no++;
                                                $total += $row->qty;
                                                $stok += $row->stok;
                                            }
                                        ?>
                                        <tr>
                                            <td colspan="3" style="text-align: right;"><strong>Total Permintaan</strong></td>
                                            <td><?=number_format($total,2,',','.');?></td>
                                            <td colspan="2" style="text-align: right;"></td>
                                            <td><strong><?=number_format($stok,2,',','.');?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                    <hr class="divider"/>
                    <?php if ($myData['status']==0 || $myData['status']==4) { ?>
                    <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger display-hide" id="alert-danger2">
                        <button class="close" data-close="alert"></button>
                        <span id="message2">&nbsp;</span>
                    </div>
                </div>
            </div>
            <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-file-word-o"></i>Input Jenis Barang
                    </div>
                    <div class="tools">    
                    
                    </div>    
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            Pilih Jenis Barang
                            <select class="form-control select2me myline" id="dtr_id" name="dtr_id" onchange="loadDetailJb(this.value);">
                                <!-- <?php foreach ($list_dtr as $row) {
                                ?>
                                <option value="<?php echo $row->id ?>"><?php echo $row->no_dtr ?></option>
                                <?php
                                } ?> -->
                            </select>
                        </div>    
                        <div class="col-md-2">
                        </div>                    
                    </div>
                    <div>
                        <br>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <th style="width:40px">No</th>
                                <th>Nama Item</th>
                                <th>Bruto (Kg)</th>
                                <th>Berat Pallete</th>
                                <th>Netto (Kg)</th>
                                <th>Nomor Pallete</th>
                                <th style="text-align: center;">Action</th>
                            </thead>
                            <tbody id="boxDetail">
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="portlet box blue-ebonyclay">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-file-word-o"></i>Pemenuhan Palette Rongsok
                    </div>
                    <div class="tools">    
                    
                    </div>    
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <th style="width:40px">No</th>
                                <th>Nama Item</th>
                                <th>Bruto (Kg)</th>
                                <th>Netto (Kg)</th>
                                <th>Berat Pallete (Kg)</th>
                                <th>Nomor Pallete</th>
                                <th style="text-align: center;">Action</th>
                            </thead>
                            <tbody id="boxDetail2">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 align="center">SPB Rongsok yang Sudah Dipenuhi</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-striped table-hover" id="tabel_pallete">
                                        <thead>
                                            <th style="width:40px">No</th>
                                            <th>Nama Item</th>
                                            <th>No Pallete</th>
                                            <th>Netto</th>
                                            <th>Keterangan</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $no = 1;
                                            $tb = 0;
                                            $tn = 0;
                                            foreach ($detailSPBFulfilment as $row){
                                                echo '<tr>';
                                                echo '<td style="text-align:center">'.$no.'</td>';
                                                echo '<td>'.$row->nama_item.'</td>';
                                                echo '<td>'.$row->no_pallete.'</td>';
                                                echo '<td>'.number_format($row->netto,2,',','.').' '.$row->uom.'</td>';
                                                echo '<td>'.$row->line_remarks.'</td>';
                                                if($row->so_id!=0){
                                                echo '<td style="background-color: green; color: white">Sudah di Kirim</td>';
                                                echo '<td></td>';
                                                }else{
                                                    echo '<td>Belum Dikirim</td>';
                                                }
                                                $tn += $row->netto;
                                                $no++;
                                            }
                                        ?>
                                        <tr>
                                            <td colspan="3">Total</td>
                                            <td style="background-color: green; color: white;"><?=number_format($tn,2,',','.');?></td>
                                            <td colspan="3"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 align="center">Pemenuhan SPB</h4>
                            <div class="row">
                                <div class="col-md-2">
                                    Tanggal Keluar <font color="#f00">*</font>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="tanggal_keluar" name="tanggal_keluar" class="form-control myline input-small" style="margin-bottom:5px; float: left;" value="<?php echo date('d-m-Y'); ?>">
                                </div>
                            </div>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-striped table-hover" id="tabel_pallete">
                                        <thead>
                                            <th style="width:40px">No</th>
                                            <th>Nama Item</th>
                                            <th>No Pallete</th>
                                            <th>Netto</th>
                                            <th>UOM</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <?php $no=1; $total_netto = 0; foreach($detailSPB as $v) { ?>
                                            <tr>
                                                <td><div id="no_tabel_1"><?=$no;?></div></td>
                                                <td><?=$v->nama_item;?></td>
                                                <td><?=$v->no_pallete;?></td>
                                                <td><?=$v->netto;?></td>
                                                <td><?=$v->uom;?></td>
                                                <td><?=$v->line_remarks;?></td>
                                            </tr>
                                            <?php 
                                            $total_netto += $v->netto;
                                            $no++; } ?>
                                            <tr>
                                                <td colspan="3"> Total</td>
                                                <td style="background-color: green; color: white;"><?=number_format($total_netto,2,',','.');?></td>
                                                <td colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-md-10">
                    <?php
                        if( ($group_id==1 || $group_id==21 || $hak_akses['approve_spb']==1) && ($myData['status']=='3' || $myData['status']=='1' || $myData['status']=='9')){
                            echo '<a href="javascript:;" class="btn blue" onclick="tambahData();"> '
                                .'<i class="fa fa-plus"></i> Edit </a> ';
                        }
                        if( ($group_id==1 || $group_id==21 || $hak_akses['save_spb']==1) && ($myData['status']=="0" || $myData['status']=="4")){
                            echo '<a href="javascript:;" class="btn green" onclick="saveFulfilment();"> '
                                .'<i class="fa fa-check"></i> Save </a> ';
                        }
                        if( ($group_id==1 || $group_id==21 || $hak_akses['approve_spb']==1) && $myData['status']=="3"){
                            echo '<a href="javascript:;" class="btn green" onclick="approveData();"> '
                                .'<i class="fa fa-check"></i> Approve </a> ';
                        }
                        if( ($group_id==1 || $group_id==21 || $hak_akses['reject_spb']==1) && $myData['status']=="0"){
                            echo '<a href="javascript:;" class="btn red" onclick="showRejectBox();"> '
                                .'<i class="fa fa-ban"></i> Reject </a>';
                        }
                        if( ($group_id==1 || $group_id==21 || $hak_akses['reject_spb']==1) && $myData['status']=="3"){
                            echo '<a href="javascript:;" class="btn red" onclick="rejectFulfilment();"> '
                                .'<i class="fa fa-ban"></i> Reject Pemenuhan </a>';
                        }
                    ?>

                    <a href="<?php echo base_url('index.php/GudangRongsok/spb_list'); ?>" class="btn blue-hoki"> 
                        <i class="fa fa-angle-left"></i> Kembali </a>
                    <?php if($group_id==1 || $hak_akses['print_spb']==1){ ?>
                    <a class="btn btn-circle blue-ebonyclay" href="<?php echo base_url('index.php/Ingot/print_spb_fulfilment/').$myData['id'];?>" style="margin-bottom:4px" target="_blank"> &nbsp; <i class="fa fa-print"></i> Print &nbsp; </a>
                    <?php } ?>
                </div>   
                <div class="col-md-2">
                    <?php
                        if( ($group_id==1 || $group_id==21 || $hak_akses['save_spb']==1) && ($myData['status']=="0" || $myData['status']=="4")){
                            echo '<a href="javascript:;" class="btn red" onclick="showCloseBox();"> '
                                .'<i class="fa fa-ban"></i> Close SPB </a> ';
                        }
                    ?>
                </div> 
            </div>
        </form>
        <?php
            }else{
        ?>
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span id="message">Anda tidak memiliki hak akses ke halaman ini!</span>
        </div>
        <?php
            }
        ?>
    </div>
</div> 
<script>
function editApolo(){
    const id = $('#id').val();
    const id_ap = $('#id_apolo').val();
    const id_pi = $('#id_pi').val();
    window.location= '<?php echo base_url(); ?>index.php/Ingot/edit_apolo?id_ap='+id_ap+'&id_pi='+id_pi+'&id='+id;
}

function saveFulfilment(){
    var r=confirm("Anda yakin meng-save permintaan barang ini?");
    if (r==true){
        $('#formku').attr("action", "<?php echo base_url(); ?>index.php/Rsk/save_fulfilment");    
        $('#formku').submit(); 
    }
};

function rejectFulfilment(){
    var r=confirm("Anda yakin me-reject pemenuhan barang ini?");
    if (r==true){
        $('#formku').attr("action", "<?php echo base_url(); ?>index.php/Ingot/reject_fulfilment");    
        $('#formku').submit(); 
    }
};

function tambahData(){
    $('#formku').attr("action", "<?php echo base_url(); ?>index.php/Ingot/tambah_spb");    
    $('#formku').submit(); 
}

function closeSPB(){
    var r=confirm("Anda yakin meng-close permintaan barang ini?");
    if (r==true){
        $('#frmClose').attr("action", "<?php echo base_url(); ?>index.php/Ingot/close_spb");    
        $('#frmClose').submit(); 
    }
};

function showCloseBox(){
    var r=confirm("Anda yakin meng-close permintaan barang ini?");
    if (r==true){
        $('#close_spb_id').val($('#id').val());
        $('#message').html("");
        $('.alert-danger').hide();
        
        $("#closeModal").find('.modal-title').text('Close Permintaan Barang');
        $("#closeModal").modal('show',{backdrop: 'true'}); 
    }
}

function approveData(){
    var r=confirm("Anda yakin meng-approve permintaan barang ini?");
    if (r==true){
        $('#formku').attr("action", "<?php echo base_url(); ?>index.php/Ingot/approve_spb");    
        $('#formku').submit(); 
    }
};

function showRejectBox(){
    var r=confirm("Anda yakin me-reject permintaan barang ini?");
    if (r==true){
        $('#header_id').val($('#id').val());
        $('#message').html("");
        $('.alert-danger').hide();
        
        $("#myModal").find('.modal-title').text('Reject Permintaan Barang');
        $("#myModal").modal('show',{backdrop: 'true'}); 
    }
}

function rejectData(){
    if($.trim($("#reject_remarks").val()) == ""){
        $('#message').html("Reject remarks harus diisi, tidak boleh kosong!");
        $('.alert-danger').show(); 
    }else{
        $('#message').html("");
        $('.alert-danger').hide();
        $('#frmReject').attr("action", "<?php echo base_url(); ?>index.php/Ingot/reject_spb");
        $('#frmReject').submit(); 
    }
}

function check_duplicate(id){
    var valid = true;
        $.each($("input[name$='[no_pallete]']"), function (index1, item1) {
            $.each($("input[name$='[no_pallete]']").not(this), function (index2, item2) {
                if ($(item1).val() == $(item2).val()) {
                    valid = false;
                }
            });
        });
    return valid;
}

function getRongsok(id){
    var no = $("#no_pallete_"+id).val();
    const new_id = id + 1;
    if(no!=''){    
        var check = check_duplicate();
        if(check){
            $.ajax({
                url: "<?php echo base_url('index.php/Ingot/get_rongsok'); ?>",
                type: "POST",
                data : {no_pallete: no},
                success: function (result){
                    if (result!=null){
                        $("#dtr_id_"+id).val(result['id']);
                        $("#ttr_id_"+id).val(result['ttr_id']);
                        $("#nama_item_"+id).val(result['rongsokname']);
                        $("#id_rongsok_"+id).val(result['id_rongsok']);
                        $("#uom_"+id).val(result['uom']);
                        $("#netto_"+id).val(result['netto']);
                        $("#keterangan_"+id).val(result['line_remarks']);
                        $("#btn_"+id).removeClass('disabled');
                        const total = (parseFloat($('#total_netto').val()) + parseFloat(result['netto']));
                        $('#total_netto').val(total);
                        create_new_input(id);
                        $('#no_pallete_'+new_id).focus();
                    } else {
                        alert('No pallete tidak ditemukan, silahkan ulangi kembali');
                        $("#no_pallete_"+id).val('');
                    }
                }
            });
        } else {
            //alert('Inputan pallete tidak boleh sama dengan inputan sebelumnya!');
            $("#no_pallete_"+id).val('');
        }
    }
}


function create_new_input(id){
       var new_id = id+1;
        $("#tabel_pallete>tbody").append('<tr><td><div id="no_tabel_'+new_id+'">'+new_id+'</div><input type="hidden" id="ttr_id_'+new_id+'" name="details['+new_id+'][ttr_id]"/><input type="hidden" id="dtr_id_'+new_id+'" name="details['+new_id+'][dtr_id]"/><input type="hidden" id="id_rongsok_'+new_id+'" name="details['+new_id+'][id_rongsok]"></td><td><input id="no_pallete_'+new_id+'" name="details['+new_id+'][no_pallete]" class="form-control myline" onchange="getRongsok('+new_id+');" type="text"></td><td><input type="text" id="nama_item_'+new_id+'" name="details['+new_id+'][nama_item]" class="form-control myline" readonly="readonly" /></td><td><input id="netto_'+new_id+'" name="details['+new_id+'][netto]" class="form-control myline" readonly="readonly" type="text"></td><td><input id="uom_'+new_id+'" name="details['+new_id+'][uom]" class="form-control myline" readonly="readonly" type="text"></td><td><input id="keterangan_'+new_id+'" name="details['+new_id+'][keterangan]" class="form-control myline" readonly="readonly" type="text"></td><td style="text-align:center"><a id="btn_'+new_id+'" href="javascript:;" class="btn btn-xs btn-circle red disabled" onclick="hapusDetail('+new_id+');" style="margin-top:5px"><i class="fa fa-trash"></i> Delete </a></td></tr>');
}

function hapusDetail(id){
    var r=confirm("Anda yakin menghapus item pallete ini?");
    if (r==true){
        const total = $('#total_netto').val();
        $('#total_netto').val(parseFloat(total) - parseFloat($('#netto_'+id).val()));
        $('#no_pallete_'+id).closest('tr').remove();
        }
}

//AMBIL DTR PALETTE

function saveParsial(no,jb) {
    var netto = $('#netto_'+no).val();
    var id_dtr_detail = $('#dtr_detail_id_'+no).val();
    var id_dtr = $('#dtr_id_'+no).val();
    var id_barang = $('#id_barang_'+no).val();
    var nama_item = $('#nama_item_'+no).val();
    var bruto = $('#bruto_'+no).val();
    var berat_palette = $('#berat_palette_'+no).val();
    var no_pallete = $('#no_pallete_'+no).val();
    var line_remarks = $('#line_remarks_'+no).val();
    var tanggal = $('#tanggal').val();
    // console.log(id_dtr_detail);

    $("#netto").val(netto);
    $("#id_dtr_detail").val(id_dtr_detail);
    $("#id_dtr").val(id_dtr);
    $("#id_barang").val(id_barang);
    $("#nama_item").val(nama_item);
    $("#bruto").val(bruto);
    $("#berat_palette").val(berat_palette);
    $("#no_pallete").val(no_pallete);
    $("#line_remarks").val(line_remarks);
    $("#tanggal_p").val(tanggal);

    $("#netto_ambil").attr({
        "min" : 0,
        "max" : netto
    });

    $("#parsialModal").find('.modal-title').text(jb);
    $("#parsialModal").modal('show',{backdrop: 'true'});
}

function saveDetailParsial(){
    var dtr_id = $('#id_dtr').val();
    var netto = $('#netto').val();
    var u_netto = $('#netto_ambil').val();
    var id_barang = $('#id_barang').val();
    var id_dtr_detail = $('#id_dtr_detail').val();

    var nama_item = $("#nama_item").val();
    var bruto = $("#bruto").val();
    var berat_palette= $("#berat_palette").val();
    var no_pallete = $("#no_pallete").val();
    var line_remarks = $("#line_remarks").val();
    // console.log(u_netto+" > "+netto);

    if(Number(u_netto) > Number(netto)){
        $('#msg_modal').html("Jumlah netto diambil tidak boleh lebih dari netto!");
        $('#box_error_modal').show();
    } else {
        $.ajax({
        type:"POST",
        url:'<?php echo base_url('index.php/Rsk/save_dtr_detail_parsial'); ?>',
        data:{
            id_dtr : dtr_id,
            spb_id : $('#id').val(),
            id_barang: id_barang,
            dtr_detail_id: id_dtr_detail,
            bruto : bruto,
            netto : netto,
            u_netto : u_netto,
            berat_pallete: berat_palette,
            keterangan: line_remarks,
            tanggal: $('#tanggal_p').val()
        },
        success:function(result){
            if(result['message_type']=="sukses"){
                if (result['flag_taken'] == 1) {
                    $('#dtr_id').select2('val', '');  
                } else {
                    $('#dtr_id').select2('val', result['jenis_barang']);
                }
                load_list_dtr();
                // load_dtr();
                loadDetailJb(result['jenis_barang']);
                loadDetailInvoice($('#id').val());
                $('#message').html("");
                $('.alert-danger').hide(); 
                $('#parsialModal').modal('hide');
                $('#netto_ambil').val('');
            }else{
                $('#message').html(result['message']);
                $('.alert-danger').show(); 
            }            
        }
    });
    }
}

function load_list_dtr(){
    $.ajax({
        url:'<?php echo base_url('index.php/Rsk/load_list_dtr'); ?>',
        success:function(result){
            $('#boxDetail0').html(result);     
        }
    });
}

// function load_dtr(){
//     $.ajax({
//         url: "<?php echo base_url('index.php/R_Matching/get_dtr_list'); ?>",
//         async: false,
//         type: "POST",
//         dataType: "html",
//         success: function(result) {
//             $('#dtr_id').html(result);
//         }
//     })
// }

function load_jenis_barang(){
    $.ajax({
        url: "<?= base_url('index.php/R_Matching/get_jenis_barang_list'); ?>",
        async: false,
        type: "POST",
        dataType: "html",
        success: function(result){
            $('#dtr_id').html(result);
        }
    });
}

function loadDetailJb(id){
    $.ajax({
        type: "POST",
        url: "<?= base_url('index.php/R_Matching/load_detail_jb'); ?>",
        data:"id="+ id,
        success:function(result){
            $('#boxDetail').html(result);
        }
    });
}

function saveData(){
    $('#message2').html("");
    $('#alert-danger2').hide(); 
    $('#formku').attr('action','<?php echo base_url(); ?>index.php/Rsk/update_dtr');
    $('#formku').submit(); 
}

function saveDetail(id){
    if(confirm('Anda yakin menambah transaksi ini?')){
        $.ajax({
                type:"POST",
                url:'<?php echo base_url('index.php/Rsk/save_detail_rsk'); ?>',
                data:{
                    id_spb:$('#id').val(),
                    id_dtr:$('#dtr_id_'+id).val(),
                    dtr_asli_id:$('#id').val(),
                    id_barang:$('#id_barang_'+id).val(),
                    dtr_detail_id:$('#dtr_detail_id_'+id).val(),
                    bruto:$('#bruto_'+id).val(),
                    netto: $('#netto_'+id).val(),
                    berat_pallete: $('#berat_palette_'+id).val(),
                    keterangan: $('#line_remarks_'+id).val(),
                    tanggal: $('#tanggal').val()
                },
                success:function(result){
                    if(result['message_type']=="sukses"){
                        if (result['flag_taken'] == 1) {
                            $('#dtr_id').select2('val', '');  
                        } else {
                            $('#dtr_id').select2('val', result['jenis_barang']);
                        }
                        load_list_dtr();
                        // load_dtr();
                        loadDetailJb(result['jenis_barang']);
                        loadDetailInvoice($('#id').val());
                        $('#message').html("");
                        $('.alert-danger').hide(); 
                    }else{
                        $('#message').html(result['message']);
                        $('.alert-danger').show(); 
                    }            
                }
            });
    }else{
        console.log('no');
    }
}

function hapusDetail(id,jb,netto, detail_id_matching){
    //$row->dtr_asli_id.','.$row->rongsok_id.','.$row->netto.','.$row->id
    var dtr_id = $('#dtr_id_'+id).val();
    console.log(detail_id_matching);
    var r=confirm("Anda yakin menghapus item Rongsok ini?");
    if (r==true){
        $.ajax({
            type:"POST",
            url:'<?php echo base_url('index.php/Rsk/delete_dtr_detail'); ?>',
            data:{
                id_dtr: dtr_id,
                id_dtr_detail:id,
                id_barang: jb,
                netto: netto,
                detail_id_matching: detail_id_matching
            },
            success:function(result){
                if(result['message_type']=="sukses"){
                    if (result['check'] == 0) {
                        $('#dtr_id').select2('val', result['jenis_barang']);
                    }
                    load_list_dtr();
                    // load_dtr();
                    loadDetailJb(result['jenis_barang']);
                    loadDetailInvoice($('#id').val());
                }else{
                    alert(result['message']);
                }     
            }
        });
    }
}

function loadDetailInvoice(id){
    $.ajax({
        type:"POST",
        url:'<?php echo base_url('index.php/Rsk/load_detail_dtr'); ?>',
        data:{
            id: id
        },
        success:function(result){
            $('#boxDetail2').html(result);     
        }
    });
}
</script>
<link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script type="text/javascript">
$(function(){   
    $("#tanggal").datepicker({
        showOn: "button",
        buttonImage: "<?php echo base_url(); ?>img/Kalender.png",
        buttonImageOnly: true,
        buttonText: "Select date",
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-mm-yy'
    });  
    load_list_dtr();
    // load_dtr();
    load_jenis_barang();
    loadDetailInvoice(<?php echo $myData['id']; ?>);

    $("#tanggal_keluar").datepicker({
        showOn: "button",
        buttonImage: "<?php echo base_url(); ?>img/Kalender.png",
        buttonImageOnly: true,
        buttonText: "Select date",
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-mm-yy'
    });       
});
</script>