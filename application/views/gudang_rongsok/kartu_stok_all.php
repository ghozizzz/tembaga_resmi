<style type="text/css">
@media print {
  footer {page-break-before: always;}
}
</style>
<?php 
$total_masuk = 0;
$total_keluar = 0;
$total_sisa = 0;

foreach ($loop as $key => $value) { ?>
<table width="100%" class="table table-striped table-bordered table-hover" id="sample_6" style="font-size:12px;">
    <tr>
        <td colspan="8"><strong style="font-size: 16px">PT. KAWAT MAS PRAKASA</strong><br></td>
    </tr>
    <tr>
        <td colspan="8" align="center"><h3 align="center"><b> Kartu Stok Rongsok <?php echo " <i>".$start.' s/d '.$end."</i>";?></b></h3></td>
    </tr>
    <tr>
        <td colspan="4" width="50%"><b>Nama Barang : </b><?=$value['header']['jenis_barang'];?></td>
        <td colspan="2" width="25%" align="center"><b>Per Tanggal : <?=$end;?></b></td>
        <td colspan="2" width="25%" align="right"><b>Kode : </b><?=$value['header']['kode'];?></td>
    </tr>
    <tr>
        <th style="width:40px">No</th>
        <th>Tanggal</th>
        <th>Nomor TTR</th>     
        <th>Keterangan</th>
        <th>Supplier/Customer</th> 
        <th>Masuk</th>
        <th>Keluar</th>
        <th>Sisa</th>
    </tr>
    <tbody>
    <?php
    $no = 1;
    $masuk = 0;
    $keluar = 0;
    $sisa_now = 0;
    $sisa = $value['header']['stok_awal'];
        echo '<tr>';
        echo '<td style="text-align:center"> - </td>';
        echo '<td></td>';
        echo '<td>Saldo Sebelumnya</td>';
        echo '<td></td>';
        echo '<td></td>';
        echo '<td></td>';
        echo '<td></td>';
        echo '<td>'.number_format($sisa,2,',','.').'</td>';
        echo '</tr>';
    foreach ($value['detailLaporan'] as $row){
        echo '<tr>';
        echo '<td style="text-align:center">'.$no.'</td>';
        echo '<td>'.$row->tanggal_masuk.$row->tanggal_keluar.'</td>';
        echo '<td>'.$row->no_ttr.'</td>';
        echo '<td>'.$row->nomor.'</td>';
        echo '<td>'.$row->nama.'</td>';
        echo '<td>'.number_format($row->netto_masuk,2,',','.').'</td>';
        echo '<td>'.number_format($row->netto_keluar,2,',','.').'</td>';
        $sisa_now = $sisa + $row->netto_masuk - $row->netto_keluar;
        echo '<td>'.number_format($sisa_now,2,',','.').'</td>';
        echo '</tr>';
        $no++;
        $sisa = $sisa_now;
        $masuk += $row->netto_masuk;
        $keluar += $row->netto_keluar;
    }
    ?>
    </tbody>
    <footer>
    <tr>
        <td colspan="5"></td>
        <td style="border-bottom:1px solid #000; border-top:1px solid #000"><?=number_format($masuk,2,',','.');?></td>
        <td style="border-bottom:1px solid #000; border-top:1px solid #000"><?=number_format($keluar,2,',','.');?></td>
        <td style="border-bottom:1px solid #000; border-top:1px solid #000"><?=number_format($sisa,2,',','.');?></td>
    </tr>
    </footer>
<?php } ?>
</table>
    <body onLoad="window.print()">
    </body>
</html>