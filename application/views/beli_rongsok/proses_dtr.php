<div class="row">
    <div class="col-md-12 alert-warning alert-dismissable">        
        <h5 style="color:navy">
            <a href="<?php echo base_url(); ?>"> <i class="fa fa-home"></i> Home </a> 
            <i class="fa fa-angle-right"></i> Pembelian 
            <i class="fa fa-angle-right"></i> 
            <a href="<?php echo base_url('index.php/BeliRongsok'); ?>"> Pembelian Rongsok </a> 
            <i class="fa fa-angle-right"></i> 
            <a href="<?php echo base_url('index.php/BeliRongsok/edit_dtr'); ?>"> Edit Data Timbang Rongsok (DTR) </a> 
        </h5>          
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">                            
    <div class="col-md-12">
        <h3 align="center"><b> Konfirmasi DTR Rongsok</b></h3>
        <hr class="divider" />
        <div class="modal fade" id="rejectModal" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                        <form class="eventInsForm" method="post" target="_self" name="frmReject" 
                              id="frmReject">                            
                            <div class="row">
                                <div class="col-md-4">
                                    Reject Remarks <font color="#f00">*</font>
                                </div>
                                <div class="col-md-8">
                                    <textarea id="reject_remarks" name="reject_remarks" 
                                        class="form-control myline" style="margin-bottom:5px" 
                                        onkeyup="this.value = this.value.toUpperCase()" rows="3"></textarea>
                                    
                                    <input type="hidden" id="header_id" name="header_id">
                                </div>
                            </div>                           
                        </form>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn blue" onClick="rejectData();">Simpan</button>
                        <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h3 align="center"><b>Approve TTR</b></h3>
                    <hr class="divider" />
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span id="message">&nbsp;</span>
                            </div>
                        </div>
                    </div>
                        <form class="eventInsForm" method="post" target="_self" name="frmApprove" 
                              id="frmApprove">                            
                            <!-- <div class="row">
                                <div class="col-md-4">
                                    No TTR<font color="#f00">*</font>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="nomor_ttr" name="nomor_ttr" class="form-control myline" style="margin-bottom:5px" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nomor TTR ...">
                                </div>
                            </div>     -->
                                    <input type="hidden" id="dtr_id" name="id">
                                    <input type="hidden" id="tanggal_ttr" name="tanggal">
                                    <input type="hidden" id="no_dtr_2" name="no_dtr">
                            <div class="row">
                                <div class="col-md-4">
                                    No Surat Jalan
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="no_sj" name="no_sj" class="form-control myline" style="margin-bottom:5px" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nomor SJ ...">
                                </div>
                            </div>   
                            <div class="row">
                                <div class="col-md-4">
                                    Jumlah Afkiran
                                </div>
                                <div class="col-md-8">
                                    <input type="number" length="10" id="jmlh_afkiran" name="jmlh_afkiran" class="form-control myline" style="margin-bottom:5px" placeholder="Jumlah Afkiran ...">
                                </div>
                            </div>   
                            <div class="row">
                                <div class="col-md-4">
                                    Jumlah Pengepakan
                                </div>
                                <div class="col-md-8">
                                    <input type="number" length="10" id="jmlh_pengepakan" name="jmlh_pengepakan" class="form-control myline" style="margin-bottom:5px" placeholder="Jumlah Pengepakan ...">
                                </div>
                            </div>   
                            <div class="row">
                                <div class="col-md-4">
                                    Jumlah Lain-Lain
                                </div>
                                <div class="col-md-8">
                                    <input type="number" length="10" id="jmlh_lain" name="jmlh_lain" class="form-control myline" style="margin-bottom:5px" placeholder="Jumlah Lain Lain ...">
                                </div>
                            </div>      
                        </form>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn blue" id="approveData" onClick="approveData();">Simpan</button>
                        <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if( ($group_id==1)||($hak_akses['edit_dtr']==1) ){
        ?>
        <form class="eventInsForm" method="post" target="_self" name="formku" 
              id="formku" action="<?php echo base_url('index.php/BeliRongsok/update_dtr_rsk'); ?>">  
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-4">
                            No. DTR <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="no_dtr" name="no_dtr" readonly="readonly"
                                class="form-control myline" style="margin-bottom:5px" 
                                value="<?php echo $header['no_dtr']; ?>">
                            
                            <input type="hidden" id="id" name="id" value="<?php echo $header['id']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Tanggal <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="tanggal" name="tanggal" readonly="readonly"
                                class="form-control myline input-small" style="margin-bottom:5px;float:left;" 
                                value="<?php echo date('d-m-Y', strtotime($header['tanggal'])); ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        <?php if($header['retur_id'] > 0){ echo 'No. Retur';}else{echo 'No. PO';}?> 
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="no_po" name="no_po" readonly="readonly"
                                class="form-control myline" style="margin-bottom:5px" 
                                value="<?php echo $header['no_po']; ?>">
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-md-4">
                            Catatan
                        </div>
                        <div class="col-md-8">
                            <textarea id="remarks" name="remarks" rows="2" readonly="readonly" 
                                class="form-control myline" style="margin-bottom:5px"><?php echo $header['remarks']; ?></textarea>                           
                        </div>
                    </div>
                </div>
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-5"> 
                    <div class="row">
                        <div class="col-md-4">
                            Supplier
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="supplier" name="supplier" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['nama_supplier']; ?>">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            Jenis Barang
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="jenis_barang" name="jenis_barang" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['jenis_barang']; ?>">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            Nama Penimbang
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="nama_penimbang" name="nama_penimbang" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['penimbang']; ?>">
                        </div>
                    </div>
                    <?php if ($header['status']==9) {?>
                    <div class="row">
                        <div class="col-md-4">
                            Rejected By
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="nama_penimbang" name="nama_penimbang" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['rejected_name']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Reject Remarks
                        </div>
                        <div class="col-md-8">
                            <textarea id="reject_remarks" name="reject_remarks" rows="2" readonly="readonly"
                                class="form-control myline" style="margin-bottom:5px"><?php echo $header['reject_remarks']; ?></textarea>                           
                        </div>
                    </div>
                    <?php } ?>
                </div>              
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>Nama Item Rongsok</th>
                                <th width="10%">UOM</th>
                                <th>Bruto (Kg)</th>
                                <th>Berat Palette</th>
                                <th>Netto (Kg)</th>
                                <th>No. Pallete</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                $bruto = 0;
                                $berat = 0;
                                $netto = 0;
                                foreach ($details as $row){
                                    echo '<tr>';
                                    echo '<td style="text-align:center">'.$no.'</td>';
                                    echo '<td>'.$row->nama_item.'</td>';
                                    echo '<td>'.$row->uom.'</td>';                                   
                                    echo '<td>'.number_format($row->bruto,2,',','.').'</td>';
                                    echo '<td>'.number_format($row->berat_palette,2,',','.').'</td>';
                                    echo '<td>'.number_format($row->netto,2,',','.').'</td>';
                                    echo '<td>'.$row->no_pallete.'</td>';
                                    echo '<td>'.$row->line_remarks.'</td>';
                                    echo '<td><a id="print_'.$no.'" href="javascript:;" class="btn btn-circle btn-xs blue-ebonyclay" onclick="printBarcode('.$no.');" style="margin-top:5px;"><i class="fa fa-trash"></i> Print Barcode </a></td>';
                                    echo '</tr>';
                                    $no++;
                                    $bruto += $row->bruto;
                                    $berat += $row->berat_palette;
                                    $netto += $row->netto;
                                }
                            ?>
                            <tr>
                                <td colspan="3"><strong>Total :</strong></td>
                                <td style="background-color: green; color: white;"><?=number_format($bruto,2,',','.');?></td>
                                <td style="background-color: green; color: white;"><?=number_format($berat,2,',','.');?></td>
                                <td style="background-color: green; color: white;"><?=number_format($netto,2,',','.');?></td>
                                <td colspan="3"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:;" class="btn green" id="simpanData" onclick="simpanData();"> 
                        <i class="fa fa-check"></i> Approve DTR </a>

                    <a href="javascript:;" class="btn red" onclick="showRejectBox();"> 
                        <i class="fa fa-times"></i> Reject DTR </a>

                    <a href="<?php echo base_url('index.php/BeliRongsok/dtr_list'); ?>" class="btn blue-hoki"> 
                        <i class="fa fa-angle-left"></i> Kembali </a>
                </div>    
            </div>
        </form>
        <?php
            }else{
        ?>
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span id="message">Anda tidak memiliki hak akses ke halaman ini!</span>
        </div>
        <?php
            }
        ?>
    </div>
</div> 
<script>
function showRejectBox(){
    var r=confirm("Anda yakin me-reject DTR Rongsok ini?");
    if (r==true){
        $('#header_id').val($('#id').val());
        $('#message').html("");
        $('.alert-danger').hide();
        
        $("#rejectModal").find('.modal-title').text('Reject DTR Rongsok');
        $("#rejectModal").modal('show',{backdrop: 'true'}); 
    }
}

function rejectData(){
    if($.trim($("#reject_remarks").val()) == ""){
        $('#message').html("Close remarks harus diisi, tidak boleh kosong!");
        $('.alert-danger').show(); 
    }else{
        $('#message').html("");
        $('.alert-danger').hide();
        $('#frmReject').attr("action", "<?php echo base_url(); ?>index.php/BeliRongsok/reject_dtr");
        $('#frmReject').submit(); 
    }
}

function myCurrency(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 95 || charCode > 105))
        return false;
    return true;
}

function getComa(value, id){
    angka = value.toString().replace(/\./g, "");
    $('#'+id).val(angka.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
}

function simpanData(){
    $('#dtr_id').val($('#id').val());
    $('#no_dtr_2').val($('#no_dtr').val());
    $('#tanggal_ttr').val($('#tanggal').val());

    $("#myModal").find('.modal-title').text('Approve TTR');
    $("#myModal").modal('show',{backdrop: 'true'}); 
}

function approveData(){
    // if($.trim($("#nomor_ttr").val()) == ""){
    //     $('#message').html("Nomor TTR harus diisi, tidak boleh kosong!");
    //     $('.alert-danger').show(); 
    // }else{
        $('#approveData').text('Please Wait ...').prop("onclick", null).off("click");
        $('#message').html("");
        $('.alert-danger').hide();
        $('#frmApprove').attr("action", "<?php echo base_url(); ?>index.php/BeliRongsok/update_dtr_rsk");
        $('#frmApprove').submit(); 
    // }
}

// function approveData(){
//     if($.trim($("#nomor_ttr").val()) == ""){
//         $('#message').html("Nomor TTR harus diisi, tidak boleh kosong!");
//         $('.alert-danger').show(); 
//     }else{
//         $.ajax({
//             type: "POST",
//             url: "<?php echo base_url('index.php/BeliRongsok/get_no_ttr'); ?>",
//             data: {
//                 no_ttr: $('#nomor_ttr').val(),
//                 tanggal: $('#tanggal').val()
//             },
//             cache: false,
//             success: function(result) {
//                 var res = result['type'];
//                 // console.log(res);
//                 if(res=='duplicate'){
//                     $('#message').html("Nomor TTR sudah ada, tolong coba lagi!");
//                     $('.alert-danger').show(); 
//                 }else{;
//                     $('#approveData').text('Please Wait ...').prop("onclick", null).off("click");
//                     $('#message').html("");
//                     $('.alert-danger').hide();
//                     $('#frmApprove').attr("action", "<?php echo base_url(); ?>index.php/BeliRongsok/update_dtr_rsk");
//                     $('#frmApprove').submit(); 
//                 }
//             }
//         });
//     }
// }

function printBarcode(id){
    const r = $('#rongsok_id_'+id).val();
    const b = $('#bruto_'+id).val();
    const bp = $('#berat_palette_'+id).val();
    const n = $('#netto_'+id).val();
    const np = $('#no_pallete_'+id).val();
    console.log(id+' | '+r+' | '+b+' | '+bp+' | '+n+' | '+np);
    window.open('<?php echo base_url();?>index.php/BeliRongsok/print_barcode_rongsok?r='+r+'&b='+b+'&bp='+bp+'&n='+n+'&np='+np,'_blank');
}
</script>