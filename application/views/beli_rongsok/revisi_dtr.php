<div class="row">
    <div class="col-md-12 alert-warning alert-dismissable">        
        <h5 style="color:navy">
            <a href="<?php echo base_url(); ?>"> <i class="fa fa-home"></i> Home </a> 
            <i class="fa fa-angle-right"></i> Pembelian 
            <i class="fa fa-angle-right"></i> 
            <a href="<?php echo base_url('index.php/BeliRongsok'); ?>"> Pembelian Rongsok </a> 
            <i class="fa fa-angle-right"></i> 
            <a href="<?php echo base_url('index.php/BeliRongsok/edit_dtr'); ?>"> Edit Data Timbang Rongsok (DTR) </a> 
        </h5>          
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">                            
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success <?php echo (empty($this->session->flashdata('flash_msg'))? "display-hide": ""); ?>" id="box_msg_sukses">
                    <button class="close" data-close="alert"></button>
                    <span id="msg_sukses"><?php echo $this->session->flashdata('flash_msg'); ?></span>
                </div>
            </div>
        </div>
        <?php
            if( ($group_id==1)||($hak_akses['revisi_dtr']==1) ){
        ?>
        <form class="eventInsForm" method="post" target="_self" name="formku" 
              id="formku" action="<?php echo base_url('index.php/BeliRongsok/proses_revisi'); ?>">  
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-4">
                            No. DTR <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="no_dtr" name="no_dtr" readonly="readonly"
                                class="form-control myline" style="margin-bottom:5px" 
                                value="<?php echo $header['no_dtr']; ?>">
                            <input type="hidden" name="id" value="<?php echo $header['id'];?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Tanggal <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="tanggal" name="tanggal" class="form-control myline input-small" style="margin-bottom:5px;float:left;" value="<?php echo date('d-m-Y', strtotime($header['tanggal'])); ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            No. PO 
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="no_po" name="no_po" readonly="readonly"
                                class="form-control myline" style="margin-bottom:5px" 
                                value="<?php echo $header['no_po']; ?>">
                            
                            <input type="hidden" id="id" name="id" value="<?php echo $header['id']; ?>">
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-md-4">
                            Catatan
                        </div>
                        <div class="col-md-8">
                            <textarea id="remarks" name="remarks" rows="2" onkeyup="this.value = this.value.toUpperCase()"
                                class="form-control myline" style="margin-bottom:5px"><?php echo $header['remarks']; ?></textarea>                           
                        </div>
                    </div>
                </div>
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-5"> 
                    <div class="row">
                        <div class="col-md-4">
                            Supplier
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="supplier" name="supplier" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['nama_supplier']; ?>">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            Jenis Barang
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="jenis_barang" name="jenis_barang" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['jenis_barang']; ?>">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            Nama Penimbang
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="nama_penimbang" name="nama_penimbang" 
                                class="form-control myline" style="margin-bottom:5px" readonly="readonly" 
                                value="<?php echo $header['penimbang']; ?>">
                        </div>
                    </div>
                </div>              
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th width="15%">Nama Item Rongsok</th>
                                <th width="5%">UOM</th>
                                <th width="15%">Bruto (Kg)</th>
                                <th width="5%">Berat Palette</th>
                                <th width="15%">Netto (Kg)</th>
                                <th width="15%">No. Pallete</th>
                                <th width="15%">Used</th>
                                <th width="15%">Keterangan</th>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                foreach ($details as $row){
                                    echo '<input type="hidden" name="myDetails['.$no.'][id_dtr]" value="'.$row->id.'">';
                                    echo '<tr>';
                                    echo '<td style="text-align:center">'.$no.'</td>';
                                    if($row->flag_taken==0){
                                    echo '<td><select id="rongsok_id" name="myDetails['.$no.'][rongsok_id]" class="form-control myline select2me" data-placeholder="Silahkan pilih..." style="margin-bottom:5px">
                                        <option value=""></option>';
                                            foreach ($list_rongsok as $v){
                                                echo '<option value="'.$v->id.'" '.(($v->id==$row->rongsok_id)? 'selected="selected"': '').'>'.$v->nama_item.'</option>';
                                            }
                                    echo '</select>';
                                    }else{
                                    echo '<td><input type="text" name="myDetails['.$no.'][nama_item]" class="form-control myline" value="'.$row->nama_item.'" readonly></td>';
                                    echo '<input type="hidden" name="myDetails['.$no.'][rongsok_id]" value="'.$row->rongsok_id.'">';
                                    }                                  
                                    echo '</td>';
                                    echo '<td>'.$row->uom.'</td>';
                                    echo '<td><input type="text" id="bruto_'.$no.'" name="myDetails['.$no.'][bruto]" class="form-control myline" maxlength="10" value="'.number_format($row->bruto,0,'','').'" readonly="readonly"></td>';               
                                    echo '<td><input type="text" name="myDetails['.$no.'][berat_palette]" class="form-control myline" value="'.number_format($row->berat_palette,0,'','').'" readonly="readonly"></td>';
                                    echo '<td><input type="text" id="netto_'.$no.'" name="myDetails['.$no.'][netto]" class="form-control myline" maxlength="10" value="'.number_format($row->netto,0,'','').'" onchange="hitung('.$no.')"></td>';
                                    echo '<td><input type="text" name="myDetails['.$no.'][no_pallete]" value="'.$row->no_pallete.'" class="form-control myline" readonly="readonly"></td>';
                                    echo '<td><input type="text" id="netto_resmi_'.$no.'" name="myDetails['.$no.'][netto_resmi]" class="form-control myline" maxlength="10" value="'.number_format($row->netto_resmi,0,'','').'" readonly="readonly"></td>';
                                    echo '<td><input type="text" name="myDetails['.$no.'][line_remarks]" value="'.$row->line_remarks.'" class="form-control myline" onkeyup="this.value = this.value.toUpperCase()"></td>';
                                    echo '</tr>';
                                    $no++;
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php if(!empty($spb)){ ?>
           <div class="row">
              <div class="col-md-12">
                 <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="message">&nbsp;</span>
                 </div>
              </div>
           </div>
            <hr class="divider">
            <h3>Palette sudah digunakan</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>No Pallete</th>
                                <th>Nama SPB</th>
                                <th>Netto (Kg)</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                $netto = 0;
                                // print_r($spb);die();
                                foreach ($spb as $k => $v){
                                    echo '<tr>';
                                    echo '<td style="text-align:center">'.$no.'</td>';
                                    echo '<td><input type="text" class="form-control myline" value="'.$v->no_pallete.'" readonly></td>';
                                    echo '<td><input type="text" class="form-control myline" value="'.$v->no_spb.'" readonly></td>';
                                    echo '</td>';
                                    echo '<td><input type="text" class="form-control myline" maxlength="10" value="'.number_format($v->netto,0,'','').'" readonly="readonly"></td>';
                                    echo '<td>';
                                    if(isset($v->no_spb)){
                                       echo '<a class="btn btn-circle btn-xs blue" href="'.base_url().'index.php/GudangRongsok/view_spb/'.$v->spb_id.'" style="margin-bottom:4px" target="_blank"> &nbsp; <i class="fa  fa-file-text-o"></i> View &nbsp; </a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                    $no++;
                                    $netto += $v->netto;
                                }
                            ?>
                            <tr>
                               <td colspan="3" align="right"><strong>Total Netto Yang Sudah Digunakan</strong></td>
                               <td><input type="text" readonly id="total_netto" class="form-control" style="background-color: green; color: white;" name="total_netto" value="<?=$netto;?>"></td>
                               <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:;" class="btn green" onclick="simpanData();"> 
                        <i class="fa fa-floppy-o"></i> Revisi DTR </a>

                    <a href="<?php echo base_url('index.php/BeliRongsok/dtr_list'); ?>" class="btn blue-hoki"> 
                        <i class="fa fa-angle-left"></i> Kembali </a>
                </div>    
            </div>
        </form>
        <?php
            }else{
        ?>
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span id="message">Anda tidak memiliki hak akses ke halaman ini!</span>
        </div>
        <?php
            }
        ?>
    </div>
</div> 
<script>
function hitung(id){
    const bs = Number($('#netto_'+id).val());
    const ne = Number($('#netto_resmi_'+id).val());
    if(bs < ne){
      $('#netto_'+id).val($('#netto_resmi_'+id).val());
      $('#message').html("Netto Tidak Boleh Lebih Kecil dari Yang Sudah di Gunakan !");
      $('.alert-danger').show(); 
    }
}
function myCurrency(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 95 || charCode > 105))
        return false;
    return true;
}

function getComa(value, id){
    angka = value.toString().replace(/\./g, "");
    $('#'+id).val(angka.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
}

function simpanData(){
    $('#formku').submit(); 
};
</script>
<link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script type="text/javascript">
$(function(){       
    $("#tanggal").datepicker({
        showOn: "button",
        buttonImage: "<?php echo base_url(); ?>img/Kalender.png",
        buttonImageOnly: true,
        buttonText: "Select date",
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-mm-yy'
    });       
});
</script>