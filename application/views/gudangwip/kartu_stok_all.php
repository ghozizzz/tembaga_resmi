<style type="text/css">
@media print {
  footer {page-break-before: always;}
}
</style>
<?php 
$total_masuk = 0;
$total_keluar = 0;
$total_sisa = 0;

foreach ($loop as $key => $value) { ?>
<table width="100%" class="table table-striped table-bordered table-hover" id="sample_6" style="font-size:12px;">
    <tr>
        <td colspan="7"><strong style="font-size: 16px">PT. KAWAT MAS PRAKASA</strong><br></td>
    </tr>
    <tr>
        <td colspan="7" align="center"><h3 align="center"><b> Kartu Stok WIP <?php echo " <i>".$start.' s/d '.$end."</i>";?></b></h3></td>
    </tr>
    <tr>
        <td colspan="3" width="50%"><b>Nama Barang : </b><?=$value['header']['jenis_barang'];?></td>
        <td colspan="2" width="25%" align="center"><b>Per Tanggal : <?=$end;?></b></td>
        <td colspan="2" width="25%" align="right"><b>Kode : </b><?=$value['header']['kode'];?></td>
    </tr>
    <tr>
        <th style="width:40px">No</th>
        <th>Tanggal</th>
        <th>Nomor</th>
        <th>Keterangan</th>
        <th>Berat Masuk</th>
        <th>Berat Keluar</th>
        <th>Sisa Berat</th>
    </tr>
    <tbody>
    <?php
    $no = 1;
    $masuk = 0;
    $keluar = 0;
    $qty_masuk = 0;
    $berat_masuk = 0;
    $qty_keluar = 0;
    $berat_keluar = 0;
    $sisa_berat = $value['header']['stok_awal'];
        echo '<tr>';
        echo '<td style="text-align:center"> - </td>';
        echo '<td></td>';
        echo '<td>Saldo Sebelumnya</td>';
        echo '<td></td>';
        echo '<td></td>';
        echo '<td></td>';
        echo '<td>'.number_format($sisa_berat,2,',','.').'</td>';
        echo '</tr>';
    foreach ($value['detailLaporan'] as $row){
        echo '<tr>';
        echo '<td style="text-align:center">'.$no.'</td>';
        echo '<td>'.$row->tanggal.'</td>';
        echo '<td>'.$row->nomor.'</td>';
        echo '<td>'.$row->keterangan.'</td>';
        echo '<td>'.number_format($row->berat_in,2,',','.').'</td>';
        echo '<td>'.number_format($row->berat_out,2,',','.').'</td>';
        $berat = $sisa_berat + $row->berat_in - $row->berat_out;
        echo '<td>'.number_format($berat,2,',','.').'</td>';
        echo '</tr>';
        $no++;
        $berat_masuk += $row->berat_in;
        $berat_keluar += $row->berat_out;
        $sisa_berat = $berat;
    }
    ?>
    </tbody>
    <footer>
        <tr>
            <td colspan="4"></td>
            <td style="border-bottom:1px solid #000; border-top:1px solid #000"><?=number_format($berat_masuk,2,',','.');?></td>
            <td style="border-bottom:1px solid #000; border-top:1px solid #000"><?=number_format($berat_keluar,2,',','.');?></td>
            <td style="border-bottom:1px solid #000; border-top:1px solid #000"><?=number_format($sisa_berat,2,',','.');?></td>
        </tr>
    </footer>
<?php } ?>
</table>
    <body onLoad="window.print()">
    </body>
</html>