<div class="row">
    <div class="col-md-12 alert-warning alert-dismissable">        
        <h5 style="color:navy">
            <a href="<?php echo base_url(); ?>"> <i class="fa fa-home"></i> Home </a> 
            <i class="fa fa-angle-right"></i> Gudang
            <i class="fa fa-angle-right"></i> 
            <a href="<?php echo base_url('index.php/Ingot/add_produksi'); ?>"> Gudang WIP </a> 
        </h5>          
    </div>
</div>

<form class="eventInsForm" method="post" target="_self" name="formku" id="formku" action="<?php echo base_url('index.php/GudangWIP/update_proses_wip_app'); ?>">
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success <?php echo (empty($this->session->flashdata('flash_msg'))? "display-hide": ""); ?>" id="box_msg_sukses">
                    <button class="close" data-close="alert"></button>
                    <span id="msg_sukses"><?php echo $this->session->flashdata('flash_msg'); ?></span>
                </div>
            </div>
        </div>
             <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            No. Produksi WIP <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input id="no_produksi" name="no_produksi" readonly="readonly" class="form-control myline" style="margin-bottom:5px" value="<?=$header['no_produksi_wip'];?>" type="text">

                            <input type="hidden" name="id_thw" value="<?=$header['id'];?>">
                            <input type="hidden" name="id_thm" value="<?=$header['hasil_masak_id'];?>">
                            <input type="hidden" name="id_bpb" value="<?=$header['id_bpb'];?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Tanggal <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="tanggal" name="tanggal" 
                                class="form-control myline input-small" style="margin-bottom:5px;float:left;" 
                                value="<?=$header['tanggal'];?>">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4">
                            Pilih Proses <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <input id="jenis_masak" name="jenis_masak" readonly="readonly" class="form-control myline" style="margin-bottom:5px" value="<?=$header['jenis_masak'];?>" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Jenis Barang <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                            <select id="jenis_barang" name="jenis_barang" placeholder="Silahkan pilih..."
                                class="form-control myline select2me" style="margin-bottom:5px;" onchange="pilih_data(this.value,6)">
                                <option value=""></option>
                                <?php 
                                foreach($jenis_barang as $k){
                                ?>
                                <option value="<?=$k->id;?>" <?php echo (($header['jenis_barang_id']==$k->id)? 'selected': '');?>><?=$k->jenis_barang;?></option>
                                <?php } ?>    
                            </select>   
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-4">
                            PIC
                        </div>
                        <div class="col-md-8">
                            <input id="nama_pic" name="nama_pic" class="form-control myline" style="margin-bottom:5px" readonly="readonly" value="<?php echo $this->session->userdata('realname');?>" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Keterangan
                        </div>
                        <div class="col-md-8">
                            <textarea id="keterangan" name="keterangan" onkeyup="this.value = this.value.toUpperCase()" class="form-control myline" style="margin-bottom:5px"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            Total Rongsok <font color="#f00">*</font>
                        </div>
                        <div class="col-md-8">
                          <input type="text" id="total_rongsok" name="total_rongsok" readonly="readonly" placeholder="Total Berat Rongsok" class="form-control myline" style="margin-bottom:5px; background-color: green; color: white; font-weight: bold;" value="<?php echo number_format($header['berat_spb'],0,'','');?>">
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="row"> 
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="message">&nbsp;</span>
                </div>
            </div>
        </div>
    <div id="div_kawat_hitam_masuk">
    <div class="col-md-12">
        <div class="row">
               <div class="col-md-6">
                  <div class="panel panel-default">
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-md-4">
                              KAWAT HITAM <font color="#f00">*</font>
                           </div>
                           <div class="col-md-4">
                              <input type="text" id="kh_balok" name="kh"
                                 class="form-control myline" placeholder="kh/batang" style="margin-bottom:5px; width:120px;" required="required" value="<?php echo $header['qty'];?>">
                           </div>
                           <div class="col-md-4">
                              <input type="text" id="kh_berat" name="berat_kh"
                                 class="form-control myline" placeholder="kg" style="margin-bottom:5px; width:100px;"  required="required" onchange="hitung_susut()" value="<?php echo $header['berat'];?>">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              SUSUT  <font color="#f00">*</font>
                           </div>
                           <div class="col-md-3">
                              <input type="text" id="susut" name="susut"
                                 class="form-control myline" placeholder="susut/kg" style="margin-bottom:5px; width:100px;"  required="required" readonly="readonly" value="<?php echo $header['berat_spb']-($header['berat']+$header['bs']+$header['bs_ingot']);?>">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php if($header['jenis_masak']=='ROLLING'){ ?>
               <div class="col-md-6">
                  <div class="panel panel-default">
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-md-4">
                              BS ROLLING
                           </div>
                           <div class="col-md-3">
                              <input type="text" id="bs" name="bs"
                                 class="form-control myline" placeholder="bs/kg" style="margin-bottom:5px; width:100px;"  required="required" onchange="hitung_susut(); check_bs();" value="<?=(($header['bs']==null)? '0':$header['bs']);?>">
                           </div>
                           <div class="col-md-3">
                              <input type="text" id="bs_old" name="bs_old" class="form-control myline" style="margin-bottom: 5px; width: 100px;" value="<?=(($header['bs']==null)? '0':$header['bs']);?>" readonly="readonly">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              BS INGOT
                           </div>
                           <div class="col-md-3">
                              <input type="text" id="bs_ingot" name="bs_ingot"
                                 class="form-control myline" placeholder="bs/kg" style="margin-bottom:5px; width:100px;"  required="required" onchange="hitung_susut()" value="<?php echo $header['bs_ingot'];?>">
                           </div>
                           <div class="col-md-3">
                              <input type="text" id="bs_ingot_old" name="bs_ingot_old" class="form-control myline" style="margin-bottom: 5px; width: 100px;" value="<?php echo $header['bs_ingot']?>" readonly="readonly">
                           </div>
                        </div>
                     </div>
                  </div>
                </div>
                <?php  } ?>
            </div>
            <?php if(!empty($details)){ ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>Nama Item Rongsok</th>
                                <th>Netto (Kg)</th>
                                <th>No. Pallete</th>
                                <th>Keterangan</th>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                foreach ($details as $row){
                                    echo '<input type="hidden" name="myDetails['.$no.'][id_dtr]" value="'.$row->id.'">';
                                    echo '<tr>';
                                    echo '<td style="text-align:center">'.$no.'</td>';
                                    echo '<td><input type="text" name="myDetails['.$no.'][nama_item]" class="form-control myline" value="'.$row->nama_item.'" readonly></td>';
                                    echo '<input type="hidden" name="myDetails['.$no.'][rongsok_id]" value="'.$row->rongsok_id.'">';
                                    echo '</td>';
                                    echo '<td><input type="text" id="netto_'.$no.'" name="myDetails['.$no.'][netto]" class="form-control myline" maxlength="10" value="'.number_format($row->netto,0,'','').'" readonly="readonly"></td>';
                                    echo '<td><input type="text" name="myDetails['.$no.'][no_pallete]" value="'.$row->no_pallete.'" '
                                            . 'class="form-control myline" readonly="readonly"></td>';
                                    echo '<td><input type="text" name="myDetails['.$no.'][line_remarks]" value="'.$row->line_remarks.'"'
                                            . 'class="form-control myline" onkeyup="this.value = this.value.toUpperCase()"></td>';
                                    echo '</tr>';
                                    $no++;
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } if(!empty($details_s)){ ?>
            <h3>Palette yang Sudah Digunakan</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>Nama Item Rongsok</th>
                                <th>Netto (Kg)</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                                $no = 1;
                                $netto = 0;
                                foreach ($details_s as $v){
                                    echo '<tr>';
                                    echo '<td style="text-align:center">'.$no.'</td>';
                                    echo '<td><input type="text" class="form-control myline" value="'.$v['no_spb'].'" readonly></td>';
                                    echo '</td>';
                                    echo '<td><input type="text" class="form-control myline" maxlength="10" value="'.number_format($v['netto'],0,'','').'" readonly="readonly"></td>';
                                    echo '<td><input type="text" value="'.$v['remarks'].'" class="form-control myline" onkeyup="this.value = this.value.toUpperCase()"></td>';
                                    echo '<td><a class="btn btn-circle btn-xs blue" href="'.base_url().'index.php/GudangRongsok/view_spb/'.$v['id'].'" style="margin-bottom:4px" target="_blank"> &nbsp;<i class="fa fa-file-text-o"></i> View &nbsp; </a></td>';
                                    echo '</tr>';
                                    $no++;
                                    $netto += $v['netto'];
                                }
                            ?>
                            <tr>
                                <td colspan="2" align="right"><strong>Total</strong></td>
                                <td><input type="text" class="form-control myline" style="background-color:green; color:white;" maxlength="10" value="<?=number_format($v['netto'],0,'','');?>" id="total_netto" readonly="readonly"></td>
                                <td colspan="2"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>
    </div>
</div>
<hr class="divider"/>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="col-md-4 col-md-offset-4">
                    <a href="#" class="btn green" onclick="simpanData();"> 
                        <i class="fa fa-save"></i> Simpan</a>
                </div>
            </div>
        </div>
    </div>
</form> 
<script>
function simpanData(){
    if($.trim($("#tanggal").val()) == ""){
        $('#message').html("Tanggal tidak boleh kosong!");
        $('.alert-danger').show(); 
    }else{     
        $('#formku').submit(); 
    };
};

function check_bs(){
   const bs = Number($('#bs').val());
   const ne = Number($('#total_netto').val());
    if(bs < ne){
      $('#bs').val($('#total_netto').val());
      $('#message').html("BS Tidak Boleh Lebih Kecil dari Yang Sudah di Gunakan !");
      $('.alert-danger').show(); 
    }
}

function hitung_susut(){
    if($('#jenis_masak').val()=='ROLLING'){
        var susut = Number( Number($('#total_rongsok').val()) - ( Number($('#kh_berat').val()) + Number($('#bs').val()) + Number($('#bs_ingot').val()) ) );
        $('#susut').val(susut);
    }else if($('#jenis_masak').val()=='CUCI'){
        var susut = Number( Number($('#total_rongsok').val()) - Number($('#kh_berat').val()));
        $('#susut').val(susut);
    }
}
</script>
<link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script>
$(function(){        
    $("#tanggal").datepicker({
        showOn: "button",
        buttonImage: "<?php echo base_url(); ?>img/Kalender.png",
        buttonImageOnly: true,
        buttonText: "Select date",
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
    });       
});
</script>