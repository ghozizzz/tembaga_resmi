<!-- CONTAINER -->
<div class="page-container">
    <!-- SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <!-- SIDEBAR MENU -->
            <?php
            $module_name = $this->uri->segment(1);
            $action_name = $this->uri->segment(2);
            $group_id    = $this->session->userdata('group_id');
            $CI =& get_instance();
            if($group_id != 9){
                $CI->load->model('Model_modules');
                $akses_menu = $CI->Model_modules->akses_menu($group_id);
                //print_r($akses_menu);
            }
            ?>
            <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">                
                <li class="sidebar-toggler-wrapper">
                    <!-- SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler"></div>
                </li>                
                <li class="sidebar-search-wrapper">
                    <!-- RESPONSIVE QUICK SEARCH FORM -->                        
                    <form class="sidebar-search " action="extra_search.html" method="POST">
                        <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                        </a>
                        <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                                </span>
                        </div>
                    </form>
                </li>
                <?php if($group_id==9 || (isset($akses_menu['R_Matching']) && $akses_menu['R_Matching']==1)){ ?>
                <li <?php if ($module_name=="Matching") echo 'class="start active open"'; ?>>
                    <a href="<?php echo base_url(); ?>index.php/R_Matching">
                        <i class="fa fa-chain"></i>
                        <span class="title">MATCHING INVOICE</span>
                    </a>
                </li>
                <?php } if($group_id==16 || (isset($akses_menu['R_BPB']) && $akses_menu['R_BPB']==1)){ ?>
                <li <?php if ($module_name=="R_BPB") echo 'class="start active open"'; ?>>
                    <a href="javascript:;">
                    <i class="fa fa-cogs"></i>
                    <span class="title">BPB</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/R_BPB/bpb_list">
                            <i class="fa fa-file-word-o"></i>
                            BPB List </a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                <li class="last ">
                    &nbsp;
                </li>
            </ul>
        </div>
    </div>
    <!-- CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            
            