<html lang="en">
    <head>
        <title></title>
        <meta charset="utf-8" />
    </head>
    <body onLoad="window.print()"><strong>PT. KAWAT MAS PRAKASA</strong><br>

      <table width="100%" style="page-break-after: auto;">
        <tr>
          <td align="center">
            <h4>Laporan Detail Penjualan per <?=tanggal_indo(date('Y-m-d', strtotime($_GET['ts']))).' sampai '.tanggal_indo(date('Y-m-d', strtotime($_GET['te'])));?></h4>
          </td>
        </tr>
      </table>
      <table width="100%" cellpadding="2" cellspacing="0" style="font-size: 13px;">
        <thead>
           <tr>
                <th style="text-align: center; border-top: 1px solid; border-left: 1px solid;">No</th>
                <th style="border-top: 1px solid; border-left: 1px solid;">No. Surat Jalan</th>
                <th style="border-top: 1px solid; border-left: 1px solid;">No. Invoice</th>
                <th style="border-top: 1px solid; border-left: 1px solid;">Nama Customer</th>
                <th style="border-top: 1px solid; border-left: 1px solid;">Jenis Barang</th>
                <th style="border-top: 1px solid; border-left: 1px solid;">Netto</th>
                <th style="text-align: center; border-top: 1px solid; border-left: 1px solid;">Cur</th>
                <th style="text-align: center; border-top: 1px solid; border-left: 1px solid;">Harga Jual</th>
                <th style="text-align: center; border-top: 1px solid; border-left: 1px solid;">Nilai PPN</th>
                <th style="text-align: center; border-top: 1px solid; border-left: 1px solid;">Nilai Materai</th>
                <th style="text-align: center; border-top: 1px solid; border-left: 1px solid; border-right: 1px solid;">Total Harga</th>
           </tr>
         </thead>
         <tbody>
        <?php 
        $no = 1; 
        $last_series = null;
        $last_series2 = null;
        $last_tolling = null;

        $netto2 = 0;
        $ni2 = 0;
        $np2 = 0;
        $nt2 = 0;
        $nm2 = 0;

        $netto = 0;
        $ni = 0;
        $np = 0;
        $nt = 0;
        $nm = 0;
        $nilai_netto = 0;
        $nilai_invoice = 0;
        $nilai_ppn = 0;
        $nilai_total = 0;
        $materai = 0;

        $total_harga = 0;
        foreach($detailLaporan as $row){ 
 /*           $total_amount = $row->netto * $row->amount;  */
              if($last_series != $row->no_surat_jalan && $last_series != null){
                if($last_currency=='IDR'){
                    $ppn = round($ni*10/100);
                }else{
                    $ppn = 0;
                }
                $total_harga = $ni + $ppn + $nm;
                echo '
                <td style="border-top: 1px solid; border-left: 1px solid;">'.number_format($ni,2,',','.').'</td>
                <td align="center" style="border-top: 1px solid; border-left: 1px solid;">'.number_format($ppn,2,',','.').'</td>
                <td style="border-top: 1px solid; border-left: 1px solid;">'.number_format($nm,2,',','.').'</td>
                <td align="right" style="border-top: 1px solid; border-left: 1px solid; border-right: 1px solid;">'.number_format($total_harga,2,',','.').'</td>';
              $nilai_netto += $netto;
              $nilai_invoice += $ni;
              $nilai_ppn += $ppn;
              $nilai_total += $total_harga;
              $materai += $nm;
                $total_harga = 0;
                $ni = 0;
                $nt = 0;
                $nm = 0;
              }elseif($last_series != null){
                echo '
                <td style="border-top: 1px solid; border-left: 1px solid;"></td>
                <td align="center" style="border-top: 1px solid; border-left: 1px solid;"></td>
                <td style="border-top: 1px solid; border-left: 1px solid;"></td>
                <td align="right" style="border-top: 1px solid; border-left: 1px solid; border-right: 1px solid;"></td>';
              }
        ?>
            <tr>
                <?php echo ($last_series==$row->no_surat_jalan) ? '<td style="border-left:1px solid #000">' : '<td style="border-top: 1px solid;border-left: 1px solid;">'.$no ; ?></td>
                <?php echo ($last_series==$row->no_surat_jalan) ? '<td style="border-left:1px solid #000">' : '<td style="border-top: 1px solid;border-left: 1px solid;">'.$row->no_surat_jalan ; ?></td>
                <?php echo ($last_series==$row->no_surat_jalan) ? '<td style="border-left:1px solid #000">' : '<td style="border-top: 1px solid;border-left: 1px solid;">'.$row->no_invoice; ?></td>
                <?php echo ($last_series2==$row->customer) ? '<td style="border-left:1px solid #000">' : '<td style="border-top: 1px solid;border-left: 1px solid;">'.$row->customer; ?></td>
                <td align="left" style="border-top: 1px solid; border-left: 1px solid;"><?= $row->nama_barang ?></td>
                <td align="left" style="border-top: 1px solid; border-left: 1px solid;"><?= number_format($row->netto,2,',','.');?></td>
                <td style="border-top: 1px solid; border-left: 1px solid;"><?=($row->currency=='IDR')?'Rp.': '$ -> Rp.';?></td>
        <?php 
            if($last_series==$row->no_surat_jalan){
              $ni += $row->total_harga;
              $nm = $row->materai;
              $netto += $row->netto;
            }else{
              $netto += $row->netto;
              $ni +=$row->total_harga;
              $nm = $row->materai;
              $no++;
            }
            $last_series = $row->no_surat_jalan;
            $last_series2 = $row->customer;
            $last_currency = $row->currency;
          } 
                if($last_currency=='IDR'){
                    $ppn = round($ni*10/100);
                }else{
                    $ppn = 0;
                }
                $total_harga = $ni + $ppn + $nm;
                echo '
                <td style="border-top: 1px solid; border-left: 1px solid;">'.number_format($ni,2,',','.').'</td>
                <td align="center" style="border-top: 1px solid; border-left: 1px solid;">'.number_format($ppn,2,',','.').'</td>
                <td style="border-top: 1px solid; border-left: 1px solid;">'.number_format($nm,2,',','.').'</td>
                <td align="right" style="border-top: 1px solid; border-left: 1px solid; border-right: 1px solid;">'.number_format($total_harga,2,',','.').'</td>';
              $nilai_netto += $netto;
              $nilai_invoice += $ni;
              $nilai_ppn += $ppn;
              $nilai_total += $total_harga;
              $materai += $nm;
          ?>
          <tr>
            <td colspan="5" style="text-align: right; border-top: 5px solid;border-bottom:1px solid;border-left: 1px solid;"><strong>Grand Total</strong></td>
            <td align="right" style="border-top: 5px solid;border-bottom:1px solid; border-left: 1px solid;"><?=number_format($netto,2,',','.');?></td>
            <td style="border-bottom:1px solid; border-left: 1px solid; border-top: 5px solid;"></td>
            <td align="right" style="border-top: 5px solid;border-bottom:1px solid; border-left: 1px solid;"><?=number_format($nilai_invoice,2,',','.');?></td>
            <td align="right" style="border-top: 5px solid;border-bottom:1px solid; border-left: 1px solid;"><?=number_format($nilai_ppn,2,',','.');?></td>
            <td style="border-bottom:1px solid; border-left: 1px solid; border-top: 5px solid;"><?=number_format($materai,2,',','.');?></td>
            <td align="right" style="border-top: 5px solid;border-bottom:1px solid; border-left: 1px solid; border-right: 1px solid;"><?=number_format($nilai_total,2,',','.');?></td>
          </tr>
        </tbody>   
      </table>
    </body>
</html>