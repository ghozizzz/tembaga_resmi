<?php
class Model_rsk extends CI_Model{

    function dtr_list(){
        $data = $this->db->query("Select dtr.*, 
                    COALESCE(po.no_po,r.no_retur) as no_po,
                    spl.nama_supplier,
                    spl.kode_supplier,
                    usr.realname As penimbang,
                (Select count(dtrd.id)As jumlah_item From dtr_detail dtrd Where dtrd.dtr_id = dtr.id)As jumlah_item
                From dtr
                    Left Join po On (dtr.po_id > 0 and po.id = dtr.po_id)
                    Left Join supplier spl On (po.supplier_id = spl.id) or (dtr.supplier_id = spl.id) 
                    Left Join users usr On (dtr.created_by = usr.id) 
                    Left Join retur r On (r.id = dtr.retur_id)
                    Where (dtr.customer_id = 0 or retur_id > 0) and dtr.type = 1
                Order By dtr.id Desc");
        return $data;
    }

    function show_detail_dtr($id){
        $data = $this->db->query("Select sdf.id, sdf.dtr_detail_id, dd.rongsok_id, sdf.netto, sdf.netto as bruto, dd.no_pallete, dd.berat_palette, r.nama_item from spb_detail_fulfilment sdf 
                    left join dtr_detail dd on dd.id = sdf.dtr_detail_id
                    left join rongsok r on r.id = dd.rongsok_id
                    where sdf.spb_id =".$id);
        return $data;
    }

    function list_dtr(){
        $data = $this->db->query("select dtr.*, r.nama_item, (select SUM(netto) from dtr_detail where dtr_detail.dtr_id = dtr.id and flag_resmi = 0) as netto, dtrd.berat_palette, dtrd.no_pallete, dtrd.line_remarks
            from dtr 
            left join dtr_detail dtrd on (dtr.id = dtrd.dtr_id)
            left join rongsok r on (dtrd.rongsok_id = r.id)
            where dtr.status = 1 and dtr.flag_taken = 0 and type = 0 group by dtr.no_dtr
            order by dtr.tanggal asc");
        return $data;
    }
}